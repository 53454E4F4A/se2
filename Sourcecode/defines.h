/*
 * defines.h
 *
 *  Created on: 14.05.2014
 *  @author: Ulrich ter Horst
 */

#ifndef DEFINES_H_
#define DEFINES_H_

//the following defines are used to identify previous states in the stateStack:


#define IDX_OPERATING					0
#define IDX_PUCKINCRITICAL				1
#define IDX_STOP						2
#define IDX_TURN_PUCK					3
#define IDX_WAIT_FOR_CONV2				4

#define IDX_READY						5
#define IDX_PUCKONCONVEYOR				6
#define IDX_STOP2						7
#define IDX_TURN_PUCK2					8
#define IDX_CONVEYORBACKWARD			9

#endif /* DEFINES_H_ */


