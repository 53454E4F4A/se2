/**
 * Signals.h
 *
 *  Created on: 07.05.2014
 *  @author: Ulrich ter Horst
 *  @file: Signals.h
 */

#ifndef SIGNALS_H_
#define SIGNALS_H_
#include "iostream.h"
#include "../PulseSignals.h"

using namespace std;

class Signals
{
public:
	Signals(){};
	virtual ~Signals(){};
	virtual void inputPuck()		 							= 0;
	virtual void puckAtHeightSensor(PulseSignals::PULSE signal)	= 0;
	virtual void puckAtGate(PulseSignals::PULSE signal)			= 0;
	virtual void puckAtJunk(PulseSignals::PULSE signal)			= 0;
	virtual void outputPuck(PulseSignals::PULSE signal)			= 0;
	virtual void refresh(PulseSignals::PULSE signal)			= 0;
	virtual void closeGate()         							= 0;
	virtual void checkJunk()         							= 0;
	virtual void conveyorTwoAnswers()							= 0;
	virtual void leaveCriticalSection()    						= 0;
	virtual void startBtn()          							= 0;
	virtual void stopBtn()           							= 0;
	virtual void resetBtn()          							= 0;
	virtual void softFailure()		 							= 0;
	virtual void hardFailure()		 							= 0;
};

#endif /* SIGNALS_H_ */
