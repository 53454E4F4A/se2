/*
 * WaitForConveyor2.cpp
 *
 *  Created on: 30.04.2014
 *  @author: Ulrich ter Horst
 */


#include "ControllerStateMachineConveyor1/WaitForConveyor2.h"
#include "../Puck.h"
#include "ControllerStateMachineConveyor1/Controller.h"
#include "../defines.h"

#include "ControllerStateMachineConveyor1/Stop.h"
#include "ControllerStateMachineConveyor1/SoftFailureUnreceipted.h"
#include "ControllerStateMachineConveyor1/HardFailureUnreceipted.h"
#include "ControllerStateMachineConveyor1/Initializing.h"
#include "ConveyorTime.h"
#include "SensorThread.h"

using namespace std;


WaitForConveyor2::WaitForConveyor2(Context* context) : ConveyorState(context)
{
	entryFunction();
	stateIndex = IDX_WAIT_FOR_CONV2;
}

WaitForConveyor2::~WaitForConveyor2()
{

}

void WaitForConveyor2::inputPuck()
{
	softFailure();
}

void WaitForConveyor2::puckAtHeightSensor(PulseSignals::PULSE signal)
{
	if (convContext->controller->firstList.size() == 0)
	{
		hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->firstList.front()->receiveSignal(signal);

		if(puckAnswer == PulseSignals::OK)
		{
			convContext->controller->secondList.push_back(convContext->controller->firstList.front());
			convContext->controller->firstList.pop_front();
		}
		else
		{
			hardFailure();
		}
	}
}

void WaitForConveyor2::puckAtJunk(PulseSignals::PULSE signal)
{
	if(convContext->controller->junkList.size() == 0)
	{
		//hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->junkList.front()->receiveSignal(signal);
		if(puckAnswer == PulseSignals::OK)
		{
			convContext->controller->junkList.pop_front();
			ConveyorTime::getInstance().addNewEvent(PulseSignals::CHECKJUNK,3000, false);
		}
		else
		{
			hardFailure();
		}
	}
}

void WaitForConveyor2::refresh(PulseSignals::PULSE signal)
{
	convContext->controller->serial.ping();
}

void WaitForConveyor2::closeGate()
{
	convContext->controller->gate.Deny();
}

void WaitForConveyor2::checkJunk()
{
	if(SensorThread::getInstance()->isRampFull())
	{
		new (this) SoftFailureUnreceipted(convContext);
		ConveyorTime::getInstance().addNewEvent(PulseSignals::CHECKJUNK,3000, false);
	}
}

void WaitForConveyor2::conveyorTwoAnswers()
{
	convContext->controller->serial.transferPuck(convContext->controller->thirdList.front());

	if(!convContext->controller->thirdList.empty()){
		delete convContext->controller->thirdList.front();
		convContext->controller->thirdList.pop_front();
	}
	convContext->goToPrevState();
}

void WaitForConveyor2::stopBtn()
{
	if(convContext->controller->serial.sendPulse(PulseSignals::STOP_BTN))
	{
			convContext->addStateOnStack(stateIndex);
			new (this) Stop(convContext);
	}
	else
	{
		hardFailure();
	}
}

void WaitForConveyor2::resetBtn()
{
		if(convContext->controller->serial.sendPulse(PulseSignals::RESET_BTN))
		{
				//Change state to initializing
				new (this) Initializing(convContext);
		}
		else
		{
			hardFailure();
		}
}

void WaitForConveyor2::softFailure()
{
	if(convContext->controller->serial.sendPulse(PulseSignals::SOFTFAILURE))
	{
			convContext->addStateOnStack(stateIndex);
			new (this) SoftFailureUnreceipted(convContext);
	}
	else
	{
		hardFailure();
	}
}

void WaitForConveyor2::hardFailure()
{
	convContext->controller->serial.sendPulse(PulseSignals::HARDFAILURE);
		//Change state to hardFailure unreceipted
		new (this) HardFailureUnreceipted(convContext);
}

void WaitForConveyor2::entryFunction()
{
	convContext->controller->conveyor.stop();
	ConveyorTime::getInstance().stopCounter();
	convContext->controller->statusLight.setColor(Statuslight::GREEN);
	convContext->controller->statusLight.setFrequency(Statuslight::FLASH_PERMANENT);

	convContext->controller->panel.revokeLedQ2();

	Logger::log("WaitForConveyor2", "entry");

	cout << convContext->controller->firstList.size() << " in Liste 1" << endl;
	cout << convContext->controller->secondList.size() << " in Liste 2" << endl;
	cout << convContext->controller->thirdList.size() << " in Liste 3" << endl;
}
