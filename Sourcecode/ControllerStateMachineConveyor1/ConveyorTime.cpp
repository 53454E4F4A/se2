/*
 * Timer.cpp
 *
 *  Created on: 29.04.2014
 *      Author: Janek Tichy
 *
 *	Quellen: www.qnx.com for the timer thread
 *
 *	It is important to set the channel id, before this class is used:
 *	'ConveyorTime::getInstance().setChid(int chid)'
 *	Do not call 'setChid()' twice!
 *
 *	Do not forget to start the thread of this class!
 *
 *	To start the timer you have to call the method:
 *		startCounter();
 *
 *	And to stop the time :
 *		stopCounter();
 *
 *	This timer is able to work conveyer dependent and independent ( system time )
 */

#include "ConveyorTime.h"
#include "EventObject.h"

#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>

#include <sys/netmgr.h>
#include <sys/neutrino.h>
#include "../Pattern/ScopePattern.h"

//#include "cpt_terminal.h"
#include "pthread.h"
#include "errno.h"
#include "unistd.h"
#include "stdio.h"
#include "stdlib.h"
#include "Logger.h"

using namespace std;

#define MY_PULSE_CODE   _PULSE_CODE_MINAVAIL
#define CONVEYORSPEED 10 // in mS
pthread_mutex_t ConveyorTime::mtx_ = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t ConveyorTime::mtxList_ = PTHREAD_MUTEX_INITIALIZER;

uint32_t ConveyorTime::getTimeCount()
{
	return this->timeCount;
}

uint32_t ConveyorTime::getEveryTime()
{
	return this->everyTime;
}

ConveyorTime::ConveyorTime()
{
	this->actualSpeed = 0;
	this->init = false;

	this->timeCount = 0; // TODO unclever, für debug erstmal ok
	this->everyTime = 0;
}

ConveyorTime& ConveyorTime::getInstance()
{
	static ConveyorTime instance;
	return instance;
}

void ConveyorTime::addNewEvent(PulseSignals::PULSE eventNumber, uint32_t timeToWaitInMs,bool cyclePulse)
{

	EventObject* timerEvent = new EventObject(eventNumber, getTimeCount(), timeToWaitInMs, cyclePulse);

	ScopePattern lock(&mtxList_);
	this->eventList.push_back(timerEvent);
	Logger::log("ConveyorTime","insert event true/false");
}

void ConveyorTime::addNewEvent(PulseSignals::PULSE eventNumber, uint32_t timeToWaitInMs,
				bool cyclePulse, bool everyPulse)
{
	ScopePattern lock(&mtxList_);
	this->eventList.push_back(new EventObject(eventNumber, getEveryTime(), timeToWaitInMs,
					cyclePulse, everyPulse));

	Logger::log("ConveyorTime","insert event true/false true/false");
}

void ConveyorTime::deleteEvent(PulseSignals::PULSE eventNumber, bool cyclePulse)
{
	std::list<EventObject*>::iterator it;
	for (it = eventList.begin(); it != eventList.end(); ++it)
	{
		if (it._Ptr->_Myval->getEventNumber() == eventNumber && it._Ptr->_Myval->isCyclePulse()
						== cyclePulse)
		{
			pthread_mutex_lock(&mtxList_);
			delete *it; // delete the object
			eventList.erase(it); // delete the pointer on the object
			pthread_mutex_unlock(&mtxList_);
			Logger::log("ConveyorTime","delete event");
		}
	}
}

void ConveyorTime::findNewEvent()
{
	std::list<EventObject*>::iterator it;
	uint32_t timeDiff;

	for (it = eventList.begin(); it != eventList.end(); ++it)
	{

		if (it._Ptr->_Myval->isEveryPulse())
		{
			timeDiff = getEveryTime() - it._Ptr->_Myval->getStartTime();
		}
		else
		{
			timeDiff = getTimeCount() - it._Ptr->_Myval->getStartTime();
		}

		if (timeDiff >= it._Ptr->_Myval->getTimeToWait())
		{
			if (!this->init)
			{
				this->init = true;
				int nodeId = 0;
				pid_t pid = 0;
				coid = ConnectAttach(nodeId, pid, getChid(), 0, 0);
			}

			MsgSendPulse(coid, SIGEV_PULSE_PRIO_INHERIT, it._Ptr->_Myval->getEventNumber(), 0);

			if (!it._Ptr->_Myval->isCyclePulse())
			{
				pthread_mutex_lock(&mtxList_);
				delete *it; // delete the object
				eventList.erase(it); // delete the pointer on the object
				pthread_mutex_unlock(&mtxList_);
			}
			else
			{
				if (it._Ptr->_Myval->isEveryPulse())
				{
					it._Ptr->_Myval->setStartTime(this->getEveryTime());
				}
				else
				{
					it._Ptr->_Myval->setStartTime(this->getTimeCount());
				}
			}

		}
	}
}

void ConveyorTime::stopCounter()
{
	ScopePattern lock(&mtx_);
	this->actualSpeed = 0;
	Logger::log("ConveyorTime","counter stopped");
}

void ConveyorTime::startCounter()
{
	ScopePattern lock(&mtx_);
	this->actualSpeed = CONVEYORSPEED;
	Logger::log("ConveyorTime","counter stopped");
}

void ConveyorTime::execute(void*)
{
	struct sigevent event;
	struct itimerspec itime;
	timer_t timer_id;
	int chid;
	my_message_t msg;

	chid = ChannelCreate(0);

	event.sigev_notify = SIGEV_PULSE;
	event.sigev_coid = ConnectAttach(ND_LOCAL_NODE, 0, chid, _NTO_SIDE_CHANNEL, 0);
	event.sigev_priority = getprio(0);
	event.sigev_code = MY_PULSE_CODE;

	timer_create(CLOCK_REALTIME, &event, &timer_id);
	itime.it_value.tv_sec = 0;
	/* 10 million nsecs = .001 secs */
	itime.it_value.tv_nsec = 10000000;
	itime.it_interval.tv_sec = 0;
	/* 10 million nsecs = .001 secs */
	itime.it_interval.tv_nsec = 10000000;
	timer_settime(timer_id, 0, &itime, NULL);

	while (!HAWThread::isStopped())
	{
		if (MsgReceive(chid, &msg, sizeof(msg), NULL) != -1)
		{ /* we got a pulse */
			if (msg.pulse.code == MY_PULSE_CODE)
			{
				this->timeCount += this->actualSpeed;
				this->everyTime += CONVEYORSPEED;
				this->findNewEvent();
			}
		}
		//TODO channel destroyen etc. speicher aufräumen
	}

}
void ConveyorTime::shutdown()
{
	cout << "ConveyorTime Thread terminated" << endl;
}
