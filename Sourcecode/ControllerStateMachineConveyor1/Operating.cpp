/**
 * Operating.cpp
 *
 *  Created on: 30.04.2014
 *  @author: Ulrich ter Horst & Lars Schwensen
 */


#include "../Puck.h"
#include "ControllerStateMachineConveyor1/Controller.h"
#include "../defines.h"

#include "ControllerStateMachineConveyor1/Operating.h"

#include "ControllerStateMachineConveyor1/Initializing.h"
#include "ControllerStateMachineConveyor1/PuckInCriticalSection.h"
#include "ControllerStateMachineConveyor1/WaitForConveyor2.h"
#include "ControllerStateMachineConveyor1/TurnPuck.h"
#include "ControllerStateMachineConveyor1/Stop.h"
#include "ControllerStateMachineConveyor1/SoftFailureUnreceipted.h"
#include "ControllerStateMachineConveyor1/HardFailureUnreceipted.h"
#include "ConveyorTime.h"
#include "SensorThread.h"

//#include "../HardwareControl/Conveyor.h"

using namespace std;

Operating::Operating(Context* context) : ConveyorState(context)
{
	entryFunction();
	stateIndex = IDX_OPERATING;
	timeOut_stopConveyor = false;
}

Operating::~Operating()
{

}

/**
 * @return void
 * This function creates a puck and enqueues it into first queue.
 * If possible, conveyor starts moving forward.
 * Transition: --> PuckInCriticalSection
*/
void Operating::inputPuck()
{
	//Create Puck and put it into inputQueue
	convContext->controller->firstList.push_back(new Puck()); //.push(new Puck());

	//Change state to PuckInCriticalSection
	new (this) PuckInCriticalSection(convContext);
}

void Operating::puckAtHeightSensor(PulseSignals::PULSE signal)
{
	if (convContext->controller->firstList.size() == 0)
	{
		hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->firstList.front()->receiveSignal(signal);

		if(puckAnswer == PulseSignals::OK)
		{
			convContext->controller->secondList.push_back(convContext->controller->firstList.front());
			convContext->controller->firstList.pop_front();
		}
		else
		{
			hardFailure();
		}
	}
}

void Operating::puckAtGate(PulseSignals::PULSE signal)
{
	Logger::log("Operating", "received PUCKATGATE");
	if(convContext->controller->secondList.size() == 0)
	{
		Logger::log("Operating", "LISTE IST LEER!!!");
		hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->secondList.front()->receiveSignal(signal);

		if(puckAnswer == PulseSignals::GATEPASS)
		{
			convContext->controller->gate.Pass();

			convContext->controller->thirdList.push_back(convContext->controller->secondList.front());
			convContext->controller->secondList.pop_front();

			ConveyorTime::getInstance().addNewEvent(PulseSignals::CLOSEGATE,800,false);
		}
		else if(puckAnswer == PulseSignals::GATEDENY)
		{
			convContext->controller->junkList.push_back(convContext->controller->secondList.front());
			convContext->controller->secondList.pop_front();

			convContext->controller->gate.Pass();
			ConveyorTime::getInstance().addNewEvent(PulseSignals::CLOSEGATE,100,false);
		}
		else
		{
			hardFailure();
		}
	}
}

void Operating::puckAtJunk(PulseSignals::PULSE signal)
{
	if(convContext->controller->junkList.size() == 0)
	{
		//hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->junkList.front()->receiveSignal(signal);

		if(puckAnswer == PulseSignals::OK)
		{
			if(convContext->controller->junkList.empty())
			{
				delete convContext->controller->junkList.front();
				convContext->controller->junkList.pop_front();
			}

			ConveyorTime::getInstance().addNewEvent(PulseSignals::CHECKJUNK,3000,false);
		}
		else
		{
			hardFailure();
		}
	}
}

void Operating::outputPuck(PulseSignals::PULSE signal)
{
	if(convContext->controller->thirdList.size() == 0)
	{
		//hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->thirdList.front()->receiveSignal(signal);
		if(puckAnswer == PulseSignals::OK)
		{
			if(convContext->controller->serial.sendPulse(PulseSignals::REQUEST)){
				convContext->addStateOnStack(stateIndex);
				new (this) WaitForConveyor2(convContext);
			}
			else
			{
				hardFailure();
			}
		}
		else if(puckAnswer == PulseSignals::TURNPUCK)
		{
			convContext->addStateOnStack(stateIndex);
			new (this) TurnPuck(convContext);
		}
		else
		{
			hardFailure();
		}
	}
}



/*
 * description:
 * The conveyor stops, if there is no puck on the conveyor for at least one
 * refresh period.
 * Otherwise iterate through all puck-lists and broadcast
 * the refresh signal to all pucks.
 * Execute an UART ping in every refresh period.
 */
void Operating::refresh(PulseSignals::PULSE signal)
{
	if(!SensorThread::getInstance()->isOutputPuck() && convContext->controller->firstList.empty() && convContext->controller->secondList.empty() && convContext->controller->thirdList.empty())
	{
		if(timeOut_stopConveyor)
		{
			convContext->controller->conveyor.stop();
			ConveyorTime::getInstance().stopCounter();
			convContext->controller->gate.Deny();
		}
		else
		{
			timeOut_stopConveyor = true;
		}
	}
	else if(!convContext->controller->firstList.empty() || !convContext->controller->secondList.empty() || !convContext->controller->thirdList.empty())
	{
		convContext->controller->conveyor.forward_fast();
		ConveyorTime::getInstance().startCounter();

		timeOut_stopConveyor = false;

		PulseSignals::PULSE puckAnswer;

		//convContext->controller->firstList.
		std::list<Puck*>::const_iterator iterator;
		for (iterator = convContext->controller->firstList.begin(); iterator != convContext->controller->firstList.end(); iterator++)
		{
			puckAnswer = (*iterator)->receiveSignal(signal);
			if(puckAnswer == PulseSignals::NOTOK)
			{
				hardFailure();
				break;
			}
		}
		for (iterator = convContext->controller->secondList.begin(); iterator != convContext->controller->secondList.end(); iterator++)
		{
			puckAnswer = (*iterator)->receiveSignal(signal);
			if(puckAnswer == PulseSignals::NOTOK)
			{
				hardFailure();
				break;
			}
		}
		for (iterator = convContext->controller->thirdList.begin(); iterator != convContext->controller->thirdList.end(); iterator++)
		{
			puckAnswer = (*iterator)->receiveSignal(signal);
			if(puckAnswer == PulseSignals::NOTOK)
			{
				hardFailure();
				break;
			}
		}
	}

	convContext->controller->serial.ping();
}

void Operating::closeGate()
{
	convContext->controller->gate.Deny();
	Logger::log("Operating","gate is closed");
}

void Operating::checkJunk()
{
	if(SensorThread::getInstance()->isRampFull()){
			softFailure();

			ConveyorTime::getInstance().addNewEvent(PulseSignals::CHECKJUNK,3000,false);
	}
}

void Operating::stopBtn()
{
	if(convContext->controller->serial.sendPulse(PulseSignals::STOP_BTN))
	{
			convContext->addStateOnStack(stateIndex);
			new (this) Stop(convContext);
	}
	else
	{
		hardFailure();
	}
}

void Operating::resetBtn()
{
		if(convContext->controller->serial.sendPulse(PulseSignals::RESET_BTN))
		{
				new (this) Initializing(convContext);
		}
		else
		{
			hardFailure();
		}
}

void Operating::softFailure()
{
	if(convContext->controller->serial.sendPulse(PulseSignals::SOFTFAILURE))
	{
			convContext->addStateOnStack(stateIndex);
			new (this) SoftFailureUnreceipted(convContext);
	}
	else
	{
		hardFailure();
	}
}

void Operating::hardFailure(){
	convContext->controller->serial.sendPulse(PulseSignals::HARDFAILURE);
	//Change state to hardFailure unreceipted
	new (this) HardFailureUnreceipted(convContext);
}

void Operating::entryFunction()
{
	convContext->controller->statusLight.setColor(Statuslight::GREEN);
	convContext->controller->statusLight.setFrequency(Statuslight::FLASH_PERMANENT);

	convContext->controller->panel.revokeLedReset();

	convContext->controller->panel.revokeLedQ1();
	convContext->controller->panel.revokeLedQ2();

	if(SensorThread::getInstance()->isOutputPuck())
	{
		convContext->controller->conveyor.forward_fast();
		ConveyorTime::getInstance().startCounter();
	}

	Logger::log("Operating", "entry");
}

