/*
 * TurnPuck.cpp
 *
 *  Created on: 30.04.2014
 *  @author: Ulrich ter Horst & Lars Schwensen
 */

#include "ControllerStateMachineConveyor1/Controller.h"
#include "../defines.h"

#include "ControllerStateMachineConveyor1/TurnPuck.h"

#include "ControllerStateMachineConveyor1/Stop.h"
#include "ControllerStateMachineConveyor1/Initializing.h"
#include "ControllerStateMachineConveyor1/SoftFailureUnreceipted.h"
#include "ControllerStateMachineConveyor1/HardFailureUnreceipted.h"
#include "ControllerStateMachineConveyor1/WaitForConveyor2.h"
#include "ConveyorTime.h"
#include "SensorThread.h"

using namespace std;

TurnPuck::TurnPuck(Context* context) : ConveyorState(context)
{
	entryFunction();
	stateIndex = IDX_TURN_PUCK;
}

TurnPuck::~TurnPuck()
{

}

void TurnPuck::inputPuck()
{
	softFailure();
}

void TurnPuck::puckAtHeightSensor(PulseSignals::PULSE signal)
{
	if (convContext->controller->firstList.size() == 0)
	{
		hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->firstList.front()->receiveSignal(signal);

		if(puckAnswer == PulseSignals::OK)
		{
			convContext->controller->secondList.push_back(convContext->controller->firstList.front());
			convContext->controller->firstList.pop_front();
		}
		else
		{
			hardFailure();
		}
	}
}

void TurnPuck::puckAtJunk(PulseSignals::PULSE signal)
{
	if(convContext->controller->junkList.size() == 0)
	{
		//hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->junkList.front()->receiveSignal(signal);
		if(puckAnswer == PulseSignals::OK)
		{
			convContext->controller->junkList.pop_front();
			ConveyorTime::getInstance().addNewEvent(PulseSignals::CHECKJUNK,3000, false);
		}
		else
		{
			hardFailure();
		}
	}
}

void TurnPuck::outputPuck(PulseSignals::PULSE signal)
{

}

void TurnPuck::refresh(PulseSignals::PULSE signal)
{
	convContext->controller->serial.ping();
}

void TurnPuck::closeGate()
{
	convContext->controller->gate.Deny();
}

void TurnPuck::checkJunk()
{
	if(SensorThread::getInstance()->isRampFull()){
		softFailure();
		ConveyorTime::getInstance().addNewEvent(PulseSignals::CHECKJUNK,3000, false);
	}
}

void TurnPuck::startBtn()
{
	if(convContext->controller->serial.sendPulse(PulseSignals::REQUEST)){

		new (this) WaitForConveyor2(convContext);
	}
	else
	{
		hardFailure();
	}
}

void TurnPuck::stopBtn()
{
	if(convContext->controller->serial.sendPulse(PulseSignals::STOP_BTN))
	{		convContext->addStateOnStack(stateIndex);
			new (this) Stop(convContext);
	}
	else
	{
		hardFailure();
	}
}

void TurnPuck::resetBtn()
{
		if(convContext->controller->serial.sendPulse(PulseSignals::RESET_BTN))
		{
			new (this) Initializing(convContext);
		}
		else
		{
			hardFailure();
		}
}

void TurnPuck::softFailure()
{
	if(convContext->controller->serial.sendPulse(PulseSignals::SOFTFAILURE))
	{
		convContext->addStateOnStack(stateIndex);
		new (this) SoftFailureUnreceipted(convContext);
	}
	else
	{
		hardFailure();
	}
}

void TurnPuck::hardFailure(){
	convContext->controller->serial.sendPulse(PulseSignals::HARDFAILURE);
	//Change state to hardFailure unreceipted
	new (this) HardFailureUnreceipted(convContext);
}

void TurnPuck::entryFunction()
{
	convContext->controller->conveyor.stop();
	ConveyorTime::getInstance().stopCounter();

	convContext->controller->statusLight.setColor(Statuslight::YELLOW);
	convContext->controller->statusLight.setFrequency(Statuslight::FLASH_05HZ);

	convContext->controller->panel.revokeLedQ2();

	Logger::log("TurnPuck","entry");
}

