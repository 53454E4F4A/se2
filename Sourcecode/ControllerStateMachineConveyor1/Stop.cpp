/*
 * Stop.cpp
 *
 *  Created on: 14.05.2014
 *      Author: blinch
 */

#include "ControllerStateMachineConveyor1/Stop.h"
#include "defines.h"
#include "ControllerStateMachineConveyor1/SoftFailureUnreceipted.h"
#include "ControllerStateMachineConveyor1/HardFailureUnreceipted.h"
#include "ControllerStateMachineConveyor1/Initializing.h"
#include "ConveyorTime.h"
#include "SensorThread.h"

Stop::Stop(Context *context) : ConveyorState(context)
{
	entryFunction();
	stateIndex = IDX_STOP;
}

Stop::~Stop()
{
}

void Stop::puckAtHeightSensor(PulseSignals::PULSE signal)
{
	if (convContext->controller->firstList.size() == 0)
	{
		hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->firstList.front()->receiveSignal(signal);

		if(puckAnswer == PulseSignals::OK)
		{
			convContext->controller->secondList.push_back(convContext->controller->firstList.front());
			convContext->controller->firstList.pop_front();
		}
		else
		{
			hardFailure();
		}
	}
}

void Stop::puckAtJunk(PulseSignals::PULSE signal)
{
	if (convContext->controller->junkList.size() == 0)
	{
		//hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->junkList.front()->receiveSignal(signal);
		if (puckAnswer == PulseSignals::OK)
		{
			convContext->controller->junkList.pop_front();
			ConveyorTime::getInstance().addNewEvent(PulseSignals::CHECKJUNK, 3000, false);
		}
		else
		{
			hardFailure();
		}
	}
}

void Stop::checkJunk()
{
	if (SensorThread::getInstance()->isRampFull())
	{
		softFailure();
		ConveyorTime::getInstance().addNewEvent(PulseSignals::CHECKJUNK, 3000, false);
	}
}

void Stop::refresh(PulseSignals::PULSE signal)
{
	convContext->controller->serial.ping();
}

void Stop::startBtn()
{
	if(convContext->controller->panel.startPressed())
	{
		convContext->controller->serial.sendPulse(PulseSignals::START_BTN);
	}
		convContext->goToPrevState();
}

void Stop::resetBtn()
{
	if (convContext->controller->serial.sendPulse(PulseSignals::RESET_BTN))
	{
		//Change state to initializing
		new (this) Initializing(convContext);
	}
	else
	{
		hardFailure();
	}
}

void Stop::softFailure()
{
	if (convContext->controller->serial.sendPulse(PulseSignals::SOFTFAILURE))
	{
		convContext->addStateOnStack(stateIndex);
		new (this) SoftFailureUnreceipted(convContext);
	}
	else
	{
		hardFailure();
	}
}

void Stop::hardFailure()
{
	convContext->controller->serial.sendPulse(PulseSignals::HARDFAILURE);
	//Change state to hardFailure unreceipted
	new (this) HardFailureUnreceipted(convContext);

}

void Stop::entryFunction()
{
	convContext->controller->conveyor.stop();
	ConveyorTime::getInstance().stopCounter();
	convContext->controller->panel.setLedQ2();

	Logger::log("Stop","entry");
}
