/**
 * State.cpp
 *
 *  Created on: 28.04.2014
 *      @author: Ulrich ter Horst
 */

//Todo refactor comments to match doxygen
#include "ControllerStateMachineConveyor1/ConveyorState.h"
#include "ControllerStateMachineConveyor1/Controller.h"
#include "ControllerStateMachineConveyor1/HardFailureUnreceipted.h"
#include <iostream>

using namespace std;

ConveyorState::ConveyorState(Context* context):
	convContext(context)
{
	//cout << "ConveyorState - constructor called" << endl;
}

ConveyorState::~ConveyorState()
{
	//cout << "ConveyorState - destructor called" << endl;
}


void ConveyorState::entryFunction()
{
	cout << "ConveyorState - entryFunction: do nothing" << endl;
}


void ConveyorState::closeGate()
{
	cout << "ConveyorState - closeGate: do nothing" << endl;
}


void ConveyorState::puckAtGate(PulseSignals::PULSE signal)
{
	cout << "ConveyorState - puckAtGate: do nothing" << endl;
}


void ConveyorState::resetBtn()
{
	cout << "ConveyorState - resetBtn: do nothing" << endl;
}


void ConveyorState::puckAtJunk(PulseSignals::PULSE signal)
{
	cout << "ConveyorState - puckAtJunk: do nothing" << endl;
}


void ConveyorState::checkJunk()
{
	cout << "ConveyorState - checkJunk: do nothing" << endl;
}


void ConveyorState::refresh(PulseSignals::PULSE signal)
{
	cout << "ConveyorState - refresh: do nothing" << endl;
}


void ConveyorState::outputPuck(PulseSignals::PULSE signal)
{
	cout << "ConveyorState - outputPuck: do nothing" << endl;
}


void ConveyorState::puckAtHeightSensor(PulseSignals::PULSE signal)
{
	cout << "ConveyorState - puckAtHeightSens: do nothing" << endl;
}


void ConveyorState::inputPuck()
{
	cout << "ConveyorState - inputPuck: do nothing" << endl;
}


void ConveyorState::conveyorTwoAnswers()
{
	cout << "ConveyorState - Conv2Answers: do nothing" << endl;
}

void ConveyorState::leaveCriticalSection()
{
	cout << "ConveyorState - leaveCriticalSection: do nothing" << endl;
}

void ConveyorState::stopBtn()
{
	cout << "ConveyorState - stopBtn: do nothing" << endl;
}


void ConveyorState::startBtn()
{
	cout << "ConveyorState - startBtn: do nothing" << endl;
}


void ConveyorState::softFailure()
{
	cout << "ConveyorState - sofFailure: do nothing" << endl;
}


void ConveyorState::hardFailure()
{
	cout << "ConveyorState - hardFailure: do nothing" << endl;
}


