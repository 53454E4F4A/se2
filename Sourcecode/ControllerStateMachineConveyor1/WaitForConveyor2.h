/*
 * WaitForConveyor2.h
 *
 *  Created on: 30.04.2014
 *  @author: Ulrich ter Horst
 */


#ifndef WAITFORCONVEYOR2_H_
#define WAITFORCONVEYOR2_H_

#include "ControllerStateMachineConveyor1/ConveyorState.h"
#include "PulseSignals.h"

using namespace std;

class WaitForConveyor2: public ConveyorState
{
public:
	WaitForConveyor2(Context* context);
	virtual ~WaitForConveyor2();

	uint8_t stateIndex;

	virtual void inputPuck();
	virtual void puckAtHeightSensor(PulseSignals::PULSE signal);
	//virtual void puckAtGate(PulseSignals::PULSE signal);
	virtual void puckAtJunk(PulseSignals::PULSE signal);
	//virtual void outputPuck(PulseSignals::PULSE signal);
	virtual void refresh(PulseSignals::PULSE signal);
	virtual void closeGate();
	virtual void checkJunk();
	virtual void conveyorTwoAnswers();
	//virtual void leaveCriticalSection();
	//virtual void startBtn();
	virtual void stopBtn();
	virtual void resetBtn();
	virtual void softFailure();
	virtual void hardFailure();

	/**
	 * @return void
	 * This function is called by constructor.
	 * it sets:
	 * -gate: deny
	 * -statuslight: all off
	 * -conveyor: stop
	 */
	virtual void entryFunction();
};

#endif /* WAITFORCONVEYOR2_H_ */
