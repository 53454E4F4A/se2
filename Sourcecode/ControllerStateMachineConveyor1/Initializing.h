/*
 * Initializing.h
 *
 *  Created on: 30.04.2014
 *  @author: Ulrich ter Horst
 */


#ifndef INITIALIZING_H_
#define INITIALIZING_H_

#include "ControllerStateMachineConveyor1/ConveyorState.h"
#include "ControllerStateMachineConveyor1/Context.h"


using namespace std;


class Initializing : public ConveyorState
{
public:
	Initializing(Context *context);
	virtual ~Initializing();

	//virtual void inputPuck()
	//virtual void puckAtHeightSensor(PulseSignals::PULSE signal)
	//virtual void puckAtGate(PulseSignals::PULSE signal)
	//virtual void puckAtJunk(PulseSignals::PULSE signal)
	//virtual void outputPuck(PulseSignals::PULSE signal)
	//virtual void refresh(PulseSignals::PULSE signal)
	//virtual void closeGate()
	//virtual void checkJunk()
	//virtual void conveyorTwoAnswers()
	//virtual void leaveCriticalSection();
	virtual void startBtn();
	//virtual void stopBtn()
	virtual void resetBtn();
	//virtual void softFailure()
	//virtual void hardFailure()

	/**
	* @return void
	* This function is called by constructor.
	* it sets:
	* -gate: deny
	* -statuslight: all off
	* -conveyor: stop
	*/
	virtual void entryFunction();
};

#endif /* INITIALIZING_H_ */

