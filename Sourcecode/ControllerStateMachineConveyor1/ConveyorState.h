/**
 * State.h
 *
 *  Created on: 28.04.2014
 *  @author: Ulrich ter Horst
 */

#ifndef STATE_H_
#define STATE_H_


#include "../HardwareControl/Statuslight.h"
#include "ControllerStateMachineConveyor1/Signals.h"
#include "PulseSignals.h"
#include "ControllerStateMachineConveyor1/Context.h"


using namespace std;

class Context;


class ConveyorState : public Signals
{
public:
	ConveyorState();
	ConveyorState(Context *context);
	void entryFunction();
	virtual ~ConveyorState();
	virtual void inputPuck();
	virtual void puckAtGate(PulseSignals::PULSE signal);
	virtual void puckAtHeightSensor(PulseSignals::PULSE signal);
	virtual void puckAtJunk(PulseSignals::PULSE signal);
	virtual void outputPuck(PulseSignals::PULSE signal);
	virtual void refresh(PulseSignals::PULSE signal);
	virtual void closeGate();
	virtual void checkJunk();
	virtual void conveyorTwoAnswers();
	virtual void leaveCriticalSection();
	virtual void startBtn();
	virtual void stopBtn();
	virtual void resetBtn();
	virtual void softFailure();
	virtual void hardFailure();

protected:
	Context* convContext;
};



#endif /* STATE_H_ */

