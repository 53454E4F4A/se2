/*
 * HardFailureUnreceipted.cpp
 *
 *  Created on: 12.05.2014
 *  @author: Ulrich ter Horst
 */

#include "ControllerStateMachineConveyor1/HardFailureUnreceipted.h"
#include "ControllerStateMachineConveyor1/HardFailureReceipted.h"
#include "ControllerStateMachineConveyor1/Controller.h"
#include "ConveyorTime.h"
using namespace std;

HardFailureUnreceipted::HardFailureUnreceipted(Context *context) :
	ConveyorState(context)
{
	entryFunction();
}

HardFailureUnreceipted::~HardFailureUnreceipted()
{

}

void HardFailureUnreceipted::refresh(PulseSignals::PULSE signal)
{
	convContext->controller->serial.ping();
}

void HardFailureUnreceipted::startBtn()
{
	if (convContext->controller->panel.startPressed())
	{
		convContext->controller->serial.sendPulse(PulseSignals::START_BTN);
	}
	new (this) HardFailureReceipted(convContext);
}

void HardFailureUnreceipted::closeGate()
{
	convContext->controller->gate.Deny();
}

void HardFailureUnreceipted::entryFunction()
{
	convContext->controller->conveyor.stop();
	ConveyorTime::getInstance().stopCounter();

	convContext->controller->statusLight.setColor(Statuslight::RED);
	convContext->controller->statusLight.setFrequency(Statuslight::FLASH_1HZ);

	convContext->controller->panel.revokeLedQ2();

	Logger::log("HardFailureUnreceipted", "entry");
}
