/*
 * EventObject.cpp
 *
 *  Created on: 30.04.2014
 *      Author: Janek Tichy
 *
 *  This class creates an EventObject for the timer class.
 *  It is administered all important information for an event, triggered on a specific time
 *
 *  There are two ways to use this class with two different constructors:
 *  	1. Create an object dependent on the conveyer time,
 *  	   this means that the time does not increase, when time conveyer does not move.
 *  	2. Create an object dependent on the system time, it means independent from the
 *  	   conveyer movement.
 */

#include "EventObject.h"
#include <stdint.h>
#include "PulseSignals.h"

EventObject::EventObject( PulseSignals::PULSE eventNumber, uint32_t startTime, uint32_t timeToWait, bool CyclePulse){
	this->CyclePulse = CyclePulse;
	this->startTime = startTime;
	this->eventNumber = eventNumber;
	this->timeToWait = timeToWait;
	this->everyPulse = false;
}

EventObject::EventObject (PulseSignals::PULSE eventNumber, uint32_t startTime,uint32_t timeToWait, bool CyclePulse, bool everyPulse){
	this->CyclePulse = CyclePulse;
	this->startTime = startTime;
	this->eventNumber = eventNumber;
	this->timeToWait = timeToWait;
	this->everyPulse = everyPulse;
}

uint32_t EventObject::getStartTime() const {
	return startTime;
}

void  EventObject::setStartTime(uint32_t timeCount){
	this->startTime = timeCount;
}

PulseSignals::PULSE  EventObject::getEventNumber() const {
	return eventNumber;
}

uint32_t  EventObject::getTimeToWait() const{
	return this->timeToWait;
}

bool  EventObject::isCyclePulse() const {
	return CyclePulse;
}

bool  EventObject::isEveryPulse() const{
	return everyPulse;
}
