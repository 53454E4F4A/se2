/*
 * PuckInCriticalSection.cpp
 *
 *  Created on: 30.04.2014
 *  @author: Ulrich ter Horst
 */

#include "Controller.h"
#include "../defines.h"

#include "ControllerStateMachineConveyor1/PuckInCriticalSection.h"

#include "ControllerStateMachineConveyor1/Initializing.h"
#include "ControllerStateMachineConveyor1/Stop.h"
#include "ControllerStateMachineConveyor1/TurnPuck.h"
#include "ControllerStateMachineConveyor1/WaitForConveyor2.h"
#include "ControllerStateMachineConveyor1/Operating.h"
#include "ControllerStateMachineConveyor1/SoftFailureUnreceipted.h"
#include "ControllerStateMachineConveyor1/HardFailureUnreceipted.h"
#include "SensorThread.h"
#include "ConveyorTime.h"
#include "Logger.h"
using namespace std;

PuckInCriticalSection::PuckInCriticalSection(Context* context) : ConveyorState(context)
{
	entryFunction();
	stateIndex = IDX_PUCKINCRITICAL;
}

PuckInCriticalSection::~PuckInCriticalSection()
{

}

void PuckInCriticalSection::inputPuck()
{
	softFailure();
}

void PuckInCriticalSection::puckAtHeightSensor(PulseSignals::PULSE signal)
{
	if (convContext->controller->firstList.size() == 0)
	{
		hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->firstList.front()->receiveSignal(signal);

		if(puckAnswer == PulseSignals::OK)
		{
			convContext->controller->secondList.push_back(convContext->controller->firstList.front());
			convContext->controller->firstList.pop_front();
		}
		else
		{
			hardFailure();
		}
	}
}

void PuckInCriticalSection::puckAtGate(PulseSignals::PULSE signal)
{
	if(convContext->controller->firstList.size() == 0)
	{
		Logger::log("PuckInCriticalSection - PuckAtGate","firstList.size==0");
		hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->secondList.front()->receiveSignal(signal);

		if(puckAnswer == PulseSignals::GATEPASS)
		{
			convContext->controller->gate.Pass();

			convContext->controller->thirdList.push_back(convContext->controller->secondList.front());
			convContext->controller->secondList.pop_front();

			ConveyorTime::getInstance().addNewEvent(PulseSignals::CLOSEGATE,800,false);
		}
		else if(puckAnswer == PulseSignals::GATEDENY)
		{
			convContext->controller->junkList.push_back(convContext->controller->secondList.front());
			convContext->controller->secondList.pop_front();

			convContext->controller->gate.Pass();
			ConveyorTime::getInstance().addNewEvent(PulseSignals::CLOSEGATE,100,false);
		}
		else
		{
			hardFailure();
		}
	}
}

void PuckInCriticalSection::puckAtJunk(PulseSignals::PULSE signal)
{
	if(convContext->controller->junkList.size() == 0)
	{
		//hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->junkList.front()->receiveSignal(signal);
		if(puckAnswer == PulseSignals::OK)
		{
			convContext->controller->junkList.pop_front();

			ConveyorTime::getInstance().addNewEvent(PulseSignals::CHECKJUNK,3000,false);
		}
		else
		{
			hardFailure();
		}
	}
}

void PuckInCriticalSection::outputPuck(PulseSignals::PULSE signal)
{
	if(convContext->controller->thirdList.size() == 0)
	{
		//hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->thirdList.front()->receiveSignal(signal);
		if(puckAnswer == PulseSignals::OK)
		{
			if(convContext->controller->serial.sendPulse(PulseSignals::REQUEST)){

				convContext->addStateOnStack(stateIndex);
				new (this) WaitForConveyor2(convContext);
			}
			else
			{
				hardFailure();
			}
		}
		else if(puckAnswer == PulseSignals::TURNPUCK)
		{
			convContext->addStateOnStack(stateIndex);
			new (this) TurnPuck(convContext);
		}
	}
}

void PuckInCriticalSection::refresh(PulseSignals::PULSE signal)
{
	PulseSignals::PULSE puckAnswer;

	//convContext->controller->firstList.
	std::list<Puck*>::const_iterator iterator;
	for (iterator = convContext->controller->firstList.begin(); iterator != convContext->controller->firstList.end(); iterator++)
	{
		puckAnswer = (*iterator)->receiveSignal(signal);
		if(puckAnswer == PulseSignals::NOTOK)
		{
			hardFailure();
			break;
		}
	}
	for (iterator = convContext->controller->secondList.begin(); iterator != convContext->controller->secondList.end(); iterator++)
	{
		puckAnswer = (*iterator)->receiveSignal(signal);
		if(puckAnswer == PulseSignals::NOTOK)
		{
			hardFailure();
			break;
		}
	}
	for (iterator = convContext->controller->thirdList.begin(); iterator != convContext->controller->thirdList.end(); iterator++)
	{
		puckAnswer = (*iterator)->receiveSignal(signal);
		if(puckAnswer == PulseSignals::NOTOK)
		{
			hardFailure();
			break;
		}
	}

	convContext->controller->serial.ping();
}

void PuckInCriticalSection::closeGate()
{
	convContext->controller->gate.Deny();
}

void PuckInCriticalSection::checkJunk()
{
	if(SensorThread::getInstance()->isRampFull()){

		softFailure();

		ConveyorTime::getInstance().addNewEvent(PulseSignals::CHECKJUNK,3000,false);
	}
}

void PuckInCriticalSection::leaveCriticalSection()
{
	new (this) Operating(convContext);
}

void PuckInCriticalSection::stopBtn()
{
	if(convContext->controller->serial.sendPulse(PulseSignals::STOP_BTN))
	{
		convContext->addStateOnStack(stateIndex);
		new (this) Stop(convContext);
	}
	else
	{
		hardFailure();
	}
}

void PuckInCriticalSection::resetBtn()
{
		if(convContext->controller->serial.sendPulse(PulseSignals::RESET_BTN))
		{
				new (this) Initializing(convContext);
		}
		else
		{
			hardFailure();
		}
}

void PuckInCriticalSection::softFailure()
{
	if(convContext->controller->serial.sendPulse(PulseSignals::SOFTFAILURE))
	{
			convContext->addStateOnStack(stateIndex);
			new (this) SoftFailureUnreceipted(convContext);
	}
	else
	{
		hardFailure();
	}
}

void PuckInCriticalSection::hardFailure(){
	convContext->controller->serial.sendPulse(PulseSignals::HARDFAILURE);
		//Change state to hardFailure unreceipted
	new (this) HardFailureUnreceipted(convContext);
}

void PuckInCriticalSection::entryFunction()
{
	convContext->controller->conveyor.forward_fast();
	ConveyorTime::getInstance().startCounter();

	convContext->controller->statusLight.setColor(Statuslight::GREEN);
	convContext->controller->statusLight.setFrequency(Statuslight::FLASH_PERMANENT);
	convContext->controller->panel.setLedQ1();

	//convContext->controller->panel.revokeLedQ2(); commented by UtH, why Q2???

	Logger::log("PuckInCriticalSection", "entry");
	ConveyorTime::getInstance().addNewEvent(PulseSignals::LEAVECRITICAL,1000,false);
}
