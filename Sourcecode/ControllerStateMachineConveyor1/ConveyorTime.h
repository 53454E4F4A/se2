/*
 * Timer.h
 *
 *  Created on: 29.04.2014
 *      Author: Janek Tichy
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <stdint.h>
#include "../lib/HAWThread.h"
#include "EventObject.h"
#include <list>

typedef union
{
	struct _pulse pulse;
	int eventNumber;
} my_message_t;

class ConveyorTime: public thread::HAWThread
{
public:
	//Variables


private:
	//Variables
	uint32_t timeCount; // In this variable is the conveyer time
	uint32_t everyTime;

	uint32_t actualSpeed;

	static pthread_mutex_t mtx_;
	static pthread_mutex_t mtxList_;

	std::list<EventObject*> eventList;

	bool init;
	pid_t pid;
	int coid;
	int chid;

public:
	//Methods
	int getChid()
	{
		return this->chid;
	}

	void setChid(int chid)
	{
		static bool isSet = false;

		if (!isSet)
		{
			this->chid = chid;
			isSet = true;
		}
	}

	static ConveyorTime& getInstance();

	/**
	 * Return the Conveyor time
	 */
	uint32_t getTimeCount();

	uint32_t getEveryTime();

	/**
	 * This method call stops to increase the counter
	 */
	void stopCounter();

	void startCounter();

	/**
	 * This method orders this timer class to send a user specified pulse message.
	 * Attention:	The time of the specified pulse message delay 'timeToWaitInMs'
	 * 				elapses only while the conveyor is moving.
	 * @param	eventNumber specifies the wanted pulse message.
	 * 			timeToWaitInMs is the delay between calling this method and sending of the wanted pulse message.
	 * 			cyclePulse is a boolean which determines, if the pulse message is sent cyclical from the moment of calling this method on.
	 */
	void addNewEvent(PulseSignals::PULSE eventNumber, uint32_t timeToWaitInMs, bool cyclePulse);

	/**
	 * In contrast to the method above, this method
	 * instructs this timer class to send pulse messages, even
	 * when the conveyor is not moving.
	 */
	void addNewEvent(PulseSignals::PULSE eventNumber, uint32_t timeToWaitInMs, bool cyclePulse,
					bool everyPulse);

	/**
	 * Delete this Event from the list
	 */
	void deleteEvent(PulseSignals::PULSE eventNumber, bool cyclePulse);

private:
	//Methods
	ConveyorTime();

	/**
	 * to be implemented in the derived class.
	 * The application programmer has to write his own loop.
	 * He can check bool isStopped() to see if the thread
	 * should exit the loop.
	 */
	virtual void execute(void*);
	/**
	 * this function must be implemented
	 * in the derived class.
	 * The function is called once after
	 * the thread has been stopped.
	 */
	virtual void shutdown();

	/**
	 * After a hardwaretimer pulse, the list have to be searched, for a new deadline
	 */
	void findNewEvent();

};

#endif /* TIMER_H_ */
