/*
 * Initializing.cpp
 *
 *  Created on: 30.04.2014
 *  @author: Ulrich ter Horst
 */

#include "ControllerStateMachineConveyor1/Initializing.h"
#include "ControllerStateMachineConveyor1/Controller.h"
#include "ControllerStateMachineConveyor1/Operating.h"
#include "../defines.h"
#include "ConveyorTime.h"
#include <new>

using namespace std;

Initializing::Initializing(Context* context) : ConveyorState(context)
{
	cout << "Initializing - constructor called" << endl;
	entryFunction();
}

Initializing::~Initializing()
{
	cout << "Initializing - destructor called" << endl;
}


/**
* @return: void
* transition: --> operating
*/
void Initializing::startBtn()
{
	if(!convContext->controller->panel.emergencyPressed()){
		convContext->controller->serial.sendPulse(PulseSignals::START_BTN);

		cout << "Initializing - startBtn called" << endl;
		new (this) Operating(convContext);
	}
}

void Initializing::resetBtn()
{
	Puck::setReferenceValue();
}

void Initializing::entryFunction()
{
	convContext->controller->conveyor.stop();
	ConveyorTime::getInstance().stopCounter();

	convContext->controller->gate.Deny();

	convContext->controller->statusLight.setColor(Statuslight::OFF);
	convContext->controller->statusLight.setFrequency(Statuslight::FLASH_PERMANENT);

	convContext->controller->panel.setLedReset();

	convContext->controller->panel.revokeLedQ1();
	convContext->controller->panel.revokeLedQ2();


	std::list<Puck*>::const_iterator iterator;
	for (iterator = convContext->controller->firstList.begin(); iterator != convContext->controller->firstList.end(); iterator++)
	{
		delete *iterator;
	}
	for (iterator = convContext->controller->secondList.begin(); iterator != convContext->controller->secondList.end(); iterator++)
	{
		delete *iterator;
	}
	for (iterator = convContext->controller->thirdList.begin(); iterator != convContext->controller->thirdList.end(); iterator++)
	{
		delete *iterator;
	}

	convContext->controller->firstList.clear();
	convContext->controller->secondList.clear();
	convContext->controller->thirdList.clear();

	Logger::log("Initializing", "entry");
}
