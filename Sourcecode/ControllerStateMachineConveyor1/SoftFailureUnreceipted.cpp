/*
 * SoftFailureUnreceipted.cpp
 *
 *  Created on: 12.05.2014
 *  @author: Ulrich ter Horst
 */

#include "ControllerStateMachineConveyor1/Controller.h"

#include "ControllerStateMachineConveyor1/SoftFailureUnreceipted.h"

#include "ControllerStateMachineConveyor1/SoftFailureReceipted.h"
#include "ControllerStateMachineConveyor1/SoftFailureUnreceiptedGone.h"
#include "ControllerStateMachineConveyor1/HardFailureUnreceipted.h"
#include "ControllerStateMachineConveyor1/Initializing.h"
#include "ConveyorTime.h"
#include "SensorThread.h"

using namespace std;

SoftFailureUnreceipted::SoftFailureUnreceipted(Context *context) : ConveyorState(context)
{
	entryFunction();
	isInInputPuck = false;
}

SoftFailureUnreceipted::~SoftFailureUnreceipted()
{

}

void SoftFailureUnreceipted::inputPuck()
{
	isInInputPuck = true;
}

void SoftFailureUnreceipted::puckAtHeightSensor(PulseSignals::PULSE signal)
{
	if (convContext->controller->firstList.size() == 0)
	{
		hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->firstList.front()->receiveSignal(signal);

		if(puckAnswer == PulseSignals::OK)
		{
			convContext->controller->secondList.push_back(convContext->controller->firstList.front());
			convContext->controller->firstList.pop_front();
		}
		else
		{
			hardFailure();
		}
	}
}

void SoftFailureUnreceipted::puckAtJunk(PulseSignals::PULSE signal)
{
	if (convContext->controller->junkList.size() == 0)
	{
		//hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->junkList.front()->receiveSignal(signal);
		if (puckAnswer == PulseSignals::OK)
		{
			if(!convContext->controller->junkList.empty()){
				delete convContext->controller->junkList.front();
				convContext->controller->junkList.pop_front();
			}

			ConveyorTime::getInstance().addNewEvent(PulseSignals::CHECKJUNK,3000, false);
		}
		else
		{
			hardFailure();
		}
	}
}

void SoftFailureUnreceipted::refresh(PulseSignals::PULSE signal)
{
	convContext->controller->serial.ping();
}

void SoftFailureUnreceipted::closeGate()
{
	convContext->controller->gate.Deny();
}

void SoftFailureUnreceipted::checkJunk()
{
	if (!SensorThread::getInstance()->isRampFull())
	{
		new (this) SoftFailureUnreceiptedGone(convContext);
	}
	else
	{
		ConveyorTime::getInstance().addNewEvent(PulseSignals::CHECKJUNK,3000, false);
	}
}

void SoftFailureUnreceipted::startBtn()
{
	if(convContext->controller->panel.startPressed()){
		convContext->controller->serial.sendPulse(PulseSignals::START_BTN);
	}
		new (this) SoftFailureReceipted(convContext);
}

void SoftFailureUnreceipted::resetBtn()
{
	if(convContext->controller->panel.resetPressed()){
			convContext->controller->serial.sendPulse(PulseSignals::RESET_BTN);
	}
	new (this) Initializing(convContext);
}

void SoftFailureUnreceipted::hardFailure()
{
	convContext->controller->serial.sendPulse(PulseSignals::HARDFAILURE);
	//Change state to hardFailure unreceipted
	new (this) HardFailureUnreceipted(convContext);
}

void SoftFailureUnreceipted::entryFunction()
{
	Logger::log("SoftFailureUnreceipted", "entry");

	convContext->controller->conveyor.stop();
	ConveyorTime::getInstance().stopCounter();

	convContext->controller->statusLight.setColor(Statuslight::RED);
	convContext->controller->statusLight.setFrequency(Statuslight::FLASH_1HZ);

	convContext->controller->panel.revokeLedQ2();

	Logger::log("SoftFailureUnreceipted", "entry");
}
