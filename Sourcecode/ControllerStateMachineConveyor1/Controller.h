/**
 * Controller.h
 *
 *  Created on: 27.04.2014
 *      @author: Ulrich ter Horst
 *
 *   description:	ALL IN PROGRESS - This is the controller, which is in charge of
 *   				most incidents, activities and events of the puck sorting machine.
 *   				It is a kind of a context class of our main state machine.
 */


#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include "../HardwareControl/Controlpanel.h"
#include "../HardwareControl/Conveyor.h"
#include "../HardwareControl/Gate.h"
#include "../HardwareControl/SerialThread.h"
#include "../HardwareControl/Statuslight.h"
#include "../lib/HAWThread.h"
#include "./Context.h"
#include "./Initializing.h"
#include "../Puck.h"


#include <list>

using namespace std;

class Controller : public thread::HAWThread
{
public:
	Controller();
	virtual ~Controller();
	void execute(void*);	//dispatcher planned to be implemented here (stand: 28.04.14)
	void shutdown();

	Gate gate;
	Controlpanel panel;
	SerialThread serial;
	Statuslight statusLight;
	Conveyor conveyor;

	list<Puck*> firstList;
	list<Puck*> secondList;
	list<Puck*> junkList;
	list<Puck*> thirdList;

private:
	pthread_mutex_t mtx_;
	int chid;

	//Timer timer;
	//InterruptHandler interruptH;
	//Dispatcher* dispatcher;
	Context* context;
};

#endif /* CONTROLLER_H_ */
