/*
 * Stop.h
 *
 *  Created on: 14.05.2014
 *      Author: blinch
 */

#ifndef STOP_H_
#define STOP_H_

#include "ControllerStateMachineConveyor1/ConveyorState.h"
#include "ControllerStateMachineConveyor1/Controller.h"
class Stop: public ConveyorState
{
public:
	Stop(Context *context);
	virtual ~Stop();
	virtual void entryFunction();

	uint8_t stateIndex;

	//Signals:

	//virtual void inputPuck()
	virtual void puckAtHeightSensor(PulseSignals::PULSE signal);
	//virtual void puckAtGate(PulseSignals::PULSE signal)
	virtual void puckAtJunk(PulseSignals::PULSE signal);
	//virtual void outputPuck(PulseSignals::PULSE signal)
	virtual void refresh(PulseSignals::PULSE signal);
	//virtual void closeGate()
	virtual void checkJunk();
	//virtual void conveyorTwoAnswers()
	//virtual void leaveCriticalSection();
	virtual void startBtn();
	//virtual void stopBtn()
	virtual void resetBtn();
	virtual void softFailure();
	virtual void hardFailure();
};

#endif /* STOP_H_ */
