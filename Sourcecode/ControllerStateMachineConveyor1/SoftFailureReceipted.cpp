/*
 * SoftFailureReceipted.cpp
 *
 *  Created on: 12.05.2014
 *      Author: blinch
 */

#include "ControllerStateMachineConveyor1/SoftFailureReceipted.h"

#include "ControllerStateMachineConveyor1/Controller.h"
#include "ControllerStateMachineConveyor1/HardFailureUnreceipted.h"
#include "ControllerStateMachineConveyor1/Initializing.h"
#include "ConveyorTime.h"
SoftFailureReceipted::SoftFailureReceipted(Context *context) : ConveyorState(context)
{
	entryFunction();
	isInInputPuck = false;

}

SoftFailureReceipted::~SoftFailureReceipted()
{

}

void SoftFailureReceipted::inputPuck()
{
	isInInputPuck = true;
}

void SoftFailureReceipted::puckAtHeightSensor(PulseSignals::PULSE signal)
{
	if (convContext->controller->firstList.size() == 0)
	{
		hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->firstList.front()->receiveSignal(signal);

		if(puckAnswer == PulseSignals::OK)
		{
			convContext->controller->secondList.push_back(convContext->controller->firstList.front());
			convContext->controller->firstList.pop_front();
		}
		else
		{
			hardFailure();
		}
	}
}

void SoftFailureReceipted::puckAtJunk(PulseSignals::PULSE signal)
{
	if (convContext->controller->junkList.size() == 0)
	{
		//hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->junkList.front()->receiveSignal(signal);
		if (puckAnswer == PulseSignals::OK)
		{
			if(!convContext->controller->junkList.empty()){
				delete convContext->controller->junkList.front();
				convContext->controller->junkList.pop_front();
			}

			ConveyorTime::getInstance().addNewEvent(PulseSignals::CHECKJUNK,3000, false);
		}
		else
		{
			hardFailure();
		}
	}
}

void SoftFailureReceipted::refresh(PulseSignals::PULSE signal)
{
	convContext->controller->serial.ping();
}


void SoftFailureReceipted::closeGate()
{
	convContext->controller->gate.Deny();
}

void SoftFailureReceipted::startBtn()
{
	if(convContext->controller->panel.startPressed()){
		convContext->controller->serial.sendPulse(PulseSignals::START_BTN);
	}
		convContext->goToPrevState();
}

void SoftFailureReceipted::resetBtn()
{
	if (convContext->controller->serial.sendPulse(PulseSignals::RESET_BTN))
	{
		//Change state to initializing
		new (this) Initializing(convContext);
	}
	else
	{
		hardFailure();
	}
}

void SoftFailureReceipted::hardFailure()
{
	convContext->controller->serial.sendPulse(PulseSignals::HARDFAILURE);
	//Change state to hardFailure unreceipted
	new (this) HardFailureUnreceipted(convContext);
}

void SoftFailureReceipted::entryFunction()
{
	convContext->controller->conveyor.stop();
	ConveyorTime::getInstance().stopCounter();

	convContext->controller->statusLight.setColor(Statuslight::RED);
	convContext->controller->statusLight.setFrequency(Statuslight::FLASH_PERMANENT);

	convContext->controller->panel.revokeLedQ2();

	Logger::log("SoftFailureReceipted", "entry");
}
