/*
 * SoftFailureUnreceiptedGone.cpp
 *
 *  Created on: 12.05.2014
 *  @author: Ulrich ter Horst
 */

#include "ControllerStateMachineConveyor1/Controller.h"
#include "ControllerStateMachineConveyor1/SoftFailureUnreceiptedGone.h"
#include "ControllerStateMachineConveyor1/SoftFailureReceipted.h"
#include "ControllerStateMachineConveyor1/HardFailureUnreceipted.h"
#include "ControllerStateMachineConveyor1/Initializing.h"
#include "ConveyorTime.h"
using namespace std;

SoftFailureUnreceiptedGone::SoftFailureUnreceiptedGone(Context *context) : ConveyorState(context)
{
	entryFunction();
	isInInputPuck = false;
}

SoftFailureUnreceiptedGone::~SoftFailureUnreceiptedGone()
{

}

void SoftFailureUnreceiptedGone::inputPuck()
{
	isInInputPuck = true;
}

void SoftFailureUnreceiptedGone::puckAtHeightSensor(PulseSignals::PULSE signal)
{
	if (convContext->controller->firstList.size() == 0)
	{
		hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->firstList.front()->receiveSignal(signal);

		if(puckAnswer == PulseSignals::OK)
		{
			convContext->controller->secondList.push_back(convContext->controller->firstList.front());
			convContext->controller->firstList.pop_front();
		}
		else
		{
			hardFailure();
		}
	}
}

void SoftFailureUnreceiptedGone::puckAtJunk(PulseSignals::PULSE signal)
{
	if (convContext->controller->junkList.size() == 0)
	{
		//hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->junkList.front()->receiveSignal(signal);
		if (puckAnswer == PulseSignals::OK)
		{
			convContext->controller->junkList.pop_front();
			ConveyorTime::getInstance().addNewEvent(PulseSignals::CHECKJUNK,3000, false);
		}
		else
		{
			hardFailure();
		}
	}
}

void SoftFailureUnreceiptedGone::refresh(PulseSignals::PULSE signal)
{
	convContext->controller->serial.ping();
}

void SoftFailureUnreceiptedGone::startBtn()
{
		if (convContext->controller->serial.sendPulse(PulseSignals::START_BTN))
		{
			new (this) SoftFailureReceipted(convContext);
		}
		else
		{
			hardFailure();
		}
}

void SoftFailureUnreceiptedGone::resetBtn()
{
	if (convContext->controller->serial.sendPulse(PulseSignals::RESET_BTN))
	{
		//Change state to initializing
		new (this) Initializing(convContext);
	}
	else
	{
		hardFailure();
	}
}

void SoftFailureUnreceiptedGone::hardFailure()
{
	convContext->controller->serial.sendPulse(PulseSignals::HARDFAILURE);
	//Change state to hardFailure unreceipted
	new (this) HardFailureUnreceipted(convContext);
}

void SoftFailureUnreceiptedGone::entryFunction()
{
	convContext->controller->conveyor.stop();
	ConveyorTime::getInstance().stopCounter();

	convContext->controller->statusLight.setColor(Statuslight::RED);
	convContext->controller->statusLight.setFrequency(Statuslight::FLASH_05HZ);

	convContext->controller->panel.revokeLedQ2();

	Logger::log("SoftFailureUnreceiptedGone", "entry");
}
