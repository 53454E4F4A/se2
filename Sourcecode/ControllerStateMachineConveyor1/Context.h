/**
 * Context.h
 *
 *  Created on: 07.05.2014
 *  @author: Ulrich ter Horst
 *  @file: Context.h
 */

#ifndef CONTEXT_H_
#define CONTEXT_H_

#include "ControllerStateMachineConveyor1/Signals.h"
#include "stdint.h"
#include <stack>

using namespace std;


class Controller;
//class ConveyorState;
//class Signals;

class Context : Signals
{
private:
	Signals *actualState;
	stack<uint8_t> stateStack;

public:
	Context(Controller *contr);
	Controller *controller;

	Signals *prevState;

	virtual ~Context();

	virtual void setState(Signals* newState);
	bool goToPrevState();
	void addStateOnStack(uint8_t);

	virtual void inputPuck();
	virtual void puckAtHeightSensor(PulseSignals::PULSE signal);
	virtual void puckAtGate(PulseSignals::PULSE signal);
	virtual void puckAtJunk(PulseSignals::PULSE signal);
	virtual void outputPuck(PulseSignals::PULSE signal);
	virtual void refresh(PulseSignals::PULSE signal);
	virtual void closeGate();
	virtual void checkJunk();
	virtual void conveyorTwoAnswers();
	virtual void leaveCriticalSection();
	virtual void startBtn();
	virtual void stopBtn();
	virtual void resetBtn();
	virtual void softFailure();
	virtual void hardFailure();

protected:
};

#endif /* CONTEXT_H_ */
