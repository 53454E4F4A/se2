/*
 * Context.cpp
 *
 *  Created on: 07.05.2014
 *  @author: Ulrich ter Horst
 */

#include "ControllerStateMachineConveyor1/Context.h"
#include "ControllerStateMachineConveyor1/Operating.h"
#include "ControllerStateMachineConveyor1/PuckInCriticalSection.h"
#include "ControllerStateMachineConveyor1/Stop.h"
#include "ControllerStateMachineConveyor1/TurnPuck.h"
#include "ControllerStateMachineConveyor1/WaitForConveyor2.h"

using namespace std;

Context::Context(Controller *contr):
	controller(contr)
{
	cout << "Context - Constructor called" << endl;

	if(actualState != NULL)
	{
		delete actualState;
		actualState = NULL;
	}
}

Context::~Context()
{
	if(actualState != NULL)
	{
		delete actualState;
		actualState = NULL;
	}
}

void Context::setState(Signals* newState)
{
	actualState = newState;
}

bool Context::goToPrevState(){
	if(stateStack.empty()){
		return false;
	}
	delete actualState;

	switch(stateStack.top()){
	case 0:
		actualState = new Operating(this);
		break;
	case 1:
		actualState = new PuckInCriticalSection(this);
		break;
	case 2:
		actualState = new Stop(this);
		break;
	case 3:
		actualState = new TurnPuck(this);
		break;
	case 4:
		actualState = new WaitForConveyor2(this);
		break;
	default:
		break;
	}

	stateStack.pop();
	return true;
}

void Context::addStateOnStack(uint8_t state){
	stateStack.push(state);

	cout << "stateStack size: " << stateStack.size() << endl;
}

void Context::puckAtHeightSensor(PulseSignals::PULSE signal)
{
	actualState->puckAtHeightSensor(signal);
}

void Context::puckAtGate(PulseSignals::PULSE signal)
{
	actualState->puckAtGate(signal);
}

void Context::outputPuck(PulseSignals::PULSE signal)
{
	actualState->outputPuck(signal);
}

void Context::stopBtn()
{
	actualState->stopBtn();
}

void Context::puckAtJunk(PulseSignals::PULSE signal)
{
	actualState->puckAtJunk(signal);
}

void Context::resetBtn()
{
	actualState->resetBtn();
}

void Context::conveyorTwoAnswers()
{
	actualState->conveyorTwoAnswers();
}

void Context::leaveCriticalSection(){
	actualState->leaveCriticalSection();
}

void Context::inputPuck()
{
	actualState->inputPuck();
}

void Context::closeGate()
{
	actualState->closeGate();
}

void Context::refresh(PulseSignals::PULSE signal)
{
	actualState->refresh(signal);
}

void Context::startBtn()
{
	actualState->startBtn();
}

void Context::checkJunk()
{
	actualState->checkJunk();
}

void Context::softFailure()
{
	actualState->softFailure();
}

void Context::hardFailure()
{
	actualState->hardFailure();
}
