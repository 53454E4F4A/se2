/*
 * EventObject.h
 *
 *  Created on: 30.04.2014
 *      Author: Janek Tichy
 */

#ifndef EVENTOBJECT_H_
#define EVENTOBJECT_H_

#include <stdint.h>
#include "PulseSignals.h"

class EventObject {
public:
	PulseSignals::PULSE eventNumber;
	uint32_t startTime;
	uint32_t timeToWait;
	bool CyclePulse;
	bool everyPulse;
private:

public:

	/**
	 * Return the time with the puck was signed in
	 */
	uint32_t getStartTime() const;

	/**
	 * Set the current time
	 */
	void setStartTime(uint32_t timeCount);

	/**
	 * Return the event number from the puck
	 */
	PulseSignals::PULSE getEventNumber() const;

	/**
	 * Return the time in ms. After this time, the puck will get a pulse
	 */
	uint32_t getTimeToWait() const;

	/**
	 * Return, whether CyclePulse is true, if it is true, the timer will send
	 * periodic a pulse, without to delete the event
	 */
	bool isCyclePulse() const;

	/**
	 * If is the member everyPulse true, then the timer send with every pulse from
	 * the QNX timer a new pulse, independent from the conveyer time.
	 */
	bool isEveryPulse() const;

	/**
	 * Constructor for a normal event with the option for a cycle event :
	 * Is a event declared as cycle, it gives pulses with every new time deadline
	 * Cycle event have to delete by hand
	 */
	EventObject (PulseSignals::PULSE eventNumber, uint32_t startTime, uint32_t timeToWait, bool CyclePulse);

	/**
	 * Principle the same constructor like above with the difference that you get
	 * pulses triggered from the system time, also independent from the conveyer time
	 */
	EventObject (PulseSignals::PULSE eventNumber, uint32_t startTime, uint32_t timeToWait, bool CyclePulse, bool everyPulse);
private:

};

#endif /* EVENTOBJECT_H_ */
