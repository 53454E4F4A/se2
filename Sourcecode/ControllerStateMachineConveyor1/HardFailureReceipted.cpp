/*
 * HardFailureReceipted.cpp
 *
 *  Created on: 12.05.2014
 *  @author: Ulrich ter Horst
 */

#include "ControllerStateMachineConveyor1/HardFailureReceipted.h"
#include "ControllerStateMachineConveyor1/Initializing.h"
#include "ControllerStateMachineConveyor1/Controller.h"
#include "ConveyorTime.h"
using namespace std;

HardFailureReceipted::HardFailureReceipted(Context *context) : ConveyorState(context)
{
	entryFunction();
}

HardFailureReceipted::~HardFailureReceipted()
{

}

void HardFailureReceipted::refresh(PulseSignals::PULSE signal)
{
	convContext->controller->serial.ping();

}

void HardFailureReceipted::conveyorTwoAnswers()
{
	if (!convContext->controller->panel.emergencyPressed())
	{
		if (!convContext->controller->panel.resetPressed() && !convContext->controller->panel.startPressed())
		{
			convContext->controller->serial.sendPulse(PulseSignals::OTHERDEVICEANSWERS);
		}
			new (this) Initializing(convContext);
	}
}

void HardFailureReceipted::startBtn()
{
	resetBtn();
}

void HardFailureReceipted::resetBtn()
{
	if (!convContext->controller->panel.emergencyPressed())
	{
		convContext->controller->serial.sendPulse(PulseSignals::OTHERDEVICEANSWERS);
	}

}

void HardFailureReceipted::closeGate()
{
	convContext->controller->gate.Deny();
}

void HardFailureReceipted::entryFunction()
{

	convContext->controller->conveyor.stop();
	ConveyorTime::getInstance().stopCounter();
	convContext->controller->statusLight.setColor(Statuslight::RED);
	convContext->controller->statusLight.setFrequency(Statuslight::FLASH_PERMANENT);

	convContext->controller->panel.revokeLedQ2();

	Logger::log("HardFailureReceipted", "entry");
}
