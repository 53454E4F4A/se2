/*
 * SoftFailureUnreceipted.h
 *
 *  Created on: 12.05.2014
 *  @author: Ulrich ter Horst
 */

#ifndef SOFTFAILUREUNRECEIPTED_H_
#define SOFTFAILUREUNRECEIPTED_H_

#include "ControllerStateMachineConveyor1/ConveyorState.h"
#include "ControllerStateMachineConveyor1/Context.h"

using namespace std;

class SoftFailureUnreceipted: public ConveyorState
{
public:
	SoftFailureUnreceipted();
	SoftFailureUnreceipted(Context *context);
	virtual ~SoftFailureUnreceipted();

	virtual void inputPuck();
	virtual void puckAtHeightSensor(PulseSignals::PULSE signal);
	//virtual void puckAtGate(PulseSignals::PULSE signal);
	virtual void puckAtJunk(PulseSignals::PULSE signal);
	//virtual void outputPuck(PulseSignals::PULSE signal);
	virtual void refresh(PulseSignals::PULSE signal);
	virtual void closeGate();
	virtual void checkJunk();
	//virtual void conveyorTwoAnswers();
	//virtual void leaveCriticalSection();
	virtual void startBtn();
	//virtual void stopBtn();
	virtual void resetBtn();
	//virtual void softFailure();
	virtual void hardFailure();

	/**
	 * @return void
	 * This function is called by constructor.
	 * it sets:
	 * -gate: deny
	 * -statuslight: all off
	 * -conveyor: stop
	 */
	virtual void entryFunction();

private:
	bool isInInputPuck;
};



#endif /* SOFTFAILUREUNRECEIPTED_H_ */


//TODO ist der Gebrauch von einem SoftFailureUnreceiptedGone noch aktuell? weil den muss man dann noch erstellen
