/**
 * Operating.h
 *
 *  Created on: 30.04.2014
 *  @author: Ulrich ter Horst
 */



#ifndef OPERATING_H_
#define OPERATING_H_

#include "ControllerStateMachineConveyor1/ConveyorState.h"
#include "PulseSignals.h"

using namespace std;

class Operating : public ConveyorState
{
public:
	Operating(Context *context);
	virtual ~Operating();

	uint8_t stateIndex;

	virtual void inputPuck();
	virtual void puckAtHeightSensor(PulseSignals::PULSE signal);
	virtual void puckAtGate(PulseSignals::PULSE signal);
	virtual void puckAtJunk(PulseSignals::PULSE signal);
	virtual void outputPuck(PulseSignals::PULSE signal);
	virtual void refresh(PulseSignals::PULSE signal);
	virtual void closeGate();
	virtual void checkJunk();
	//virtual void conveyorTwoAnswers();
	//virtual void leaveCriticalSection();
	//virtual void startBtn();
	virtual void stopBtn();
	virtual void resetBtn();
	virtual void softFailure();
	virtual void hardFailure();

	/**
	 * @return void
	 * This function is called by constructor.
	 * it sets:
	 * -gate: deny
	 * -statuslight: all off
	 * -conveyor: stop
	 */
private:
	bool timeOut_stopConveyor;
	virtual void entryFunction();
};

#endif /* OPERATING_H_ */

