/*
 * Puck.h
 *
 *  Created on: May 12, 2014
 *      Author: Lars Schwensen
 */

#ifndef PUCK_H_
#define PUCK_H_

#include "PulseSignals.h"
#include "ConveyorTime.h"
#include "HeightSensor.h"
#include "MetalDetector.h"
#include "ConveyorTime.h"
#include "Serial.h"
#include <stdint.h>

#include <string>
#include <sstream>

#define TOLERANCE_TIME 1500
#define TOLERANCE_HEIGHT 150

#define JUNK_PUCK 2800
#define DRILL_PUCK 800
#define NORMAL_PUCK -300

#define DISTANCE_INPUT_HEIGHT 2200
#define DISTANCE_HEIGHT_GATE 800
#define DISTANCE_GATE_JUNK 5000
#define DISTANCE_GATE_OUTPUT 1000
#define DISTANCE_START_HEIGHT 3200

static uint16_t Junk_Puck;
static uint16_t Drill_Puck;
static uint16_t Normal_Puck;

class Puck {
public:

        enum States{
                          NEWPUCK,
                          UNKNOWNDRILL,
                          DRILLONTOP,
                          DRILLONBOTTOM,
                          METALDRILLONTOP,
                          JUNK
          };

          enum MetalMeasuring{
                          UNDEFINED,
                          METAL,
                          NOMETAL,
          };
	Puck();
	Puck(uint8_t id, uint64_t nextSignal, int state, uint8_t turns, uint16_t heightMeasurement1,
	      uint16_t heightMeasurement2, Puck::MetalMeasuring metalMeasurement1, Puck::MetalMeasuring metalMeasurement2);
	virtual ~Puck();

	PulseSignals::PULSE receiveSignal(PulseSignals::PULSE pulse);

	struct  __attribute__ ((__packed__)) uartPuck {
	                uint8_t id;
	                uint64_t nextSignal;
	                int state;
	                uint8_t turns;
	                uint16_t heightMeasurement1;
	                uint16_t heightMeasurement2;
	                MetalMeasuring metalMeasurement1;
	                MetalMeasuring metalMeasurement2;
	};
	struct Puck::uartPuck toMessage();

	static void setReferenceValue(){
		Junk_Puck = HeightSensor::getInstance().getHeightValue();
		Drill_Puck = Junk_Puck + DRILL_PUCK;
		Normal_Puck = Junk_Puck + NORMAL_PUCK;
	}

	string toString();
	void toStringForUser();
private:

	uint8_t id;
	uint64_t nextSignal;

	States state;

	uint8_t turns;

	uint16_t heightMeasurement1;
	uint16_t heightMeasurement2;
	bool heightMeasurement2_OK;
	MetalMeasuring metalMeasurement1;
	MetalMeasuring metalMeasurement2;

	ConveyorTime *convTime;

	void measureHeight();


};

#endif /* PUCK_H_ */
