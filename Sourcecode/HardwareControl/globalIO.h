/*
 * globalIO.h
 *
 *  Created on: 27.03.2014
 *      Author: Yannik Wilkening, tichy
 */

#ifndef GLOBALIO_H_
#define GLOBALIO_H_

#define BASE_ADDRESS		0x0300
#define CTRL_ADDRESS		0x0303

#define INT_ADDRESS			0x0320


#define IO_CONTROL_MASK		0b10001010

#define PORT_A				BASE_ADDRESS
#define PORT_B				(BASE_ADDRESS + 1)
#define PORT_C				(BASE_ADDRESS + 2)

#define CTRL_REG			(BASE_ADDRESS + 3)    

#define ADC_LOW				0x02
#define ADC_HIGH			0x03

//! Interrupt specific
#define INT_CTRL_ADDRESS 	(BASE_ADDRESS + 0x0B)
#define INT_STATUS_ADDRESS 	(BASE_ADDRESS + 0x0F)

#define PB_STATUS      	 	0x02
#define PC_STATUS       	0x08
#define PB_CTRL         	0x02
#define PC_CTRL         	0x04


//! PORT A
/*!
	these defines specify values allowed to be written to port A
*/
#define PA_ENGINE_RIGHT		0b00000001	// 0x01
#define PA_ENGINE_LEFT		0b00000010	// 0x02
#define PA_ENGINE_SLOW		0b00000100	// 0x04
#define PA_ENGINE_STOP		0b00001000	// 0x08
#define PA_GATE_OPEN		0b00010000	// 0x10
#define PA_LIGHT_GREEN		0b00100000	// 0x20
#define PA_LIGHT_YELLOW		0b01000000	// 0x40
#define PA_LIGHT_RED		0b10000000	// 0x80
//! PORT C
/*!
	these defines specify values allowed to be written to port C
*/
#define PC_LED_START		0b00000001 // 0x01
#define PC_LED_RESET		0b00000010 // 0x02

#define PC_LED_Q1		0b00000100 // 0x04
#define PC_LED_Q2		0b00001000 // 0x08
/*!
	these defines specify bit meanings allowed to be read from port C
*/

#define PC_BUTTON_START 0x10
#define PC_BUTTON_STOP 0x20
#define PC_BUTTON_RESET 0x40
#define PC_BUTTON_ESTOP 0x80



#define BUTTON_START_PRESSED(bval) ((bval & PC_BUTTON_START))
#define BUTTON_STOP_PRESSED(bval) (!(bval & PC_BUTTON_STOP))
#define BUTTON_RESET_PRESSED(bval) ((bval & PC_BUTTON_RESET))
#define BUTTON_ESTOP_PRESSED(bval) (!(bval & PC_BUTTON_ESTOP))

//! PORT B
/*!
	these defines specify bit meanings allowed to be read from port B
*/
#define PB_PUCK_AT_IN_SENSOR 0x1
#define PB_PUCK_AT_HEIGHT_SENSOR 0x2
#define PB_PUCK_HEIGHT_OUT_OF_TOLERANCE 0x4
#define PB_PUCK_AT_GATE_SENSOR 0x8
#define PB_PUCK_HAS_METAL 0x10
#define PB_GATE_OPEN 0x20
#define PB_RAMP_FULL 0x40
#define PB_PUCK_AT_OUT_SENSOR 0x80




#define PUCK_AT_IN_SENSOR(bval) (!(bval & PB_PUCK_AT_IN_SENSOR))
#define PUCK_AT_HEIGHT_SENSOR(bval) (!(bval & PB_PUCK_AT_HEIGHT_SENSOR))
#define PUCK_HEIGHT_OUT_OF_TOLERANCE(bval) (!(bval & PB_PUCK_HEIGHT_OUT_OF_TOLERANCE))
#define PUCK_AT_GATE_SENSOR(bval) (!(bval & PB_PUCK_AT_GATE_SENSOR))
#define PUCK_HAS_METAL(bval) ((bval & PB_PUCK_HAS_METAL))
#define GATE_OPEN(bval) ((bval & PB_GATE_OPEN))
#define RAMP_FULL(bval) (!(bval & PB_RAMP_FULL))
#define PUCK_AT_OUT_SENSOR(bval) (!(bval & PB_PUCK_AT_OUT_SENSOR))

//! Analog Digital Converter 
/*
	for converting height measurements

*/
#define	ADC_CONTROL_OFFSET 0x20
#define ADC_CTRL_REG (BASE_ADDRESS+ADC_CONTROL_OFFSET)
#define ADC_CTRL_START_CONVERION 0x10
#define ADC_CTRL_DATA (ADC_CTRL_REG+0x2)
#define ADC_CTRL_STATUS_OFFSET 0x80

#define ADC_CONVERTION_FINISHED(CTRL_REG) (CTRL_REG & ADC_CTRL_STATUS_OFFSET)

//! INTERRUPTS
/*!
	Interrupt Status and Control register offsets and pulse codes
*/

#define IRQ_PORT_B       0x02
#define IRQ_PORT_C       0x08
#define _PULSE_CODE_PORT_B       0x02
#define _PULSE_CODE_PORT_C       0x08

#define OFFS_IR_STATUS 	     0x0F 
#define OFFS_IR_CTRL   		0x0B

#define IR_CTRL_PORT_B         0x02
#define IR_CTRL_PORT_C         0x04

#define IR_STATUS (BASE_ADDRESS+OFFS_IR_STATUS)
#define IR_CTRL (BASE_ADDRESS+OFFS_IR_CTRL)

#define irq_on_b(isr) (isr & IRQ_PORT_B)
#define irq_on_c(isr) (isr & IRQ_PORT_C)


#endif /* GLOBALIO_H_ */
