/*
 * SensorThread.cpp
 */

#include <stdint.h>
#include "SensorThread.h"
#include "globalIO.h"
#include "HWaccess.h"
#include "HAL.h"
#include "HeightSensor.h"
#include "PulseSignals.h"
#include <sstream>
#include "Logger.h"
static int isr_coid;
static int ctrl_coid;

#define INTERRUPT_HANDLER_CHANNEL_INTERN 0
#define INTERRUPT_CHANNEL 11
const struct sigevent*
ISR(void* arg, int id)
{

	struct sigevent *event = (struct sigevent *) arg;
	uint16_t port_val = 0;
	uint8_t irq_msk;

	irq_msk = in8(IR_STATUS) & (IRQ_PORT_B | IRQ_PORT_C);
	out8(IR_STATUS, 0);

	switch (irq_msk)
	{
	case (IRQ_PORT_B | IRQ_PORT_C):
		port_val = (in8(PORT_B) << 8) | (in8(PORT_C));
		SIGEV_PULSE_INIT(event, isr_coid, SIGEV_PULSE_PRIO_INHERIT,
						_PULSE_CODE_PORT_B | _PULSE_CODE_PORT_C, port_val);
		break;
	case IRQ_PORT_B:
		port_val = in8(PORT_B);
		SIGEV_PULSE_INIT(event, isr_coid, SIGEV_PULSE_PRIO_INHERIT,
						_PULSE_CODE_PORT_B, port_val);
		break;
	case IRQ_PORT_C:
		port_val = in8(PORT_C);
		SIGEV_PULSE_INIT(event, isr_coid, SIGEV_PULSE_PRIO_INHERIT,
						_PULSE_CODE_PORT_C, port_val);
		break;

	default:
		event = NULL;
	}
	return event;
}

SensorThread* SensorThread::instance = NULL;

SensorThread::SensorThread()
{

	initInterrupts();
	this->oldval_c = HAL::getInstance().read_byte(PORT_C);
	this->oldval_b = HAL::getInstance().read_byte(PORT_B);
	// signalChid = ChannelCreate(channel);
	// if (signalChid == -1) {
	//     perror("Dispatcher: ChannelCreate signalChid failed");
	//     exit(EXIT_FAILURE);
	// }


}

void SensorThread::setChannel(int chid)
{

	ctrl_coid = ConnectAttach(0, 0, chid, _NTO_SIDE_CHANNEL, 0);
	//cout << "SensorThread - ctrl_coid: " << ctrl_coid << endl;

	if (ctrl_coid == -1)
	{
		perror("SensorThread: ConnectAttach isr_coid failed");
		exit(EXIT_FAILURE);
	}
	else
	{
		instance->start(NULL);
		cout << "SensorThread started" << endl;
	}
}
SensorThread::~SensorThread()
{

	if (instance != NULL)
	{
		delete instance;
		instance = NULL;
	}
}

SensorThread*
SensorThread::getInstance()
{
	if (instance == NULL)
	{
		instance = new SensorThread();
	}
	return instance;
}

void SensorThread::initInterrupts()
{

	isrChid = ChannelCreate(INTERRUPT_HANDLER_CHANNEL_INTERN);
	if (isrChid == -1)
	{
		perror("SensorThread: ChannelCreate isrChid failed");
		exit(EXIT_FAILURE);
	}
	isr_coid = ConnectAttach(INTERRUPT_HANDLER_CHANNEL_INTERN, 0, isrChid, _NTO_SIDE_CHANNEL, 0);
	if (isr_coid == -1)
	{
		perror("SensorThread: ConnectAttach isr_coid failed");
		exit(EXIT_FAILURE);
	}

	SIGEV_PULSE_INIT(&event, isr_coid, SIGEV_PULSE_PRIO_INHERIT, 0, 0);
	interruptId = InterruptAttach(INTERRUPT_CHANNEL, ISR, &event, sizeof(event), 0);
	if (interruptId == -1)
	{
		perror("SensorThread: InterruptAttach failed");
		exit(EXIT_FAILURE);
	}

	out8(INT_STATUS_ADDRESS, 0);

	out8(INT_CTRL_ADDRESS, in8(INT_CTRL_ADDRESS) | 0b00111111);

	out8(INT_CTRL_ADDRESS, in8(INT_CTRL_ADDRESS) & ~(PB_CTRL | PC_CTRL));
	in8(PORT_B);
	in8(PORT_C);
}

void SensorThread::stop()
{
	HAWThread::stop();
	if (-1 == ConnectDetach(ctrl_coid))
	{
		perror("SensorCtrl: ConnectDetach signalCoid failed");
	}
	if (-1 == ConnectDetach(isr_coid))
	{
		perror("SensorCtrl: ConnectDetach isr_coid failed");
	}
	if (-1 == ChannelDestroy(isrChid))
	{
		perror("SensorCtrl: ChannelDestroy isr_chid failed");
	}
	if (-1 == InterruptDetach(interruptId))
	{
		perror("SensorCtrl: InterruptDetach failed");
	}
}

void SensorThread::demux_port_b(uint8_t val)
{



	if (PUCK_AT_IN_SENSOR(val) && (val == 0 || !PUCK_AT_IN_SENSOR(	oldval_b)))
	{
		Logger::log("SensorThread", "Sending In-Sensor Pulse");
		MsgSendPulse(ctrl_coid, SIGEV_PULSE_PRIO_INHERIT, PulseSignals::INPUTPUCK, 0);
	}
	if (PUCK_AT_HEIGHT_SENSOR(val) && (val == 0 || !PUCK_AT_HEIGHT_SENSOR(	oldval_b)))
	{
		MsgSendPulse(ctrl_coid, SIGEV_PULSE_PRIO_INHERIT, PulseSignals::PUCKATHEIGHT,
						PUCK_HEIGHT_OUT_OF_TOLERANCE(val));
	}
	/*if (PUCK_HEIGHT_OUT_OF_TOLERANCE(val))
    {
      MsgSendPulse(signalCoid, SIGEV_PULSE_PRIO_INHERIT, ControllerSignals::PUCKHEIGHTOUTOFTOLERANCE, 0);
    }*/
  if (PUCK_AT_GATE_SENSOR(val)&& (val == 0 || !PUCK_AT_GATE_SENSOR(	oldval_b)))
    {
	  Logger::log("SensorThread", "Sending Puck at Gate Pulse");
      MsgSendPulse(ctrl_coid, SIGEV_PULSE_PRIO_INHERIT, PulseSignals::PUCKATGATE, GATE_OPEN(val) | PUCK_HAS_METAL(val));
    }
  /*if (PUCK_HAS_METAL(val))
    {
      MsgSendPulse(signalCoid, SIGEV_PULSE_PRIO_INHERIT, ControllerSignals::PUCKHASMETAL, 0);
    }
  if (GATE_OPEN(val))
    {
      MsgSendPulse(signalCoid, SIGEV_PULSE_PRIO_INHERIT, ControllerSignals::GATEISOPEN, 0);
    }*/
  if (RAMP_FULL(val)&& (val == 0 || !RAMP_FULL(	oldval_b)))
    {
      MsgSendPulse(ctrl_coid, SIGEV_PULSE_PRIO_INHERIT, PulseSignals::PUCKATJUNK, 0);
    }
  if (PUCK_AT_OUT_SENSOR(val)&& (val == 0 || !PUCK_AT_OUT_SENSOR(	oldval_b)))
    {
      MsgSendPulse(ctrl_coid, SIGEV_PULSE_PRIO_INHERIT, PulseSignals::OUTPUTPUCK, 0);
    }
  this->oldval_b = val;
}
void SensorThread::demux_port_c(uint8_t val)
{

	//printf("%x\n", val);

	if (BUTTON_START_PRESSED(val) && (val == 0 || !BUTTON_START_PRESSED(oldval_c)))
	{
		MsgSendPulse(ctrl_coid, SIGEV_PULSE_PRIO_INHERIT, PulseSignals::START_BTN, 0);
	}
	if (BUTTON_STOP_PRESSED(val) && (val == 0 || !BUTTON_STOP_PRESSED(oldval_c)))
	{
		MsgSendPulse(ctrl_coid, SIGEV_PULSE_PRIO_INHERIT, PulseSignals::STOP_BTN, 0);
	}
	if (BUTTON_RESET_PRESSED(val) && (val == 0 || !BUTTON_RESET_PRESSED(oldval_c)))
	{
		MsgSendPulse(ctrl_coid, SIGEV_PULSE_PRIO_INHERIT, PulseSignals::RESET_BTN, 0);
	}
	if (BUTTON_ESTOP_PRESSED(val) && (val == 0 || !BUTTON_ESTOP_PRESSED(oldval_c)))
	{
		MsgSendPulse(ctrl_coid, SIGEV_PULSE_PRIO_INHERIT, PulseSignals::HARDFAILURE, 0);
	}

	this->oldval_c = val;
}

void SensorThread::execute(void *arg)
{
	stringstream ss;


	struct _pulse pulse;
	while (!isStopped())
	{
		if (-1 == MsgReceivePulse(isrChid, &pulse, sizeof(pulse), NULL))
		{
			if (isStopped())
			{
				break;
			}
			perror("SensorCtrl: MsgReceivePulse");
			exit(EXIT_FAILURE);
		}

		//ss.str("");
		//ss << "Getting pulse: " << pulse.value.sival_int;
		//Logger::log("SensorThread", ss.str());

		switch (pulse.code)
		{
		case (_PULSE_CODE_PORT_C | _PULSE_CODE_PORT_B):
			demux_port_c(pulse.value.sival_int);
			demux_port_b(pulse.value.sival_int >> 8);
			break;
		case _PULSE_CODE_PORT_B:
			demux_port_b(pulse.value.sival_int);
			break;

		case _PULSE_CODE_PORT_C:
			demux_port_c(pulse.value.sival_int);
			break;
		default:

			break;

		}
	}
}

void SensorThread::shutdown()
{

}

bool SensorThread::isInputPuck()
{
	return PUCK_AT_IN_SENSOR(HAL::getInstance().read_byte(PORT_B));
}
bool SensorThread::isOutputPuck()
{
	return PUCK_AT_OUT_SENSOR(HAL::getInstance().read_byte(PORT_B));
}
bool SensorThread::isPuckAtHeight()
{
	return PUCK_AT_HEIGHT_SENSOR(HAL::getInstance().read_byte(PORT_B));
}
bool SensorThread::isPuckAtGate()
{
	return PUCK_AT_GATE_SENSOR(HAL::getInstance().read_byte(PORT_B));
}
bool SensorThread::isRampFull()
{
	return RAMP_FULL(HAL::getInstance().read_byte(PORT_B));
}
