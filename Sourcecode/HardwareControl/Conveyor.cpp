/*
 * Conveyor.cpp
 *
 *  Created on: 31.03.2014
 *      Author: Schwensen
 */

#include <iostream>
#include <stdint.h>
#include "Conveyor.h"
#include "HWaccess.h"
#include "HAL.h"

using namespace std;

Conveyor::Conveyor()
{
  //
}

Conveyor::~Conveyor()
{
  //
}

void Conveyor::forward_normal()
{
	uint8_t mask = PA_ENGINE_RIGHT | PA_ENGINE_SLOW;
	stop();
	HAL::getInstance().set_bit(PORT_A,mask);
}

void Conveyor::backward_normal()
{
	uint8_t mask = PA_ENGINE_LEFT | PA_ENGINE_SLOW;
	stop();
	HAL::getInstance().set_bit(PORT_A,mask);
}

void Conveyor::forward_fast()
{
	uint8_t mask = PA_ENGINE_RIGHT;
	stop();
	HAL::getInstance().set_bit(PORT_A,mask);

}

void Conveyor::backward_fast()
{
	uint8_t mask = PA_ENGINE_LEFT;
	stop();
	HAL::getInstance().set_bit(PORT_A,mask);
}

void Conveyor::stop()
{
	uint8_t mask = PA_ENGINE_LEFT | PA_ENGINE_RIGHT | PA_ENGINE_SLOW;
	HAL::getInstance().clr_bit(PORT_A,mask);
}
