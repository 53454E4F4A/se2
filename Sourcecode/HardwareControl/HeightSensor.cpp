/*
 * HeightSensor.cpp
 *
 *  Created on: 28.04.2014
 *      Author: Yannic
 */

#include <stdint.h>
#include "../Pattern/ScopePattern.h"
#include "HAL.h"
#include "globalIO.h"
#include "HeightSensor.h"

pthread_mutex_t HeightSensor::mtx_ = PTHREAD_MUTEX_INITIALIZER;

HeightSensor::HeightSensor() {
	//
}

HeightSensor& HeightSensor::getInstance() {
	static HeightSensor instance;
	return instance;
}

uint16_t HeightSensor::getHeightValue() {
	ScopePattern lock(&mtx_);
	HAL hal = HAL::getInstance();

	hal.clr_bit(INT_ADDRESS, 1<<7); // clear conversion complete flag
	hal.set_byte(INT_ADDRESS + ADC_LOW, 0x10); // writing 0x10 to ADC_LOW starts new ADC conversion
    while(!(hal.read_byte(INT_ADDRESS) & 0x80)) {
    	// wait for end of conversion
    }

    return hal.read_int(INT_ADDRESS + ADC_LOW); // read int from ADC low byte returns full 16 bit ADC value
}
