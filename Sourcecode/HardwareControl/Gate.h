/*
 * Gate.h
 *
 *  Created on: 31.03.2014
 *      Author: Schwensen
 */

#ifndef GATE_H_
#define GATE_H_

#include "globalIO.h"
#include "HAL.h"
//! HAL - Gate
/*!
	This class is part of the HAL and represents the Gate.
	
	Open the gate, so the items can move throw the gate ( pass )
	Close the gate, so the items get out ( deny )
 	
	Uses Core functions of the HAL class.
	
*/
class Gate {
public:
	Gate();
	virtual ~Gate();

	/**

	 */
	void Pass();
	void Deny();
	bool junkIsFull();
private:
};

#endif /* GATE_H_ */
