/*
 * SerialThread.h
 *
 *  Created on: 09.04.2014
 *      Author: abd264
 *  Description:
 *  This is a Thread, that runs the USART (Serial class).
 *  For more information see Serial.cpp/h
 */

#ifndef SERIALTHREAD_H_
#define SERIALTHREAD_H_

#include "../lib/HAWThread.h"
#include "ScopePattern.h"
#include "../HardwareControl/Serial.h"
#include "PulseSignals.h"
#include "Puck.h"

#include "Logger.h"

class SerialThread : public thread::HAWThread
{
	public:
	SerialThread();
	~SerialThread();

	bool sendPulse(PulseSignals::PULSE signal);
	bool ping();
	bool transferPuck(Puck *puck);
	void setChannel(int chid);

	private:
	//! entrypoint
	/*!
		implementation for HAWThread
	*/
	virtual void execute(void*);
	//! shutdown callback
	/*!
		called after execute() returns
	*/
	virtual void shutdown();

    static pthread_mutex_t mtx_;

};

#endif /* SERIALTHREAD_H_ */
