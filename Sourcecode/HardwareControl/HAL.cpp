/*
 * HAL.cpp
 *
 *  Created on: 28.03.2014
 *      Author: tichy
 *
 *   Necessary and important !! : Before you can use this class
 *   you have to get the access privileges from the QNX system for your thread or what ever !!
 *
 *   It is something like this call :
 *
 *   if(ThreadCtl(_NTO_TCTL_IO,0)==-1){
 *   	"you don't got hardware access"
 *   }
 */

#include <stdint.h>
#include "HAL.h"
#include "../lib/HWaccess.h" // header hinzufügen
#include "../Pattern/ScopePattern.h"

pthread_mutex_t HAL::mtx_ = PTHREAD_MUTEX_INITIALIZER;

HAL& HAL::getInstance(){
	static HAL instance;
	return instance;
}

HAL::HAL(){
	set_byte(CTRL_ADDRESS, IO_CONTROL_MASK); // init IO direction
}

void HAL::set_bit(uint16_t port, uint8_t mask){
	ScopePattern lock(&mtx_);
	out8(port,in8(port)| mask);
}

void HAL::clr_bit(uint16_t port, uint8_t mask){
	ScopePattern lock(&mtx_);
	out8(port,in8(port)&~mask);
}

uint8_t HAL::read_bit(uint16_t port, uint8_t mask){
	ScopePattern lock(&mtx_);
	return (in8(port)& mask);
}

uint8_t HAL::read_byte(uint16_t port){
	ScopePattern lock(&mtx_);
	return in8(port);
}

void HAL::set_byte(uint16_t port, uint8_t val){
	ScopePattern lock(&mtx_);
	out8(port,val);
}

void HAL::set_int(uint16_t port, uint16_t val){
	ScopePattern lock(&mtx_);
	out16(port,val);
}

uint16_t HAL::read_int(uint16_t port){
	ScopePattern lock(&mtx_);
	return in16(port);
}





