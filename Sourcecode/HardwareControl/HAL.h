﻿/*
 * HAL.h
 *
 *  Created on: 28.03.2014
 *      Author: tichy
 */

#ifndef HAL_H_
#define HAL_H_

#include <pthread.h>
#include <stdint.h>
#include "globalIO.h"
//! HAL - Bus functions
/*!
	This Class offers basic functions to write on ports in a threadsafe manner.
	It is implemented as singleton
	
	Example: HAL::getInstace()->clr_bit(PORT_A,PA_LIGHT_YELLOW);
*/
class HAL {

private:

	static pthread_mutex_t mtx_; //!< Mutex for all ports
	HAL(); //!< private Konstruktor (Singleton)

public:

	/**
	* Implements the singleton pattern with a static reference
	*
	* Example: HAL::getInstace()->clr_bit(PORT_A,PA_LIGHT_YELLOW)
	*
	* @return instance of HAL
	*/
	static HAL& getInstance(); //!< returns HAL instance


	 /**
	* Set Bits of a Port to logical 1
	* @param port Port on which to set desired bits (PORT_A, PORT_B, PORT_C)
	* @param mask 8-bit bitmask
	* @return void
	*/
	void set_bit(uint16_t port, uint8_t mask);
	 /**
	* Set Bits of a Port to logical 0
	* @param port Port on which to set desired bits (PORT_A, PORT_B, PORT_C)
	* @param mask 8-bit bitmask
	* @return void
	*/
	void clr_bit(uint16_t port, uint8_t mask);
	 /**
	* Read Bits of a Port
	* @param port Port on which to read desired bits (PORT_A, PORT_B, PORT_C)
	* @param mask 8-bit bitmask
	* @return value of given bits
	*/
	uint8_t read_bit(uint16_t port, uint8_t mask);
	 /**
	* Reads 8 bit from specified port
	* @param port Port on which to read  (PORT_A, PORT_B, PORT_C)
	* @return value of bits on given port
	*/
	uint8_t read_byte(uint16_t port);
	 /**
	* Set Bits of a Port
	* @param port Port on which to set bits (PORT_A, PORT_B, PORT_C)
	* @param val 8-bit data
	* @return void
	*/
	void set_byte(uint16_t port, uint8_t val);
	 /**
	* Set Bits of a Port
	* @param port Port on which to set bits (PORT_A, PORT_B, PORT_C)
	* @param val 16-bit data
	* @return void
	*/
	void set_int(uint16_t port, uint16_t val);
	 /**
	* Reads 16 bit from specified port
	* @param port Port on which to read  (PORT_A, PORT_B, PORT_C)
	* @return value of bits on given port
	*/
	uint16_t read_int(uint16_t port);
};

#endif /* HAL_H_ */
