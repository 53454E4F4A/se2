/*
 * Serial.h
 *
 *  Created on: 09.04.2014
 *  Author: Ulrich ter Horst
 *
 *  Description:
 *  This Module is used to control the USART interface.
 *  Both GEME machines use USART to communicate with eachother.
 *  This is needed to exchange item data and emergency messages (E_halt).
 */

#ifndef SERIAL_H_
#define SERIAL_H_

#include <string.h>
#include <termios.h>
#include <fcntl.h>
#include <stdint.h>
#include <string>
#include "PulseSignals.h"
#include "Puck.h"
using namespace std;

class Serial
{
	public:
	static Serial& getInstance();


	//This defines the architecture of a USART message.
	enum MESSAGE_TYPE {
		USART_MESSAGE_PUCK = 0,
		USART_MESSAGE_PULSE = 1,
		USART_MESSAGE_PING = 2	
	};
	
	
	

	struct  __attribute__ ((__packed__)) uartPulse {
	                        PulseSignals::PULSE pulse;
	                };
	struct  __attribute__ ((__packed__)) uartPing{
	                };

	
	//This function sends a usartMessage:
	int32_t usartTx(void *sendData, uint16_t amountOfData);

	//This function receives a usartMessage.
	//A size condition is used, so that this
	//function expects a specific amount of Bytes
	//and after receiving this amount, it returns
	//the received data.
	int32_t usartRx(void *recData, uint16_t amountOfData, int timeout);

	private:
	Serial();
	~Serial();

	int fd_to_USART;
	//This function initializes the USART interface
	//Baudrate: 19200
	//no Stop Bit
	//no Parity Bit
	int32_t initSerial();
};


#endif /* SERIAL_H_ */
