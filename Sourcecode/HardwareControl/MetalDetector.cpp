

#include <stdint.h>
#include "HAL.h"
#include "globalIO.h"
#include "MetalDetector.h"

pthread_mutex_t MetalDetector::mtx_ = PTHREAD_MUTEX_INITIALIZER;

MetalDetector::MetalDetector() {
	//
}

MetalDetector& MetalDetector::getInstance() {
	static MetalDetector instance;
	return instance;
}

bool MetalDetector::puckHasMetal() {
	
	return HAL::getInstance().read_bit(PORT_B, PB_PUCK_HAS_METAL);
	
}
