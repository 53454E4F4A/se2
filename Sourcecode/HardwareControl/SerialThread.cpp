/*
 * SerialThread.cpp
 *
 *  Created on: 09.04.2014
 *  Author: Ulrich ter Horst & Yannic Wilkening
 *
 *  Description:
 *  This is a Thread, that runs the USART (Serial class).
 *  For more information see Serial.cpp/h
 */

#include <SerialThread.h>
#include "Serial.h"

#define TIMEOUT_INTERVAL 60
static int ctrl_coid;
pthread_mutex_t SerialThread::mtx_ = PTHREAD_MUTEX_INITIALIZER;

SerialThread::SerialThread()
{
	//
}

SerialThread::~SerialThread()
{

}

void SerialThread::execute(void*)
{
    Puck *rPuck = 0, *lPuck = 0;
	uint8_t *bytes = 0;

	ScopePattern lock(&mtx_);

	Serial::MESSAGE_TYPE t;
	int bytes_recieved;

	Logger::log("SerialThread", "starting");

	while (!this->isStopped())
	{
		bytes_recieved = Serial::getInstance().usartRx((void*)&t, sizeof(Serial::MESSAGE_TYPE), TIMEOUT_INTERVAL);
		if(bytes_recieved == -1) {
			perror("SerialThread: usartRx type failed: ");
		} else {
			if(bytes_recieved == sizeof(Serial::MESSAGE_TYPE)) { // no timeout
				//Logger::log("SerialThread", "Empfange MessageType");
				switch(t){
					case Serial::USART_MESSAGE_PULSE:
						//Logger::log("SerialThread", "type Pulse");
						Serial::uartPulse pulse;
						bytes_recieved = Serial::getInstance().usartRx((void*)&pulse, sizeof(Serial::uartPulse), TIMEOUT_INTERVAL);

						if(bytes_recieved == -1) {
							perror("SerialThread: usartRx pulse failed ");
						}
						if(bytes_recieved == sizeof(Serial::uartPulse)) {
							cout <<"MY NUMBER: " << ctrl_coid << endl;
							MsgSendPulse(ctrl_coid, SIGEV_PULSE_PRIO_INHERIT, pulse.pulse, 0);
						} else {
							Logger::log("SerialThread", "Timeout");
							pulse.pulse = PulseSignals::HARDFAILURE;
							MsgSendPulse(ctrl_coid, SIGEV_PULSE_PRIO_INHERIT, pulse.pulse, 0);
						}
						break;
					case Serial::USART_MESSAGE_PUCK:

						Logger::log("SerialThread", "type Puck");

						Puck::uartPuck puck;
						bytes_recieved = Serial::getInstance().usartRx((void*)&puck, sizeof(Puck::uartPuck), TIMEOUT_INTERVAL);
						if(bytes_recieved == -1) {
							perror("SerialThread: usartRx puck  failed ");
						}

						if(bytes_recieved == sizeof(Puck::uartPuck)) {

							rPuck = new Puck(puck.id, puck.nextSignal, puck.state, puck.turns,
									puck.heightMeasurement1, puck.heightMeasurement2,
									puck.metalMeasurement1, puck.metalMeasurement2);

							MsgSendPulse(ctrl_coid, SIGEV_PULSE_PRIO_INHERIT, PulseSignals::RECEIVEDPUCKINFORMATION, (int)rPuck);
						} else {
							Logger::log("SerialThread", "Timeout");
							pulse.pulse = PulseSignals::HARDFAILURE;
							MsgSendPulse(ctrl_coid, SIGEV_PULSE_PRIO_INHERIT, pulse.pulse, 0);
						}

						break;
					case Serial::USART_MESSAGE_PING:
						//Logger::log("SerialThread", "type Ping");

						Serial::uartPing p;
						bytes_recieved = Serial::getInstance().usartRx(&p, sizeof(Serial::uartPing), TIMEOUT_INTERVAL);

						if(bytes_recieved == -1) {
							perror("SerialThread: usartRx ping  failed ");
						}

						if (bytes_recieved == sizeof(Serial::uartPing)) {
							/*
								TODO: handle PING
							 */
						} else {
							Logger::log("SerialThread", "Timeout");
							pulse.pulse = PulseSignals::HARDFAILURE;
							MsgSendPulse(ctrl_coid, SIGEV_PULSE_PRIO_INHERIT, pulse.pulse, 0);
						}


						break;
					default:
						Logger::log("SerialThread", "Unknown MessageType");
						break;
				}
			}  else {
				Serial::uartPulse pulse;
				cout <<"SERIAL TIMEOUT" << endl;
				pulse.pulse = PulseSignals::HARDFAILURE;
				MsgSendPulse(ctrl_coid, SIGEV_PULSE_PRIO_INHERIT, pulse.pulse, 0);
			}
		}
	}
}

bool SerialThread::sendPulse(PulseSignals::PULSE signal){

	// See Serial.h
	// Create Message
        Serial::MESSAGE_TYPE t;
	t = Serial::USART_MESSAGE_PULSE;
	
	// Create Data
	Serial::uartPulse message;
	message.pulse = signal;

	// Send Type
	Serial::getInstance().usartTx((void*)&t, sizeof(Serial::MESSAGE_TYPE));
	
	// Send Message
	Serial::getInstance().usartTx((void*)&message, sizeof(Serial::uartPulse));
	
	return true;
}

bool SerialThread::ping(){

	Serial::MESSAGE_TYPE t = Serial::USART_MESSAGE_PING;
	
	Serial::uartPing message;
	
	Serial::getInstance().usartTx((void*)&t, sizeof(Serial::MESSAGE_TYPE));
	Serial::getInstance().usartTx((void*)&message, sizeof(Serial::uartPing));
	return true;
}

bool SerialThread::transferPuck(Puck *puck){

    int32_t sentBytes = 0;
	Serial::MESSAGE_TYPE t = Serial::USART_MESSAGE_PUCK;
	Puck::uartPuck message = puck->toMessage();

	sentBytes = Serial::getInstance().usartTx(&t, sizeof(Serial::MESSAGE_TYPE));
	if(sentBytes != sizeof(Serial::MESSAGE_TYPE)) {
	  perror("SerialThread: TransferPuck");
	  return false;
	}
	sentBytes = Serial::getInstance().usartTx(&message, sizeof(Puck::uartPuck));
	if(sentBytes != sizeof(Puck::uartPuck)) {
	  perror("SerialThread: TransferPuck");
	  return false;
	}
	return true;
}

void SerialThread::setChannel(int chid)
{

   ctrl_coid = ConnectAttach(0, 0, chid,_NTO_SIDE_CHANNEL, 0);
   if (ctrl_coid == -1)
     {
       perror("SerialThread: ConnectAttach isr_coid failed");
       exit(EXIT_FAILURE);
     }
}

void SerialThread::shutdown()
{

  if (-1 == ConnectDetach(ctrl_coid))
  {
     perror("SerialThread: ConnectDetach signalCoid failed");
  }

  Logger::log("SerialThread", "shutdown");

}
