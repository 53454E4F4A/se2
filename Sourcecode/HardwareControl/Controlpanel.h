/*
 * Controlpanel.h
 *
 *  Created on: 31.03.2014
 *      Author: abl427
 */

#ifndef CONTROLPANEL_H_
#define CONTROLPANEL_H_

#include "globalIO.h"
#include "HAL.h"
//! HAL - Controlpanel
/*!
 This class is part of the HAL and represents the control panel.

 Turn the specific lights from the button on

 Uses Core functions of the HAL class.

 */
class Controlpanel
{
public:
  Controlpanel();
  virtual ~Controlpanel();

  void setLedQ1();
  void revokeLedQ1();

  void setLedQ2();
  void revokeLedQ2();

  void setLedStart();
  void revokeLedStart();

  void setLedReset();
  void revokeLedReset();
  
  bool emergencyPressed();
	bool startPressed();
	bool stopPressed();
	bool resetPressed();
	
private:
  void revokeAll();
};

#endif /* CONTROLPANEL_H_ */
