/*
 * HeightSensor.h
 *
 *  Created on: 28.04.2014
 *      Author: Yannic
 */

#ifndef HEIGHTSENSOR_H_
#define HEIGHTSENSOR_H_

#include "stdint.h"

class HeightSensor {
public:
	static HeightSensor& getInstance();
	uint16_t getHeightValue();
private:
	static pthread_mutex_t mtx_; //!< Mutex for ADC access

	HeightSensor();
};

#endif /* HEIGHTSENSOR_H_ */
