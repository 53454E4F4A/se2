/*
 * Controlpanel.cpp
 *
 *  Created on: 31.03.2014
 *      Author: Lars Schwensen
 */

#include <iostream>
#include <stdint.h>
#include "Controlpanel.h"
#include "HWaccess.h"
#include "HAL.h"

using namespace std;

Controlpanel::Controlpanel()
{
	revokeAll();
}

Controlpanel::~Controlpanel()
{
	revokeAll();
}

void Controlpanel::setLedQ1()
{
	HAL::getInstance().set_bit(PORT_C, PC_LED_Q1);
}

void Controlpanel::revokeLedQ1()
{
	HAL::getInstance().clr_bit(PORT_C, PC_LED_Q1);
}

void Controlpanel::setLedQ2()
{
	HAL::getInstance().set_bit(PORT_C, PC_LED_Q2);
}

void Controlpanel::revokeLedQ2()
{
	HAL::getInstance().clr_bit(PORT_C, PC_LED_Q2);
}

void Controlpanel::setLedStart()
{
	HAL::getInstance().set_bit(PORT_C, PC_LED_START);
}

void Controlpanel::revokeLedStart()
{
	HAL::getInstance().clr_bit(PORT_C, PC_LED_START);
}

void Controlpanel::setLedReset()
{
	HAL::getInstance().set_bit(PORT_C, PC_LED_RESET);
}

void Controlpanel::revokeLedReset()
{
	HAL::getInstance().clr_bit(PORT_C, PC_LED_RESET);
}

void Controlpanel::revokeAll()
{
	revokeLedQ1();
	revokeLedQ2();

	revokeLedStart();
	revokeLedReset();
}

bool Controlpanel::emergencyPressed() {
	return BUTTON_ESTOP_PRESSED(HAL::getInstance().read_byte(PORT_C));
}
bool Controlpanel::resetPressed() {
	return BUTTON_RESET_PRESSED(HAL::getInstance().read_byte(PORT_C));
}
bool Controlpanel::stopPressed() {
	return BUTTON_STOP_PRESSED(HAL::getInstance().read_byte(PORT_C));
}
bool Controlpanel::startPressed() {
	return BUTTON_START_PRESSED(HAL::getInstance().read_byte(PORT_C));
}
