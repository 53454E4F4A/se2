/*
 * Serial.cpp
 *
 *  Created on: 09.04.2014
 *  Author: Ulrich ter Horst
 *
 *  Description:
 *  This Module is used to control the USART interface.
 *  Both GEME machines use USART to communicate with eachother.
 *  This is needed to exchange item data and emergency messages (E_halt).
 */

#include <Serial.h>
#include <iostream>
using namespace std;

const char pathToDevice[] = "/dev/ser1";

Serial::Serial()
{
	fd_to_USART = open(pathToDevice, O_RDWR);
	initSerial();
}


Serial& Serial::getInstance()
{
	//this is a static variable, so the constructor
	//is called once, fully automaticly
	static Serial instance;
	return instance;
}


//private Constructor (Singleton)
Serial::~Serial()
{
	close(fd_to_USART);
}


int32_t Serial::initSerial()
{
	if(fd_to_USART < 0)
	{
		cout << "unable to open USART-device: " << pathToDevice << endl;
		return (int32_t)fd_to_USART;
	}
	struct termios config;
	tcflush(fd_to_USART, TCIOFLUSH);
	tcgetattr(fd_to_USART,&config);
	cfsetispeed(&config,B19200);
	cfsetospeed(&config,B19200);
	config.c_cflag &= ~CSIZE;
	config.c_cflag &= ~CSTOPB;
	config.c_cflag &= ~PARENB;
	config.c_cflag |= CS8;
	config.c_cflag |= CREAD;
	config.c_cflag |= CLOCAL;
	tcsetattr(fd_to_USART, TCSANOW, &config);
	return 0;
}



int32_t Serial::usartRx(void *recData, uint16_t amountOfData, int timeout)
{
	/**
	 * useful knowledge for readcond()
	 * http://www.qnx.com/developers/docs/6.5.0/index.jsp?topic=%2Fcom.qnx.doc.neutrino_lib_ref%2Fr%2Freadcond.html
	 * if an error occurred, errno is set
	 * */
	uint32_t result = readcond(fd_to_USART, recData, amountOfData, amountOfData,0, timeout);
	return result;
}



int32_t Serial::usartTx(void *sendData, uint16_t amountOfData)
{
	/**
	 * http://www.qnx.com/developers/docs/6.5.0/index.jsp?topic=%2Fcom.qnx.doc.neutrino_lib_ref%2Fw%2Fwrite.html
	 * if an error occurred, errno is set
	 * returns the number of successfully written bytes, or -1
	 */
	return (uint32_t)write(fd_to_USART, sendData, amountOfData);
}

