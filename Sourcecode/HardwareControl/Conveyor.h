/*
 * Conveyor.h
 *
 *  Created on: 31.03.2014
 *      Author: Schwensen
 */

#ifndef CONVEYOR_H_
#define CONVEYOR_H_

#include "globalIO.h"
#include "HAL.h"
//! HAL - Conveyor
/*!
	This class is part of the HAL and represents the Conveyor.
	
	Let the conveyer move forward and backward.
	For every direction in the speed fast and normal
	
	Uses Core functions of the HAL class.
	@See HAL
	
*/
class Conveyor {
public:
	Conveyor();
	~Conveyor();
	
	/**
	* Let the conveyer move forward in normal speed.
	*
	* @return void
	*/
	void forward_normal();
	/**
	* Let the conveyer move backward in normal speed.
	*
	* @return void
	*/
	void backward_normal();
	/**
	* Let the conveyer move forward in fast speed.
	*
	* @return void
	*/
	void forward_fast();
	/**
	* Let the conveyer move backward in fast speed.
	*
	* @return void
	*/
	void backward_fast();
	/**
	* Let the conveyer stop.
	*
	* @return void
	*/
	void stop();

private:
	//
};

#endif /* CONVEYOR_H_ */
