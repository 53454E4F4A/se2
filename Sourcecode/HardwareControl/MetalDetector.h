

#ifndef METALDETECTOR_H_
#define METALDETECTOR_H_

class MetalDetector {
public:
	static MetalDetector& getInstance();
	bool puckHasMetal();
private:
	static pthread_mutex_t mtx_; //!< Mutex for ADC access
	MetalDetector();
};

#endif /* METALDETECTOR_H_ */
