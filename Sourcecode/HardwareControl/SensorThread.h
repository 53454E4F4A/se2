/*
 * SensorThread.h
 */

#ifndef SensorThread_H_
#define SensorThread_H_

#include <stdint.h>
#include "HAWThread.h"

const struct sigevent * ISR (void *arg, int id);

class SensorThread: public thread::HAWThread {
    private:
        static SensorThread* instance;

        int isrChid;
        int interruptId;
        struct sigevent event;
        int ctrlChid;

        SensorThread();

    public:
        ~SensorThread();
        static SensorThread* getInstance();
        void setChannel(int chid);
        void stop(); // HAWThread: stop -> virtual
        void initInterrupts();
		
		bool isInputPuck();
		bool isOutputPuck() ;
		bool isPuckAtHeight(); 
		bool isPuckAtGate();
		bool isRampFull();
		
    protected:
        virtual void execute(void* arg);
        virtual void shutdown();

    private:
        uint8_t oldval_b;
        uint8_t oldval_c;
        void demux_port_c(uint8_t val);
        void demux_port_b(uint8_t val);
};


#endif /* SensorThread_H_ */
