/*
 * Gate.cpp
 *
 *  Created on: 31.03.2014
 *      Author: Schwensen
 */
#include <iostream>
#include <stdint.h>
#include "Gate.h"
#include "HWaccess.h"
#include "HAL.h"

using namespace std;

Gate::Gate()
{
	//
}

Gate::~Gate()
{
	Deny();
}

void Gate::Pass()
{
	HAL::getInstance().set_bit(PORT_A,PA_GATE_OPEN);
}
void Gate::Deny()
{
	HAL::getInstance().clr_bit(PORT_A,PA_GATE_OPEN);
}

bool Gate::junkIsFull() {
	return RAMP_FULL(HAL::getInstance().read_byte(PORT_B));
}