/*
 * Statuslight.h
 *
 *  Created on: 30.03.2014
 *      Author: Schwensen
 */

#ifndef STATUSLIGHT_H_
#define STATUSLIGHT_H_

#include <stdint.h>
#include <unistd.h>

//#include <pthread.h>
#include "../lib/HAWThread.h"
#include "globalIO.h"
#include "./HAL.h"
//! HAL - Statuslight
/*!
	This class is part of the HAL and represents the Statuslight.
	
	extending HAWThread, the entrypoint is execute()
	 
    Turn the status lights on and off in the colors
    red, yellow and green
  
	
	Uses Core functions of the HAL class.
	
*/
class Statuslight : public thread::HAWThread {

public:
	 /**
	* available colors
	* to use with setColor()
	*/
    enum color
    {

        GREEN = PA_LIGHT_GREEN, /**< GREEN */
        YELLOW = PA_LIGHT_YELLOW, /**< YELLOW */
        RED = PA_LIGHT_RED, /**< RED */
		OFF = 0 	 /**< Permanent */
    };
	 /**
	* available frequencies
	* to use with setFrequenzy()
	*/
    enum frequency
    {
        FLASH_1HZ = 500000, /**< 1.0 Hz */
        FLASH_05HZ = 1000000, /**< 0.5 Hz */
        FLASH_PERMANENT = 0	 /**< Permanent  */
	
    };


    Statuslight();
    ~Statuslight();
	
	/**
	* sets used color
	*
	* @return void
	*/
    void setColor(color usedColor);
		
	/**
	* sets used frequency
	*
	* @return void
	*/
    void setFrequency(frequency flashFrequenzy);

private:
	/**
	* turns on the lights specified in usedColor
	*
	* @return void
	*/
    color usedColor;
    void statusAllOff();
	//! Specifies which lights to use
	/*!
		this variable is read by statuslightOn and statuslightOff
	*/

	//! Specifies which frequency to use
	/*!
		this variable is read by setFrequenzy
	*/
    frequency usedFrequency;

    static pthread_mutex_t mtx_;
	//! entrypoint 
	/*!
		implementation for HAWThread
	*/
    virtual void execute(void*);
	//! shutdown callback 
	/*!
		called after execute() returns
	*/
    virtual void shutdown();
};

#endif /* HAL_H_ */
