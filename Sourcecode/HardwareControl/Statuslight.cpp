/*
 * Statuslight.cpp
 *
 *  Created on: 30.03.2014
 *      Author: Schwensen
 */

#include "Statuslight.h"
#include "ScopePattern.h"
#include "HAL.h"

pthread_mutex_t Statuslight::mtx_ = PTHREAD_MUTEX_INITIALIZER;

Statuslight::Statuslight()
{

	setFrequency(FLASH_PERMANENT);

	start(NULL);
}

Statuslight::~Statuslight()
{

}

void Statuslight::setColor(color usedColor)
{
	uint8_t pa = HAL::getInstance().read_byte(PORT_A);
	pa &= ~(PA_LIGHT_GREEN | PA_LIGHT_RED | PA_LIGHT_YELLOW);
	pa |= usedColor;
	HAL::getInstance().set_byte(PORT_A, pa);

	this->usedColor = usedColor;
}

void Statuslight::setFrequency(frequency flashFrequenzy)
{
	usedFrequency = flashFrequenzy;
}

void Statuslight::execute(void*)
{
	ScopePattern lock(&mtx_);

	while (!isStopped())
	{

		if (usedFrequency)
		{
			//cout << "Statuslight-exec: " << usedFrequency << endl;
			usleep(usedFrequency);
			//uint8_t pa = HAL::getInstance().read_bit(PORT_A, PA_LIGHT_RED | PA_LIGHT_GREEN | PA_LIGHT_YELLOW);
			HAL::getInstance().clr_bit(PORT_A, PA_LIGHT_RED | PA_LIGHT_GREEN | PA_LIGHT_YELLOW);
			usleep(usedFrequency);
			HAL::getInstance().set_bit(PORT_A, usedColor);
		}
	}
}

void Statuslight::statusAllOff()
{

	HAL::getInstance().clr_bit(PORT_A, PA_LIGHT_RED | PA_LIGHT_GREEN | PA_LIGHT_YELLOW);
}
void Statuslight::shutdown()
{
	statusAllOff();
	cout << "Statuslight thread shutdown" << endl;
}

