/*
 * Puck.cpp
 *
 *  Created on: May 12, 2014
 *      Author: Lars Schwensen
 */

#include <Puck.h>
#include <string.h>
#include "Logger.h"

static uint8_t idCounter = 0;

Puck::Puck()
{
	id = idCounter++;
	nextSignal = convTime->getInstance().getTimeCount() + DISTANCE_INPUT_HEIGHT;

	cout << "(2) Puck: " <<(int)id<< " wurde erzeugt! next Signal: " << nextSignal << endl;
	state = NEWPUCK;

	turns = 0;

	heightMeasurement1 = 0;
	heightMeasurement2 = 0;

	metalMeasurement1 = UNDEFINED;
	metalMeasurement2 = UNDEFINED;

	heightMeasurement2_OK = false;

	Junk_Puck = 2800;
	Drill_Puck = 3600;
	Normal_Puck = 2500;
}
Puck::Puck(uint8_t id, uint64_t nextSignal, int state, uint8_t turns, uint16_t heightMeasurement1, uint16_t heightMeasurement2, Puck::MetalMeasuring metalMeasurement1, Puck::MetalMeasuring metalMeasurement2)
{
        this->id = id;
        this->nextSignal = convTime->getInstance().getTimeCount() + DISTANCE_START_HEIGHT;
        cout << "(2) Puck: " <<(int)id<< " wurde empfangen! next Signal: " << nextSignal << endl;

        switch(state)
        {
        case 0:
        	Logger::log("Puck", "received NEWPUCK");
        	this->state = Puck::NEWPUCK;
        	break;
        case 1:
        	Logger::log("Puck", "received UNKNOWNDRILL");
        	this->state =Puck::UNKNOWNDRILL;
        	break;
        case 2:
        	Logger::log("Puck", "received DRILLONTOP");
        	this->state =Puck::DRILLONTOP;
        	break;
        case 3:
        	Logger::log("Puck", "received DRILLONBOTTOM");
        	this->state =Puck::DRILLONBOTTOM;
        	break;
        case 4:
        	Logger::log("Puck", "received METALDRILLONTOP");
        	this->state =Puck::METALDRILLONTOP;
        	break;
        case 5:
        	Logger::log("Puck", "received JUNK");
        	this->state =Puck::JUNK;
        	break;
        }

        this->turns = turns;

        this->heightMeasurement1 = heightMeasurement1;
        this->heightMeasurement2 = heightMeasurement2;

        this->metalMeasurement1 = metalMeasurement1;
        this->metalMeasurement2 = metalMeasurement2;

        Junk_Puck = 2800;
        Drill_Puck = 3600;
        Normal_Puck = 2500;
}
Puck::~Puck()
{
}

PulseSignals::PULSE Puck::receiveSignal(PulseSignals::PULSE pulse)
{
	cout << "(3) Puck: " <<(int)id<< " empfaengt Pulse: " << pulse << endl;

	uint64_t time = convTime->getInstance().getTimeCount();
	uint64_t timeDif;

	if(time > nextSignal)
	{
		timeDif = time - nextSignal;
		cout << "(4) Puck: " <<(int)id<< " Zeitdifferenz zur Erwartung: " << timeDif << endl;
	}
	else
	{
		timeDif = nextSignal - time;
		cout << "(5) Puck: " <<(int)id<< " Zeitdifferenz zur Erwartung: -" << timeDif << endl;
	}

	switch(state)
	{
		case NEWPUCK:
			switch(pulse)
			{
					case PulseSignals::INPUTPUCK:
						cout << "(6) Puck: " <<(int)id<< " (NEWPUCK) empfaengt: INPUTPUCK" << endl;
					break;
					case PulseSignals::PUCKATHEIGHT:
						cout << "(7) Puck: " <<(int)id<< " (NEWPUCK) empfaengt: PUCKATHEIGHT" << endl;

					if(timeDif > TOLERANCE_TIME)
					{
						cout << "(8) Puck: " <<(int)id<< " (NEWPUCK) ist OUTOFTIME" << endl;
						return PulseSignals::NOTOK;
					}

					heightMeasurement1 = HeightSensor::getInstance().getHeightValue();

					if(heightMeasurement1 <= Junk_Puck + TOLERANCE_HEIGHT && heightMeasurement1 >= Junk_Puck - TOLERANCE_HEIGHT)
					{
						//Puck is Junk
						cout << "(9) Puck: " <<(int)id<< " aendert State von NEWPUCK zu JUNK" << endl;
						state = JUNK;
					}
					else if(heightMeasurement1 > Junk_Puck + TOLERANCE_HEIGHT)
					{
						//Puck has got a drill
						cout << "(10) Puck: " <<(int)id<< " aendert State von NEWPUCK zu DRILLONTOP" << endl;
						state = DRILLONTOP;
					}
					else
					{
						//No Drill found
						cout << "(11) Puck: " <<(int)id<< " aendert State von NEWPUCK zu UNKNOWNDRILL" << endl;
						state = UNKNOWNDRILL;
					}

					nextSignal = time + DISTANCE_HEIGHT_GATE;

					cout << "(12) Puck: " <<(int)id<< " gibt OK zurueck" << endl;
					return PulseSignals::OK;

					break;
				case PulseSignals::PUCKATGATE:
					cout << "(13) Puck: " <<(int)id<< " (NEWPUCK) empfaengt: PUCKATGATE" << endl;
					cout << "(14) Puck: " <<(int)id<< " gibt NOTOK zurueck" << endl;
					return PulseSignals::NOTOK;

					break;
				case PulseSignals::PUCKATJUNK:
					cout << "(15) Puck: " <<(int)id<< " (NEWPUCK) empfaengt: PUCKATJUNK" << endl;
					cout << "(16) Puck: " <<(int)id<< " gibt NOTOK zurueck" << endl;
					return PulseSignals::NOTOK;

					break;
				case PulseSignals::OUTPUTPUCK:
					cout << "(17) Puck: " <<(int)id<< " (NEWPUCK) empfaengt: OUTPUTPUCK" << endl;
					cout << "(18) Puck: " <<(int)id<< " gibt NOTOK zurueck" << endl;
					return PulseSignals::NOTOK;
					break;
				case PulseSignals::REFRESH:
					cout << "(19) Puck: " <<(int)id<< " (NEWPUCK) empfaengt: REFRESH" << endl;

					if(time <= nextSignal + TOLERANCE_TIME)
					{
						cout << "(20) Puck: " <<(int)id<< " gibt OK zurueck" << endl;
						return PulseSignals::OK;
					}

					cout << "(21) Puck: " <<(int)id<< " gibt NOTOK zurueck" << endl;
					return PulseSignals::NOTOK;
					break;
				default:

					break;
			}
		case UNKNOWNDRILL :
			switch (pulse)
			{
				case PulseSignals::INPUTPUCK:
					cout << "(22) Puck: " <<(int)id<< " (UNKNOWNDRILL) empfaengt: INPUTPUCK" << endl;
					nextSignal = time + DISTANCE_INPUT_HEIGHT;
				break;
				case PulseSignals::PUCKATHEIGHT:
					cout << "(23) Puck: " <<(int)id<< " (UNKNOWNDRILL) empfaengt: PUCKATHEIGHT" << endl;
					if((timeDif > TOLERANCE_TIME))
					{
						cout << "(24) Puck: " <<(int)id<< " (UNKNOWNDRILL) ist OUTOFTIME" << endl;
						return PulseSignals::NOTOK;
					}

					heightMeasurement2 = HeightSensor::getInstance().getHeightValue();

					if (heightMeasurement2 <= Junk_Puck + TOLERANCE_HEIGHT && heightMeasurement2 >= Junk_Puck - TOLERANCE_HEIGHT)
					{
						//Puck is Junk
						heightMeasurement2_OK = true;
						cout << "(25) Puck: " <<(int)id<< " aendert State von UNKNOWNDRILL zu JUNK" << endl;
						state = JUNK;
						}
						else if (heightMeasurement2 > Junk_Puck + TOLERANCE_HEIGHT)
						{
							//Puck has got a drill
							heightMeasurement2_OK = true;
							cout << "(26) Puck: " <<(int)id<< " aendert State von UNKNOWNDRILL zu DRILLONTOP" << endl;
							state = DRILLONTOP;
						}
						else
						{
							//No Drill found.
							turns++;
							if (turns < 3)
							{
								nextSignal = time + DISTANCE_INPUT_HEIGHT;
								cout << "(27) Puck: " <<(int)id<< " gibt TURNPUCK zurueck" << endl;

								return PulseSignals::TURNPUCK;
							}
							cout << "(28) Puck: " <<(int)id<< " aendert State von UNKNOWNDRILL zu JUNK" << endl;
							state = JUNK;
						}
					nextSignal = time + DISTANCE_HEIGHT_GATE;

					cout << "(27) Puck: " <<(int)id<< " gibt OK zurueck" << endl;
					return PulseSignals::OK;
					break;
				case PulseSignals::PUCKATGATE:
					if(timeDif > TOLERANCE_TIME)
					{
						cout << "(28) Puck: " <<(int)id<< " (UNKNOWNDRILL) ist OUTOFTIME" << endl;
						return PulseSignals::NOTOK;
					}
					cout << "Zeit am Gate: " <<  timeDif << endl;
					nextSignal = time + DISTANCE_GATE_OUTPUT;
					cout << "(29) Puck: " <<(int)id<< " gibt GATEPASS zurueck" << endl;
					return PulseSignals::GATEPASS;
					break;
				case PulseSignals::PUCKATJUNK:

					cout << "(30) Puck: " <<(int)id<< " gibt OK zurueck" << endl;
					return PulseSignals::OK;
					break;
				case PulseSignals::OUTPUTPUCK:
					if(timeDif > TOLERANCE_TIME)
					{
						cout << "(31) Puck: " <<(int)id<< " (UNKNOWNDRILL) ist OUTOFTIME" << endl;
						return PulseSignals::NOTOK;
					}
					cout << "(32) Puck: " <<(int)id<< " gibt TURNPUCK zurueck" << endl;
					return PulseSignals::TURNPUCK;
					break;
				case PulseSignals::REFRESH:
					if(time <= nextSignal + TOLERANCE_TIME)
					{
						cout << "(33) Puck: " <<(int)id<< " gibt OK zurueck" << endl;
						return PulseSignals::OK;
					}
					cout << "(34) Puck: " <<(int)id<< " gibt NOTOK zurueck" << endl;
					return PulseSignals::NOTOK;
					break;
				default:

					break;
			}
		case DRILLONTOP:
			switch (pulse)
			{
				case PulseSignals::INPUTPUCK:
					cout << "(35) Puck: " <<(int)id<< " (DRILLONTOP) empfaengt: INPUTPUCK" << endl;
					nextSignal = time + DISTANCE_INPUT_HEIGHT;
				break;
				case PulseSignals::PUCKATHEIGHT:
					cout << "(36) Puck: " <<(int)id<< " (DRILLONTOP) empfaengt: PUCKATHEIGHT" << endl;
				break;
				case PulseSignals::PUCKATGATE:
					cout << "(37) Puck: " <<(int)id<< " (DRILLONTOP) empfaengt: PUCKATGATE" << endl;

					if(timeDif > TOLERANCE_TIME)
					{
						cout << "(38) Puck: " <<(int)id<< " (DRILLONTOP) ist OUTOFTIME" << endl;
						return PulseSignals::NOTOK;
					}
					if(heightMeasurement2 == 0)
					{
						if(MetalDetector::getInstance().puckHasMetal())
						{
							cout << "(39) Puck: " <<(int)id<< " aendert State von DRILLONTOP zu METALDRILLONTOP" << endl;

							metalMeasurement1 = METAL;
							state = METALDRILLONTOP;
						}
						else
						{
							metalMeasurement1 = NOMETAL;
							state = DRILLONTOP;
						}

						nextSignal = time + DISTANCE_GATE_OUTPUT;

						cout << "(40) Puck: " <<(int)id<< " gibt GATEPASS zurueck" << endl;
						return PulseSignals::GATEPASS;
					}
					else
					{
						if(MetalDetector::getInstance().puckHasMetal())
						{
							cout << "(41) Puck: " <<(int)id<< " aendert State von DRILLONTOP zu METALDRILLONTOP" << endl;

							metalMeasurement2 = METAL;
							state = METALDRILLONTOP;

							nextSignal = time + DISTANCE_GATE_OUTPUT;
							cout << "(42) Puck: " <<(int)id<< " gibt GATEPASS zurueck" << endl;
							return PulseSignals::GATEPASS;
						}
						else
						{
							metalMeasurement2 = NOMETAL;
							state = DRILLONBOTTOM;

							nextSignal = time + DISTANCE_INPUT_HEIGHT + DISTANCE_HEIGHT_GATE;
							cout << "(43) Puck: " <<(int)id<< " gibt TURNPUCK zurueck" << endl;
							return PulseSignals::TURNPUCK;
						}
					}
					break;
				case PulseSignals::PUCKATJUNK:
					cout << "(44) Puck: " <<(int)id<< " gibt NOTOK zurueck" << endl;
					return PulseSignals::NOTOK;
					break;
				case PulseSignals::OUTPUTPUCK:
					if(timeDif > TOLERANCE_TIME)
					{
						cout << "(45) Puck: " <<(int)id<< " (DRILLONTOP) ist OUTOFTIME" << endl;
						return PulseSignals::NOTOK;
					}

					cout << "(46) Puck: " <<(int)id<< " gibt TURNPUCK zurueck" << endl;
					state = DRILLONBOTTOM;
					return PulseSignals::TURNPUCK;
					break;
				case PulseSignals::REFRESH:

					if(time <= nextSignal + TOLERANCE_TIME)
					{
						cout << "(47) Puck: " <<(int)id<< " gibt OK zurueck" << endl;
						return PulseSignals::OK;
					}

					cout << "(48) Puck: " <<(int)id<< " gibt NOTOK zurueck" << endl;
					return PulseSignals::NOTOK;
					break;
				default:
					break;
			}

			break;
		case DRILLONBOTTOM:
			switch (pulse)
			{
			case PulseSignals::INPUTPUCK:
				cout << "(49) Puck: " <<(int)id<< " (DRILLONBOTTOM) empfaengt: INPUTPUCK" << endl;
				nextSignal = time + DISTANCE_INPUT_HEIGHT;
			break;
			case PulseSignals::PUCKATHEIGHT:
				cout << "(50) Puck: " <<(int)id<< " (DRILLONBOTTOM) empfaengt: PUCKATHEIGHT" << endl;
					if(timeDif > TOLERANCE_TIME)
					{
						cout << "(51) Puck: " <<(int)id<< " (DRILLONBOTTOM) ist OUTOFTIME" << endl;
						return PulseSignals::NOTOK;
					}

					heightMeasurement2 = HeightSensor::getInstance().getHeightValue();
					if (heightMeasurement2 <= Junk_Puck + TOLERANCE_HEIGHT && heightMeasurement2 >= Junk_Puck - TOLERANCE_HEIGHT)
					{
						//Puck is Junk
						cout << "(52) Puck: " <<(int)id<< " aendert State von DRILLONBOTTOM zu JUNK" << endl;
						state = JUNK;
					}
					else if (heightMeasurement2 > Junk_Puck + TOLERANCE_HEIGHT)
					{
						//Puck has got a drill on Top! This is not allowed
						cout << "(53) Puck: " <<(int)id<< " aendert State von DRILLONBOTTOM zu JUNK" << endl;
						state = JUNK;
					}

					nextSignal = time + DISTANCE_HEIGHT_GATE;

					cout << "(54) Puck: " <<(int)id<< " gibt OK zurueck" << endl;
					return PulseSignals::OK;

					break;
				case PulseSignals::PUCKATGATE:
					cout << "(55) Puck: " <<(int)id<< " (DRILLONBOTTOM) empfaengt: PUCKATGATE" << endl;
					if (metalMeasurement1 == UNDEFINED)
					{
						if (MetalDetector::getInstance().puckHasMetal())
						{
							cout << "(56) Puck: " <<(int)id<< " gibt NOTOK zurueck" << endl;
							return PulseSignals::NOTOK;
						}
						else
						{
							nextSignal = time + DISTANCE_GATE_OUTPUT;
							cout << "(57) Puck: " <<(int)id<< " gibt GATEPASS zurueck" << endl;
							return PulseSignals::GATEPASS;
						}
					}
					else
					{
						if (MetalDetector::getInstance().puckHasMetal())
						{
							cout << "(58) Puck: " <<(int)id<< " gibt NOTOK zurueck" << endl;
							return PulseSignals::NOTOK;
						}
						else
						{
							nextSignal = time + DISTANCE_GATE_OUTPUT;
							cout << "(59) Puck: " <<(int)id<< " gibt GATEPASS zurueck" << endl;
							return PulseSignals::GATEPASS;
						}
					}
					break;
				case PulseSignals::PUCKATJUNK:
					cout << "(60) Puck: " <<(int)id<< " (DRILLONBOTTOM) empfaengt: PUCKATJUNK" << endl;
					cout << "(61) Puck: " <<(int)id<< " gibt NOTOK zurueck" << endl;
					return PulseSignals::NOTOK;

					break;
				case PulseSignals::OUTPUTPUCK:
					cout << "(62) Puck: " <<(int)id<< " (DRILLONBOTTOM) empfaengt: OUTPUTPUCK" << endl;
					if(timeDif > TOLERANCE_TIME)
					{
						cout << "(63) Puck: " <<(int)id<< " gibt NOTOK zurueck" << endl;
						return PulseSignals::NOTOK;
					}

					cout << "(64) Puck: " <<(int)id<< " gibt OK zurueck" << endl;
					return PulseSignals::OK;
					break;
				case PulseSignals::REFRESH:
					cout << "(65) Puck: " <<(int)id<< " (DRILLONBOTTOM) empfaengt: REFRESH" << endl;
					if(time <= nextSignal + TOLERANCE_TIME)
					{
						cout << "(66) Puck: " <<(int)id<< " gibt OK zurueck" << endl;
						return PulseSignals::OK;
					}
					cout << "(67) Puck: " <<(int)id<< " gibt NOTOK zurueck" << endl;
					Logger::log("Puck", "return NOTOK");
					return PulseSignals::NOTOK;
					break;
				default:

					break;
			}
			break;
			case METALDRILLONTOP:
				switch (pulse)
				{
				case PulseSignals::INPUTPUCK:
					cout << "(68) Puck: " <<(int)id<< " (METALDRILLONTOP) empfaengt: INPUTPUCK" << endl;
					nextSignal = time + DISTANCE_INPUT_HEIGHT;
				break;
				case PulseSignals::PUCKATHEIGHT:
					cout << "(69) Puck: " <<(int)id<< " (METALDRILLONTOP) empfaengt: PUCKATHEIGHT" << endl;
					if(timeDif > TOLERANCE_TIME)
						{
							cout << "(70) Puck: " <<(int)id<< " (METALDRILLONTOP) ist OUTOFTIME" << endl;
							return PulseSignals::NOTOK;
						}

						heightMeasurement2 = HeightSensor::getInstance().getHeightValue();
						if(heightMeasurement2 <= Junk_Puck + TOLERANCE_HEIGHT && heightMeasurement2 >= Junk_Puck - TOLERANCE_HEIGHT)
						{
							//Puck is Junk
							cout << "(71) Puck: " <<(int)id<< " aendert State von METALDRILLONBOTTOM zu JUNK" << endl;
							state = JUNK;
						}
						else if(heightMeasurement2 > Junk_Puck + TOLERANCE_HEIGHT)
						{
							//Puck has got a drill
						}
						else
						{
							//A drilled puck was expected!
							cout << "(72) Puck: " <<(int)id<< " aendert State von METALDRILLONBOTTOM zu JUNK" << endl;
							state = JUNK;
						}

						nextSignal = time + DISTANCE_HEIGHT_GATE;
						cout << "(73) Puck: " <<(int)id<< " gibt OK zurueck" << endl;
						return PulseSignals::OK;
						break;
					case PulseSignals::PUCKATGATE:
						cout << "(74) Puck: " <<(int)id<< " (METALDRILLONTOP) empfaengt: PUCKATGATE" << endl;
						if(timeDif > TOLERANCE_TIME)
						{
							cout << "(75) Puck: " <<(int)id<< " (METALDRILLONTOP) ist OUTOFTIME" << endl;
							return PulseSignals::NOTOK;
						}

						if (metalMeasurement1 == UNDEFINED)
						{
							if (MetalDetector::getInstance().puckHasMetal())
							{
								metalMeasurement1 = METAL;

								state = METALDRILLONTOP;
								nextSignal = time + DISTANCE_GATE_OUTPUT;
								cout << "(76) Puck: " <<(int)id<< " gibt GATEPASS zurueck" << endl;
								return PulseSignals::GATEPASS;
							}
							else
							{
								cout << "(77) Puck: " <<(int)id<< " gibt NOTOK zurueck" << endl;
								return PulseSignals::NOTOK;
							}
						}
						else
						{
							if (MetalDetector::getInstance().puckHasMetal())
							{
								metalMeasurement2 = METAL;

								state = METALDRILLONTOP;
								nextSignal = time + DISTANCE_GATE_OUTPUT;
								cout << "(78) Puck: " <<(int)id<< " gibt GATEPASS zurueck" << endl;
								return PulseSignals::GATEPASS;
							}
							else
							{
								metalMeasurement2 = NOMETAL;
								state = JUNK;
								nextSignal = time + DISTANCE_GATE_JUNK;
								cout << "(79) Puck: " <<(int)id<< " gibt GATEDENY zurueck" << endl;
								return PulseSignals::GATEDENY;
							}
						}

						nextSignal = time + DISTANCE_GATE_OUTPUT;
						cout << "(80) Puck: " <<(int)id<< " gibt GATEPASS zurueck" << endl;
						return PulseSignals::GATEPASS;
						break;
					case PulseSignals::PUCKATJUNK:
						cout << "(81) Puck: " <<(int)id<< " (METALDRILLONTOP) empfaengt: PUCKATJUNK" << endl;
						cout << "(82) Puck: " <<(int)id<< " gibt NOTOK zurueck" << endl;
						return PulseSignals::NOTOK;
						break;
					case PulseSignals::OUTPUTPUCK:
						cout << "(83) Puck: " <<(int)id<< " (METALDRILLONTOP) empfaengt: OUTPUTPUCK" << endl;
						if(timeDif > TOLERANCE_TIME)
						{
							cout << "timeDif: " << timeDif << "TOLERANCE_TIME: " << TOLERANCE_TIME << endl;
							cout << "(84) Puck: " <<(int)id<< " (METALDRILLONTOP) ist OUTOFTIME" << endl;
							return PulseSignals::NOTOK;
						}
						cout << "(85) Puck: " <<(int)id<< " gibt OK zurueck" << endl;
						return PulseSignals::OK;
						break;
					case PulseSignals::REFRESH:
						if(time <= nextSignal + TOLERANCE_TIME)
						{
							cout << "(86) Puck: " <<(int)id<< " gibt OK zurueck" << endl;
							return PulseSignals::OK;
						}
						cout << "time: " << time <<" nextSignal: " << nextSignal + TOLERANCE_TIME << endl;
						cout << "(87) Puck: " <<(int)id<< " gibt NOTOK zurueck" << endl;
						return PulseSignals::NOTOK;
						break;
					default:
						break;
				}
				break;
		case JUNK:
			switch (pulse)
			{
				case PulseSignals::INPUTPUCK:
					cout << "(88) Puck: " <<(int)id<< " (JUNK) empfaengt: INPUTPUCK" << endl;
					break;
				case PulseSignals::PUCKATHEIGHT:
					cout << "(89) Puck: " <<(int)id<< " (JUNK) empfaengt: PUCKATHEIGHT" << endl;
					cout << "(90) Puck: " <<(int)id<< " gibt NOTOK zurueck" << endl;
					return PulseSignals::NOTOK;
					break;
				case PulseSignals::PUCKATGATE:
					cout << "(91) Puck: " <<(int)id<< " (JUNK) empfaengt: PUCKATGATE" << endl;
					nextSignal = time + DISTANCE_GATE_JUNK;
					cout << "(92) Puck: " <<(int)id<< " gibt GATEDENY zurueck" << endl;
					return PulseSignals::GATEDENY;
					break;
				case PulseSignals::PUCKATJUNK:
					cout << "(93) Puck: " <<(int)id<< " (JUNK) empfaengt: PUCKATJUNK" << endl;
					cout << "(94) Puck: " <<(int)id<< " gibt OK zurueck" << endl;
					return PulseSignals::OK;
					break;
				case PulseSignals::OUTPUTPUCK:
					cout << "(95) Puck: " <<(int)id<< " (JUNK) empfaengt: OUTPUTPUCK" << endl;
					cout << "(96) Puck: " <<(int)id<< " gibt NOTOK zurueck" << endl;
					return PulseSignals::NOTOK;
					break;
				case PulseSignals::REFRESH:
					if(time <= nextSignal + TOLERANCE_TIME)
					{
						cout << "(97) Puck: " <<(int)id<< " gibt OK zurueck" << endl;
						return PulseSignals::OK;
					}
					cout << "(98) Puck: " <<(int)id<< " gibt NOTOK zurueck" << endl;
					return PulseSignals::NOTOK;
					break;
				default:

					break;
			}
			break;
		default:

			break;

	}

	return PulseSignals::OK;
}

string Puck::toString() {
      stringstream ss;//create a stringstream
      ss << "Puck " << (uint16_t)this->id << ": " << " state: " << this->state << " next signal: " << this->nextSignal << " turns: " << (uint16_t)this->turns << " HM: " << this->heightMeasurement1 << " HM2: " << this->heightMeasurement2 << " MM: " << this->metalMeasurement1 << " MM2: " << this->metalMeasurement2;
      return ss.str();
}

void Puck::toStringForUser()
{
	cout << endl << endl << "Puck" << endl << "id: " << (int)this->id << endl << "HM1: " << this->heightMeasurement1 << endl << "HM2: " << this->heightMeasurement2 << endl;
	if(this->metalMeasurement2 == 1)
	{
		cout << "Type: metal, drill on Top" << endl << endl;
	}
	else
	{
		cout << "Type: no metal, drill on bottom" << endl << endl;
	}
}

struct Puck::uartPuck Puck::toMessage() {
	struct Puck::uartPuck data;
	data.id = id;
	data.nextSignal = nextSignal;
	data.state = state;
	data.turns = turns;
	data.heightMeasurement1 = heightMeasurement1;
	data.heightMeasurement2 = heightMeasurement2;
	data.metalMeasurement1 = metalMeasurement1;
	data.metalMeasurement2 = metalMeasurement2;
	return data;
}
