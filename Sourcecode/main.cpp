/* 
 * @file    main.cpp
 * @author
 * @version 0.1
 *
 * Start of application threads
 */

#include <cstdlib>
#include <iostream>
#include "./SystemTest/ActorTest.h"
#include "./HardwareControl/SerialThread.h"
#include "./HardwareControl/SensorThread.h"
#include "./lib/HWaccess.h"
#include "./ControllerStateMachineConveyor1/Controller.h"
#include "./ControllerStateMachineConveyor2/Controller2.h"

#include "SystemTest/RegressionTests/PuckTest.h"

using namespace std;

//#define TESTMODE

int main(int argc, char *argv[])
{
	//SensorThread* senThread = NULL;
	//cout << "Main thread running !." << endl;
//	Logger::log("Main", "Thread runing");
	// get IO access for main thread
	if (ThreadCtl(_NTO_TCTL_IO, 0) == -1)
	{
		cout << "No IO access" << endl;
	}
	//Logger::log("Main", "Thread runing");
	// dont create objects above this line


	Controller contr = Controller();

	contr.start(NULL);
	contr.join();

	cout << "Quitting...";

	cout << " done." << endl;

	return EXIT_SUCCESS;
}
