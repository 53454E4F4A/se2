/**
 * Controller2.cpp
 *
 *  Created on: 27.04.2014
 *      @author: Ulrich ter Horst
 */

//Todo refactor comments to match doxygen

#include "ControllerStateMachineConveyor2/Controller2.h"
#include "ConveyorTime.h"

#include "../HardwareControl/SensorThread.h"

using namespace std;

//internal notes by UtH, will be deleted in future, related to "Notiz.h" of Lars Schwensen:
//ignore this!

/* puckAtJunk triggered durch lichtschranke: gib mir in n sek ein checkJunk()
 * checkJunk(): heißt nich frei sondern prüfen ob frei: wenn ja dann puck löschen sonst fehler
 * refresh(): auf signal refresh
 * 				controller iterriert über jede queue gibt allen pucks somit signal refresh
 * 				itteration ist in den states
 *
 *
 * jonas dev_jns gibts samplecode für den controller-pulseteil
 *
 * Controller erzeugt beim initialisieren eine channelID und verteilt diese via parameter an seine Untergebenen durch den Constructor
 */

//internal notes - End-

Controller2::Controller2()
{
	Logger::log("Controller2", "constructor called!");
	context = new Context2(this);

	puck = NULL;

	chid = ChannelCreate(0);
	if (chid == -1)
	{
		perror("Controller: ChannelCreate failed");
		exit(EXIT_FAILURE);	//TODO vllt ist das mit exit etwas zu brutal. Besprechen!
	}

	serial.setChannel(chid);
	serial.start(NULL);

	cout << "Controller1 chid: " << chid << endl;
	ConveyorTime::getInstance().setChid(chid);
	ConveyorTime::getInstance().start(NULL);
	ConveyorTime::getInstance().addNewEvent(PulseSignals::REFRESH,3000,true,true);

	SensorThread::getInstance()->setChannel(chid);

	context->setState(new Initializing2(context));
}

Controller2::~Controller2()
{
	if(context != NULL)
	{
		delete context;
	}
}


void Controller2::shutdown()
{

}

void Controller2::execute(void *pfunc)
{
	struct _pulse pulse;

	while(!isStopped())
	{
		if (MsgReceivePulse(chid, &pulse, sizeof(pulse), NULL) == -1)
		{
			if (isStopped()) break;
			perror("Controller2: MsgReceivePulse failed");

			exit(EXIT_FAILURE);
		}

		switch(pulse.code)
		{
		case PulseSignals::INPUTPUCK:
			Logger::log("Controller-exec","inputPuck");
			context->inputPuck(PulseSignals::INPUTPUCK);
			break;
		case PulseSignals::PUCKATHEIGHT:
			Logger::log("Controller-exec","puck at heigt sensor");
			context->puckAtHeightSensor(PulseSignals::PUCKATHEIGHT);
			break;
		case PulseSignals::PUCKATGATE:
			Logger::log("Controller-exec","puck at gate");
			context->puckAtGate(PulseSignals::PUCKATGATE);
			break;
		case PulseSignals::PUCKATJUNK:
			Logger::log("Controller-exec","puck at junk");
			context->puckAtJunk(PulseSignals::PUCKATJUNK);
			break;
		case PulseSignals::OUTPUTPUCK:
			Logger::log("Controller-exec","output puck");
			context->outputPuck(PulseSignals::OUTPUTPUCK);
			break;
		case PulseSignals::REQUEST:
			Logger::log("Controller-exec","request");
			context->request();
			break;
		case PulseSignals::REFRESH:
			Logger::log("Controller-exec","refresh");
			context->refresh(PulseSignals::REFRESH);
			break;
		case PulseSignals::CLOSEGATE:
			Logger::log("Controller-exec","close gate");
			context->closeGate();
			break;
		case PulseSignals::CHECKJUNK:
			Logger::log("Controller-exec","check junk");
			context->checkJunk();
			break;
		case PulseSignals::OTHERDEVICEANSWERS:
			Logger::log("Controller-exec","conveyor2 answers");
			context->conveyorTwoAnswers();
			break;
		case PulseSignals::RECEIVEDPUCKINFORMATION:
			Logger::log("Controller-exec","ReceivedPulseInformation");
			//TODO: Moegliche Fehlerquelle, falls Der Puck nicht richtig uebertragen wird!
			context->receivedPuckInformation((Puck*)pulse.value.sival_int);
		case PulseSignals::START_BTN:
			Logger::log("Controller-exec","start button");
			context->startBtn();
			break;
		case PulseSignals::STOP_BTN:
			Logger::log("Controller-exec","stop button");
			context->stopBtn();
			break;
		case PulseSignals::RESET_BTN:
			Logger::log("Controller-exec","reset button");
			context->resetBtn();
			break;
		case PulseSignals::SOFTFAILURE:
			Logger::log("Controller-exec","Soft Failure");
			context->softFailure();
			break;
		case PulseSignals::HARDFAILURE:
			Logger::log("Controller-exec","Hard Failure");
			context->hardFailure();
			break;
		}
	}
}
