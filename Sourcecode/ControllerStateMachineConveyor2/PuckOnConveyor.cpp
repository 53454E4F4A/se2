/**
 * PuckOnConveyor.cpp
 *
 *  Created on: 30.04.2014
 *  @author: Ulrich ter Horst
 */

#include "Puck.h"
#include "ControllerStateMachineConveyor2/Controller2.h"
#include "../defines.h"

#include "ControllerStateMachineConveyor2/PuckOnConveyor.h"

#include "ControllerStateMachineConveyor2/ConveyorBackward.h"
#include "ControllerStateMachineConveyor2/Ready.h"
#include "ControllerStateMachineConveyor2/Stop2.h"
#include "ControllerStateMachineConveyor2/SoftFailureUnreceipted2.h"
#include "ControllerStateMachineConveyor2/HardFailureUnreceipted2.h"

//#include "../HardwareControl/Conveyor.h"

using namespace std;

PuckOnConveyor::PuckOnConveyor(Context2* context) : ConveyorState2(context)
{
	entryFunction();
	stateIndex = IDX_PUCKONCONVEYOR;
}

PuckOnConveyor::~PuckOnConveyor()
{

}

void PuckOnConveyor::inputPuck(PulseSignals::PULSE signal)
{
	convContext->controller->puck->receiveSignal(signal);
}

void PuckOnConveyor::puckAtHeightSensor(PulseSignals::PULSE signal)
{
		PulseSignals::PULSE puckAnswer = convContext->controller->puck->receiveSignal(signal);
		if (puckAnswer == PulseSignals::TURNPUCK)
		{
			new (this) ConveyorBackward(convContext);
		}
		else if (puckAnswer == PulseSignals::NOTOK)
		{
			hardFailure();
		}
}

void PuckOnConveyor::puckAtGate(PulseSignals::PULSE signal)
{
		PulseSignals::PULSE puckAnswer = convContext->controller->puck->receiveSignal(signal);
		if (puckAnswer == PulseSignals::TURNPUCK)
		{
			new (this) ConveyorBackward(convContext);
		}
		else if (puckAnswer == PulseSignals::GATEPASS)
		{
			convContext->controller->gate.Pass();

			ConveyorTime::getInstance().addNewEvent(PulseSignals::CLOSEGATE, 800, false);
		}
		else if (puckAnswer == PulseSignals::GATEDENY)
		{
			convContext->controller->gate.Pass();
			ConveyorTime::getInstance().addNewEvent(PulseSignals::CLOSEGATE,100,false);
		}
		else
		{
			hardFailure();
		}
}

void PuckOnConveyor::puckAtJunk(PulseSignals::PULSE signal)
{
		if(convContext->controller->puck == NULL)
		{
			hardFailure();
		}
		else
		{
			PulseSignals::PULSE puckAnswer = convContext->controller->puck->receiveSignal(signal);
			if(puckAnswer == PulseSignals::OK)
			{
				if(convContext->controller->puck != NULL)
				{
					delete convContext->controller->puck;
					convContext->controller->puck = NULL;
				}

				convContext->controller->convTime->getInstance().addNewEvent(PulseSignals::CHECKJUNK,3000,false);
				new (this) Ready(convContext);
			}
			else
			{
				hardFailure();
			}
		}
}

void PuckOnConveyor::outputPuck(PulseSignals::PULSE signal)
{
		if(SensorThread::getInstance()->isOutputPuck()){

			convContext->controller->conveyor.stop();
			ConveyorTime::getInstance().stopCounter();

			//Falls der Puck in outputPuck liegt, schaue ich in einer Sekunde noch einmal nach
			convContext->controller->convTime->getInstance().addNewEvent(PulseSignals::OUTPUTPUCK,1000,false, true);

			if(convContext->controller->puck != NULL)
			{
				convContext->controller->puck->toStringForUser();
				delete(convContext->controller->puck);
				convContext->controller->puck = NULL;
			}
		}
		else
		{
			//Falls der Puck inzwischen vom Band genommen wurde kann weiter gemacht werden.
			if(convContext->controller->puck == NULL)
			{
				new (this) Ready(convContext);
			}
		}
}

void PuckOnConveyor::refresh(PulseSignals::PULSE signal)
{
	if (convContext->controller->puck != NULL)
	{
		PulseSignals::PULSE puckAnswer;

		puckAnswer = convContext->controller->puck->receiveSignal(signal);
		if (puckAnswer == PulseSignals::NOTOK)
		{
			hardFailure();
		}
	}else{
		outputPuck(PulseSignals::OUTPUTPUCK);
	}
	convContext->controller->serial.ping();
}

void PuckOnConveyor::closeGate()
{
	convContext->controller->gate.Deny();
}

void PuckOnConveyor::checkJunk()
{
	if(SensorThread::getInstance()->isRampFull()){
			softFailure();

			ConveyorTime::getInstance().addNewEvent(PulseSignals::CHECKJUNK,3000,false);
	}

}

void PuckOnConveyor::stopBtn()
{
	if(convContext->controller->serial.sendPulse(PulseSignals::STOP_BTN))
	{
			convContext->addStateOnStack(stateIndex);
			new (this) Stop2(convContext);
	}
	else
	{
		hardFailure();
	}
}

void PuckOnConveyor::resetBtn()
{
	if(convContext->controller->serial.sendPulse(PulseSignals::RESET_BTN))
	{
		new (this) Initializing2(convContext);
	}
	else
	{
		hardFailure();
	}
}

void PuckOnConveyor::softFailure()
{
	if(convContext->controller->serial.sendPulse(PulseSignals::SOFTFAILURE))
	{
			convContext->addStateOnStack(stateIndex);
			new (this) SoftFailureUnreceipted2(convContext);
	}
	else
	{
		hardFailure();
	}
}

void PuckOnConveyor::hardFailure()
{
	convContext->controller->serial.sendPulse(PulseSignals::HARDFAILURE);
	//Change state to hardFailure unreceipted
	new (this) HardFailureUnreceipted2(convContext);
}

void PuckOnConveyor::entryFunction()
{
	if(!SensorThread::getInstance()->isOutputPuck()){
		convContext->controller->conveyor.forward_fast();
		ConveyorTime::getInstance().startCounter();
	}

	convContext->controller->statusLight.setColor(Statuslight::GREEN);
	convContext->controller->statusLight.setFrequency(Statuslight::FLASH_PERMANENT);

	convContext->controller->panel.setLedQ1();
	convContext->controller->panel.revokeLedQ2();

	Logger::log("PuckOnConveyor", "entry");
}

