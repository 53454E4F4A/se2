/*
 * Initializing2.cpp
 *
 *  Created on: 30.04.2014
 *  @author: Ulrich ter Horst
 */

#include "ControllerStateMachineConveyor2/Initializing2.h"
#include "ControllerStateMachineConveyor2/Controller2.h"
#include "ControllerStateMachineConveyor2/Ready.h"
#include "../defines.h"
#include "ConveyorTime.h"
#include <new>

using namespace std;

Initializing2::Initializing2(Context2* context) : ConveyorState2(context)
{
	cout << "Initializing2 - constructor called" << endl;
	entryFunction();
}

Initializing2::~Initializing2()
{
	cout << "Initializing2 - destructor called" << endl;
}


/**
	 * @return: void
	 * transition: --> operating
	 */
void Initializing2::startBtn()
{
	if(!convContext->controller->panel.emergencyPressed()){
		convContext->controller->serial.sendPulse(PulseSignals::START_BTN);

		cout << "Initializing2 - startBtn called" << endl;
		new (this) Ready(convContext);
	}
}

void Initializing2::resetBtn()
{
	Puck::setReferenceValue();
}

void Initializing2::entryFunction()
{
	convContext->controller->conveyor.stop();
	ConveyorTime::getInstance().stopCounter();

	convContext->controller->gate.Deny();

	convContext->controller->statusLight.setColor(Statuslight::OFF);
	convContext->controller->statusLight.setFrequency(Statuslight::FLASH_PERMANENT);

	convContext->controller->panel.setLedReset();

	convContext->controller->panel.revokeLedQ1();
	convContext->controller->panel.revokeLedQ2();

	if (!(convContext->controller->puck == NULL)) {
		delete convContext->controller->puck;
		convContext->controller->puck = NULL;
	}

	Logger::log("Initializing2", "entry");
}
