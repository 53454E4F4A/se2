/*
 * SoftFailureReceipted2.cpp
 *
 *  Created on: 12.05.2014
 *      Author: blinch
 */


#include "ControllerStateMachineConveyor2/Controller2.h"
#include "ControllerStateMachineConveyor2/Initializing2.h"
#include "ControllerStateMachineConveyor2/SoftFailureReceipted2.h"
#include "ControllerStateMachineConveyor2/HardFailureUnreceipted2.h"


SoftFailureReceipted2::SoftFailureReceipted2(Context2 *context) : ConveyorState2(context)
{
	entryFunction();
	isInInputPuck = false;

}

SoftFailureReceipted2::~SoftFailureReceipted2()
{

}

void SoftFailureReceipted2::puckAtJunk(PulseSignals::PULSE signal)
{
	if(convContext->controller->puck == NULL)
	{
		//hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->puck->receiveSignal(signal);
		if(puckAnswer == PulseSignals::OK)
		{
			if(convContext->controller->puck != NULL)
			{
				delete convContext->controller->puck ;
				convContext->controller->puck = NULL;
			}

			convContext->controller->convTime->getInstance().addNewEvent(PulseSignals::CHECKJUNK,3000,false);
		}
		else
		{
			hardFailure();
		}
	}
}

void SoftFailureReceipted2::refresh(PulseSignals::PULSE signal)
{
	convContext->controller->serial.ping();
}

void SoftFailureReceipted2::startBtn()
{
	if(convContext->controller->panel.startPressed()){
		convContext->controller->serial.sendPulse(PulseSignals::START_BTN);
	}
	convContext->goToPrevState();
}

void SoftFailureReceipted2::resetBtn()
{
		if(convContext->controller->serial.sendPulse(PulseSignals::RESET_BTN))
		{
			//Change state to initializing
			new (this) Initializing2(convContext);
		}
		else
		{
			hardFailure();
		}
}

void SoftFailureReceipted2::hardFailure()
{
	convContext->controller->serial.sendPulse(PulseSignals::HARDFAILURE);
		//Change state to hardFailure unreceipted
	new (this) HardFailureUnreceipted2(convContext);
}

void SoftFailureReceipted2::entryFunction()
{
	convContext->controller->conveyor.stop();
	ConveyorTime::getInstance().stopCounter();

	convContext->controller->statusLight.setColor(Statuslight::RED);
	convContext->controller->statusLight.setFrequency(Statuslight::FLASH_PERMANENT);

	convContext->controller->panel.setLedQ1();
	convContext->controller->panel.revokeLedQ2();

	Logger::log("SoftFailureReceipted2","entry");
}
