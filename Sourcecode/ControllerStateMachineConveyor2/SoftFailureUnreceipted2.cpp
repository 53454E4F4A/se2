/*
 * SoftFailureUnreceipted2.cpp
 *
 *  Created on: 12.05.2014
 *  @author: Ulrich ter Horst
 */

#include "ControllerStateMachineConveyor2/Controller2.h"

#include "ControllerStateMachineConveyor2/SoftFailureUnreceipted2.h"

#include "ControllerStateMachineConveyor2/SoftFailureReceipted2.h"
#include "ControllerStateMachineConveyor2/SoftFailureUnreceiptedGone2.h"
#include "ControllerStateMachineConveyor2/HardFailureUnreceipted2.h"
#include "ControllerStateMachineConveyor2/Initializing2.h"
#include "ConveyorTime.h"
#include "SensorThread.h"

using namespace std;

SoftFailureUnreceipted2::SoftFailureUnreceipted2(Context2 *context) : ConveyorState2(context)
{
	entryFunction();
	isInInputPuck = false;
}


SoftFailureUnreceipted2::~SoftFailureUnreceipted2()
{

}

void SoftFailureUnreceipted2::puckAtJunk(PulseSignals::PULSE signal)
{
	if(convContext->controller->puck == NULL)
	{
		hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->puck->receiveSignal(signal);
		if(puckAnswer == PulseSignals::OK)
		{
			if(convContext->controller->puck != NULL)
			{
				delete convContext->controller->puck ;
				convContext->controller->puck = NULL;
			}

			ConveyorTime::getInstance().addNewEvent(PulseSignals::CHECKJUNK,3000,false);
		}
		else
		{
			hardFailure();
		}
	}
}

void SoftFailureUnreceipted2::refresh(PulseSignals::PULSE signal)
{
	convContext->controller->serial.ping();
}

void SoftFailureUnreceipted2::checkJunk(){

	if(!SensorThread::getInstance()->isRampFull())
	{
		new (this) SoftFailureUnreceiptedGone2(convContext);
	}
	else
	{
		convContext->controller->convTime->getInstance().addNewEvent(PulseSignals::CHECKJUNK,3000,false);
	}
}

void SoftFailureUnreceipted2::startBtn()
{
	if(convContext->controller->panel.startPressed()){
		convContext->controller->serial.sendPulse(PulseSignals::START_BTN);
	}
		new (this) SoftFailureReceipted2(convContext);
}

void SoftFailureUnreceipted2::resetBtn()
{
	if(convContext->controller->panel.resetPressed()){
			convContext->controller->serial.sendPulse(PulseSignals::RESET_BTN);
	}
	new (this) Initializing2(convContext);
}

void SoftFailureUnreceipted2::hardFailure()
{
	convContext->controller->serial.sendPulse(PulseSignals::HARDFAILURE);
		//Change state to hardFailure unreceipted
		new (this) HardFailureUnreceipted2(convContext);
}

void SoftFailureUnreceipted2::entryFunction()
{
		convContext->controller->conveyor.stop();
		ConveyorTime::getInstance().stopCounter();

		convContext->controller->statusLight.setColor(Statuslight::RED);
		convContext->controller->statusLight.setFrequency(Statuslight::FLASH_1HZ);

		convContext->controller->panel.revokeLedQ2();

		Logger::log("SoftFailureUnreceipted2", "entry");
}
