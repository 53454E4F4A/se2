/**
 * ConveyorState2.h
 *
 *  Created on: 28.04.2014
 *  @author: Ulrich ter Horst
 */

#ifndef CONVEYORSTATE2_H_
#define CONVEYORSTATE2_H_


#include "../HardwareControl/Statuslight.h"
#include "ControllerStateMachineConveyor2/Signals2.h"
#include "PulseSignals.h"
#include "ControllerStateMachineConveyor2/Context2.h"


using namespace std;

class Context;


class ConveyorState2 : public Signals2
{
public:
	ConveyorState2();
	ConveyorState2(Context2 *context);
	void entryFunction();
	virtual ~ConveyorState2();
	virtual void inputPuck(PulseSignals::PULSE signal);
	virtual void puckAtGate(PulseSignals::PULSE signal);
	virtual void puckAtHeightSensor(PulseSignals::PULSE signal);
	virtual void puckAtJunk(PulseSignals::PULSE signal);
	virtual void outputPuck(PulseSignals::PULSE signal);
	virtual void refresh(PulseSignals::PULSE signal);
	virtual void request();
	virtual void closeGate();
	virtual void checkJunk();
	virtual void conveyorTwoAnswers();
	virtual void receivedPuckInformation(Puck *puck);
	virtual void startBtn();
	virtual void stopBtn();
	virtual void resetBtn();
	virtual void softFailure();
	virtual void hardFailure();

protected:
	Context2* convContext;
};



#endif /* CONVEYORSTATE_H_ */

