/**
 * PuckOnConveyor.h
 *
 *  Created on: 30.04.2014
 *  @author: Ulrich ter Horst
 */



#ifndef PUCKONCONVEYOR_H_
#define PUCKONCONVEYOR_H_

#include "ControllerStateMachineConveyor2/ConveyorState2.h"
#include "ControllerStateMachineConveyor2/Context2.h"

#include "PulseSignals.h"

#include "SensorThread.h"

using namespace std;

class PuckOnConveyor : public ConveyorState2
{
public:
	PuckOnConveyor(Context2 *context);
	virtual ~PuckOnConveyor();

	uint8_t stateIndex;

	virtual void inputPuck(PulseSignals::PULSE signal);
	virtual void puckAtHeightSensor(PulseSignals::PULSE signal);
	virtual void puckAtGate(PulseSignals::PULSE signal);
	virtual void puckAtJunk(PulseSignals::PULSE signal);
	virtual void outputPuck(PulseSignals::PULSE signal);
	virtual void refresh(PulseSignals::PULSE signal);
	//virtual void request();
	virtual void closeGate();
	virtual void checkJunk();
	//virtual void conveyorTwoAnswers();
	//virtual void receivedPuckInformation();
	//virtual void startBtn();
	virtual void stopBtn();
	virtual void resetBtn();
	virtual void softFailure();
	virtual void hardFailure();

	/**
	 * @return void
	 * This function is called by constructor.
	 * it sets:
	 * -gate: deny
	 * -statuslight: all off
	 * -conveyor: stop
	 */
	virtual void entryFunction();

private:
	bool puckArrivedOutput;
};

#endif /* PUCKONCONVEYOR_H_ */

