/**
 * Ready.cpp
 *
 *  Created on: 30.04.2014
 *  @author: Ulrich ter Horst
 */


#include "../Puck.h"
#include "ControllerStateMachineConveyor2/Controller2.h"
#include "../defines.h"

#include "ControllerStateMachineConveyor2/Ready.h"
#include "ControllerStateMachineConveyor2/PuckOnConveyor.h"
#include "ControllerStateMachineConveyor2/Stop2.h"
#include "ControllerStateMachineConveyor2/SoftFailureUnreceipted2.h"
#include "ControllerStateMachineConveyor2/HardFailureUnreceipted2.h"

//#include "../HardwareControl/Conveyor.h"

using namespace std;

Ready::Ready(Context2* context) : ConveyorState2(context)
{
	entryFunction();
	stateIndex = IDX_READY;
}

Ready::~Ready()
{

}

void Ready::refresh(PulseSignals::PULSE signal)
{
	convContext->controller->serial.ping();
}

void Ready::checkJunk()
{
	cout << "(1) Ready: empfange checkJunk" << endl;

	if(SensorThread::getInstance()->isRampFull()){
			softFailure();

			ConveyorTime::getInstance().addNewEvent(PulseSignals::CHECKJUNK,3000,false);
	}

}

void Ready::request()
{
	cout << "(2) Ready: empfange request" << endl;

	convContext->controller->serial.sendPulse(PulseSignals::OTHERDEVICEANSWERS);
}

void Ready::receivedPuckInformation(Puck *puck)
{
	cout << "(3) Ready: empfange receivedPuckInformation" << endl;

	convContext->controller->puck = puck;

	new (this) PuckOnConveyor(convContext);
}

void Ready::stopBtn()
{
	cout << "(4) Ready: empfange stopBtn" << endl;

	if(convContext->controller->serial.sendPulse(PulseSignals::STOP_BTN))
	{
			convContext->addStateOnStack(stateIndex);
			new (this) Stop2(convContext);
	}
	else
	{
		hardFailure();
	}
}

void Ready::resetBtn()
{
	cout << "(5) Ready: empfange resetBtn" << endl;
		if(convContext->controller->serial.sendPulse(PulseSignals::RESET_BTN))
		{
				new (this) Initializing2(convContext);
		}
		else
		{
			hardFailure();
		}
}

void Ready::softFailure()
{
	cout << "(6) Ready: empfange softFailure" << endl;
	if(convContext->controller->serial.sendPulse(PulseSignals::SOFTFAILURE))
	{
			convContext->addStateOnStack(stateIndex);
			new (this) SoftFailureUnreceipted2(convContext);
	}
	else
	{
		hardFailure();
	}
}

void Ready::hardFailure()
{
	cout << "(7) Ready: empfange hardFailure" << endl;
	convContext->controller->serial.sendPulse(PulseSignals::HARDFAILURE);
		//Change state to hardFailure unreceipted
	new (this) HardFailureUnreceipted2(convContext);
}

void Ready::entryFunction()
{
	cout << "(8) Ready: entryFunction wird aufgerufen" << endl;

	convContext->controller->conveyor.stop();
	ConveyorTime::getInstance().stopCounter();

	convContext->controller->statusLight.setColor(Statuslight::GREEN);
	convContext->controller->statusLight.setFrequency(Statuslight::FLASH_PERMANENT);

	convContext->controller->panel.revokeLedReset();

	convContext->controller->panel.revokeLedQ1();
	convContext->controller->panel.revokeLedQ2();

	convContext->controller->serial.sendPulse(PulseSignals::OTHERDEVICEANSWERS);
}



