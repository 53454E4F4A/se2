/**
 * ConveyorState2.cpp
 *
 *  Created on: 28.04.2014
 *      @author: Ulrich ter Horst
 */

//Todo refactor comments to match doxygen
#include "ControllerStateMachineConveyor2/ConveyorState2.h"
#include "ControllerStateMachineConveyor2/Controller2.h"
#include "ControllerStateMachineConveyor2/HardFailureUnreceipted2.h"
#include <iostream>

using namespace std;

ConveyorState2::ConveyorState2(Context2* context):
	convContext(context)
{
	cout << "ConveyorState - constructor called" << endl;
}

ConveyorState2::~ConveyorState2()
{
	cout << "ConveyorState - destructor called" << endl;
}


void ConveyorState2::entryFunction()
{
	cout << "ConveyorState - entryFunction: do nothing" << endl;
}


void ConveyorState2::closeGate()
{
	cout << "ConveyorState - closeGate: do nothing" << endl;
}


void ConveyorState2::puckAtGate(PulseSignals::PULSE signal)
{
	cout << "ConveyorState - puckAtGate: do nothing" << endl;
}


void ConveyorState2::resetBtn()
{
	cout << "ConveyorState - resetBtn: do nothing" << endl;
}


void ConveyorState2::puckAtJunk(PulseSignals::PULSE signal)
{
	cout << "ConveyorState - puckAtJunk: do nothing" << endl;
}


void ConveyorState2::checkJunk()
{
	cout << "ConveyorState - checkJunk: do nothing" << endl;
}


void ConveyorState2::refresh(PulseSignals::PULSE signal)
{
	cout << "ConveyorState - refresh: do nothing" << endl;
}

void ConveyorState2::request()
{
	cout << "ConveyorState - request: do nothing" << endl;
}

void ConveyorState2::outputPuck(PulseSignals::PULSE signal)
{
	cout << "ConveyorState - outputPuck: do nothing" << endl;
}


void ConveyorState2::puckAtHeightSensor(PulseSignals::PULSE signal)
{
	cout << "ConveyorState - puckAtHeightSens: do nothing" << endl;
}


void ConveyorState2::inputPuck(PulseSignals::PULSE signal)
{
	cout << "ConveyorState - inputPuck: do nothing" << endl;
}


void ConveyorState2::conveyorTwoAnswers()
{
	cout << "ConveyorState - Conv2Answers: do nothing" << endl;
}

void ConveyorState2::receivedPuckInformation(Puck *puck)
{
	cout << "ConveyorState - receivedPuckInformation: do nothing" << endl;
}

void ConveyorState2::stopBtn()
{
	cout << "ConveyorState - stopBtn: do nothing" << endl;
}


void ConveyorState2::startBtn()
{
	cout << "ConveyorState - startBtn: do nothing" << endl;
}


void ConveyorState2::softFailure()
{
	cout << "ConveyorState - hardFailure" << endl;
}


void ConveyorState2::hardFailure()
{
	cout << "ConveyorState - hardFailure" << endl;
}


