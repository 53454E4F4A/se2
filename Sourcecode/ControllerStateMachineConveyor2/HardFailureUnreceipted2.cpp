/*
 * HardFailureUnreceipted2.cpp
 *
 *  Created on: 12.05.2014
 *  @author: Ulrich ter Horst
 */

#include "ControllerStateMachineConveyor2/HardFailureUnreceipted2.h"
#include "ControllerStateMachineConveyor2/HardFailureReceipted2.h"
#include "ControllerStateMachineConveyor2/Controller2.h"

using namespace std;

HardFailureUnreceipted2::HardFailureUnreceipted2(Context2 *context) :
	ConveyorState2(context)
{
	entryFunction();
}

HardFailureUnreceipted2::~HardFailureUnreceipted2()
{

}

void HardFailureUnreceipted2::refresh(PulseSignals::PULSE signal)
{
	convContext->controller->serial.ping();
}

void HardFailureUnreceipted2::startBtn()
{
	if (convContext->controller->panel.startPressed())
	{
		convContext->controller->serial.sendPulse(PulseSignals::START_BTN);
	}
	new (this) HardFailureReceipted2(convContext);
}

void HardFailureUnreceipted2::entryFunction()
{
	convContext->controller->conveyor.stop();
	ConveyorTime::getInstance().stopCounter();

	convContext->controller->statusLight.setColor(Statuslight::RED);
	convContext->controller->statusLight.setFrequency(Statuslight::FLASH_1HZ);

	convContext->controller->panel.revokeLedQ2();

	Logger::log("HardFailureUnreceipted2","entry");
}
