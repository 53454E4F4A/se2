/**
 * Context2.h
 *
 *  Created on: 07.05.2014
 *  @author: Ulrich ter Horst
 *  @file: Context.h
 */

#ifndef CONTEXT2_H_
#define CONTEXT2_H_

#include "ControllerStateMachineConveyor2/Signals2.h"
#include "stdint.h"
#include <stack>

using namespace std;


class Controller2;
//class ConveyorState;
//class Signals;

class Context2 : Signals2
{
private:
	Signals2 *actualState;
	stack<uint8_t> stateStack;

public:
	Context2(Controller2 *contr);
	Controller2 *controller;

	Signals2 *prevState;
	bool puckPassedInput;

	virtual ~Context2();

	virtual void setState(Signals2* newState);
	bool goToPrevState();
	void addStateOnStack(uint8_t);

	virtual void inputPuck(PulseSignals::PULSE signal);
	virtual void puckAtHeightSensor(PulseSignals::PULSE signal);
	virtual void puckAtGate(PulseSignals::PULSE signal);
	virtual void puckAtJunk(PulseSignals::PULSE signal);
	virtual void outputPuck(PulseSignals::PULSE signal);
	virtual void refresh(PulseSignals::PULSE signal);
	virtual void request();
	virtual void closeGate();
	virtual void checkJunk();
	virtual void conveyorTwoAnswers();
	virtual void receivedPuckInformation(Puck *puck);
	virtual void startBtn();
	virtual void stopBtn();
	virtual void resetBtn();
	virtual void softFailure();
	virtual void hardFailure();


protected:
};

#endif /* CONTEXT2_H_ */
