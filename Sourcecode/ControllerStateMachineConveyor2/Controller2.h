/**
 * Controller2.h
 *
 *  Created on: 27.04.2014
 *      @author: Ulrich ter Horst
 *
 *   description:	ALL IN PROGRESS - This is the controller, which is in charge of
 *   				most incidents, activities and events of the puck sorting machine.
 *   				It is a kind of a context class of our main state machine.
 */


#ifndef CONTROLLER2_H_
#define CONTROLLER2_H_

#include "../HardwareControl/Controlpanel.h"
#include "../HardwareControl/Conveyor.h"
#include "../HardwareControl/Gate.h"
#include "../HardwareControl/SerialThread.h"
#include "../HardwareControl/Statuslight.h"
#include "../lib/HAWThread.h"
#include "./ControllerStateMachineConveyor2/Context2.h"
#include "./ControllerStateMachineConveyor2/Initializing2.h"
#include "../Puck.h"
#include "ConveyorTime.h"

#include <list>

using namespace std;

class Controller2 : public thread::HAWThread
{
public:
	Controller2();
	virtual ~Controller2();
	void execute(void*);	//dispatcher planned to be implemented here (stand: 28.04.14)
	void shutdown();

	Gate gate;
	Controlpanel panel;
	SerialThread serial;
	Statuslight statusLight;
	Conveyor conveyor;

	ConveyorTime *convTime;

	Puck* puck;

private:
	pthread_mutex_t mtx_;
	int chid;

	//Timer timer;
	//InterruptHandler interruptH;
	//Dispatcher* dispatcher;
	Context2* context;
};

#endif /* CONTROLLER2_H_ */
