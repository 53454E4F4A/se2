/*
 * Stop2.cpp
 *
 *  Created on: 14.05.2014
 *      Author: Lars Schwensen
 */

#include "ControllerStateMachineConveyor2/Stop2.h"
#include "defines.h"
#include "ControllerStateMachineConveyor2/SoftFailureUnreceipted2.h"
#include "ControllerStateMachineConveyor2/HardFailureUnreceipted2.h"
#include "ControllerStateMachineConveyor2/Initializing2.h"
#include "ConveyorTime.h"
#include "SensorThread.h"

Stop2::Stop2(Context2 *context) : ConveyorState2(context)
{
	entryFunction();
	stateIndex = IDX_STOP2;
}

Stop2::~Stop2()
{
}

void Stop2::puckAtJunk(PulseSignals::PULSE signal)
{
	if (convContext->controller->puck == NULL)
	{
		//hardFailure();
	}
	else
	{
		PulseSignals::PULSE puckAnswer = convContext->controller->puck->receiveSignal(signal);
		if (puckAnswer == PulseSignals::OK)
		{
			delete convContext->controller->puck;
			convContext->controller->puck = NULL;
			ConveyorTime::getInstance().addNewEvent(PulseSignals::CHECKJUNK, 3000, false);
		}
		else
		{
			hardFailure();
		}
	}
}

void Stop2::checkJunk()
{
	if (SensorThread::getInstance()->isRampFull())
	{
		softFailure();
		ConveyorTime::getInstance().addNewEvent(PulseSignals::CHECKJUNK, 3000, false);
	}
}

void Stop2::refresh(PulseSignals::PULSE signal)
{
	convContext->controller->serial.ping();
}

void Stop2::startBtn()
{
	if(convContext->controller->panel.startPressed())
	{
		convContext->controller->serial.sendPulse(PulseSignals::START_BTN);
	}
		convContext->goToPrevState();
}

void Stop2::resetBtn()
{
		if(convContext->controller->serial.sendPulse(PulseSignals::RESET_BTN))
		{
				//Change state to initializing
				new (this) Initializing2(convContext);
		}
		else
		{
			hardFailure();
		}
}

void Stop2::softFailure()
{
	if(convContext->controller->serial.sendPulse(PulseSignals::SOFTFAILURE))
	{
			convContext->addStateOnStack(stateIndex);
			new (this) SoftFailureUnreceipted2(convContext);
	}
	else
	{
		hardFailure();
	}
}

void Stop2::hardFailure()
{
	convContext->controller->serial.sendPulse(PulseSignals::HARDFAILURE);
		//Change state to hardFailure unreceipted
		new (this) HardFailureUnreceipted2(convContext);
}

void Stop2::entryFunction()
{
	convContext->controller->conveyor.stop();
	ConveyorTime::getInstance().stopCounter();
	convContext->controller->panel.setLedQ2();

	Logger::log("Stop2","entry");
}
