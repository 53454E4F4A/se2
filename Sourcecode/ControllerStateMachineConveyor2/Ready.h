/**
 * Operating.h
 *
 *  Created on: 30.04.2014
 *  @author: Ulrich ter Horst
 */



#ifndef READY_H_
#define READY_H_

#include "ControllerStateMachineConveyor2/ConveyorState2.h"
#include "ControllerStateMachineConveyor2/Context2.h"

#include "PulseSignals.h"

using namespace std;

class Ready : public ConveyorState2
{
public:
	Ready(Context2 *context);
	virtual ~Ready();

	uint8_t stateIndex;

	//virtual void inputPuck(PulseSignals::PULSE signal);
	//virtual void puckAtHeightSensor(PulseSignals::PULSE signal);
	//virtual void puckAtGate(PulseSignals::PULSE signal);
	//virtual void puckAtJunk(PulseSignals::PULSE signal);
	//virtual void outputPuck(PulseSignals::PULSE signal);
	virtual void refresh(PulseSignals::PULSE signal);
	virtual void request();
	//virtual void closeGate();
	virtual void checkJunk();
	//virtual void conveyorTwoAnswers();
	virtual void receivedPuckInformation(Puck *puck);
	//virtual void startBtn();
	virtual void stopBtn();
	virtual void resetBtn();
	virtual void softFailure();
	virtual void hardFailure();

	/**
	 * @return void
	 * This function is called by constructor.
	 * it sets:
	 * -gate: deny
	 * -statuslight: all off
	 * -conveyor: stop
	 */
	virtual void entryFunction();
};

#endif /* READY_H_ */

