/*
 * Initializing2.h
 *
 *  Created on: 30.04.2014
 *  @author: Ulrich ter Horst
 */


#ifndef INITIALIZING2_H_
#define INITIALIZING2_H_

#include "ControllerStateMachineConveyor2/ConveyorState2.h"
#include "ControllerStateMachineConveyor2/Context2.h"


using namespace std;


class Initializing2 : public ConveyorState2
{
public:
	Initializing2(Context2 *context);
	virtual ~Initializing2();

	//virtual void inputPuck(PulseSignals::PULSE signal);
	//virtual void puckAtHeightSensor(PulseSignals::PULSE signal);
	//virtual void puckAtGate(PulseSignals::PULSE signal);
	//virtual void puckAtJunk(PulseSignals::PULSE signal);
	//virtual void outputPuck(PulseSignals::PULSE signal);
	//virtual void refresh(PulseSignals::PULSE signal);
	//virtual void request();
	//virtual void closeGate();
	//virtual void checkJunk();
	//virtual void conveyorTwoAnswers();
	//virtual void receivedPuckInformation();
	virtual void startBtn();
	//virtual void stopBtn();
	virtual void resetBtn();
	//virtual void softFailure();
	//virtual void hardFailure();

	/**
	* @return void
	* This function is called by constructor.
	* it sets:
	* -gate: deny
	* -statuslight: all off
	* -conveyor: stop
	*/
	virtual void entryFunction();
};

#endif /* INITIALIZING2_H_ */

