/*
 * TurnPuck2.cpp
 *
 *  Created on: 30.04.2014
 *  @author: Ulrich ter Horst
 */

#include "ControllerStateMachineConveyor2/Controller2.h"
#include "../defines.h"

#include "ControllerStateMachineConveyor2/TurnPuck2.h"

#include "ControllerStateMachineConveyor2/Stop2.h"
#include "ControllerStateMachineConveyor2/Initializing2.h"
#include "ControllerStateMachineConveyor2/SoftFailureUnreceipted2.h"
#include "ControllerStateMachineConveyor2/HardFailureUnreceipted2.h"
#include "ControllerStateMachineConveyor2/PuckOnConveyor.h"
#include "ConveyorTime.h"
#include "SensorThread.h"

using namespace std;

TurnPuck2::TurnPuck2(Context2* context) : ConveyorState2(context)
{
	entryFunction();
	stateIndex = IDX_TURN_PUCK2;
}

TurnPuck2::~TurnPuck2()
{

}

void TurnPuck2::refresh(PulseSignals::PULSE signal)
{
	convContext->controller->serial.ping();
}

void TurnPuck2::startBtn()
{
	new (this) PuckOnConveyor(convContext);
}

void TurnPuck2::stopBtn()
{
	if(convContext->controller->serial.sendPulse(PulseSignals::STOP_BTN))
	{		convContext->addStateOnStack(stateIndex);
			new (this) Stop2(convContext);
	}
	else
	{
		hardFailure();
	}
}

void TurnPuck2::resetBtn()
{
		if(convContext->controller->serial.sendPulse(PulseSignals::RESET_BTN))
		{
				new (this) Initializing2(convContext);
		}
		else
		{
			hardFailure();
		}
}

void TurnPuck2::softFailure()
{
	if(convContext->controller->serial.sendPulse(PulseSignals::SOFTFAILURE))
	{
		convContext->addStateOnStack(stateIndex);
		new (this) SoftFailureUnreceipted2(convContext);
	}
	else
	{
		hardFailure();
	}
}

void TurnPuck2::hardFailure(){
	convContext->controller->serial.sendPulse(PulseSignals::HARDFAILURE);
		//Change state to hardFailure unreceipted
		new (this) HardFailureUnreceipted2(convContext);
}

void TurnPuck2::entryFunction()
{
	convContext->controller->conveyor.stop();
	ConveyorTime::getInstance().stopCounter();

	convContext->controller->statusLight.setColor(Statuslight::YELLOW);
	convContext->controller->statusLight.setFrequency(Statuslight::FLASH_05HZ);

	convContext->controller->panel.revokeLedQ2();

	Logger::log("TurnPuck2","entry");
}

