/*
 * HardFailureReceipted2.cpp
 *
 *  Created on: 12.05.2014
 *  @author: Ulrich ter Horst
 */

#include "ControllerStateMachineConveyor2/HardFailureReceipted2.h"
#include "ControllerStateMachineConveyor2/Initializing2.h"
#include "ControllerStateMachineConveyor2/Controller2.h"
#include "ConveyorTime.h"
using namespace std;



HardFailureReceipted2::HardFailureReceipted2(Context2 *context) : ConveyorState2(context)
{
	entryFunction();
}


HardFailureReceipted2::~HardFailureReceipted2()
{

}

void HardFailureReceipted2::refresh(PulseSignals::PULSE signal)
{
	convContext->controller->serial.ping();
}

void HardFailureReceipted2::conveyorTwoAnswers()
{
	if (!convContext->controller->panel.emergencyPressed())
		{
			if (!convContext->controller->panel.resetPressed() && !convContext->controller->panel.startPressed())
			{
				convContext->controller->serial.sendPulse(PulseSignals::OTHERDEVICEANSWERS);
			}
				new (this) Initializing2(convContext);
		}
}

void HardFailureReceipted2::startBtn()
{
		resetBtn();
}

void HardFailureReceipted2::resetBtn()
{
	if (!convContext->controller->panel.emergencyPressed())
	{
		convContext->controller->serial.sendPulse(PulseSignals::OTHERDEVICEANSWERS);
	}
}

void HardFailureReceipted2::entryFunction(){
	convContext->controller->conveyor.stop();
	ConveyorTime::getInstance().stopCounter();
	convContext->controller->statusLight.setColor(Statuslight::RED);
	convContext->controller->statusLight.setFrequency(Statuslight::FLASH_PERMANENT);

	convContext->controller->panel.revokeLedQ2();

	Logger::log("HardFailureReceipted2","entry");
}
