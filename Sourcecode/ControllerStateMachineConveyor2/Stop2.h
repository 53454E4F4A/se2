/*
 * Stop2.h
 *
 *  Created on: 14.05.2014
 *      Author: Ulrich ter Horst
 */

#ifndef STOP2_H_
#define STOP2_H_

#include "ControllerStateMachineConveyor2/ConveyorState2.h"
#include "ControllerStateMachineConveyor2/Context2.h"

#include "ControllerStateMachineConveyor2/Controller2.h"
class Stop2: public ConveyorState2
{
public:
	Stop2(Context2 *context);
	virtual ~Stop2();
	virtual void entryFunction();

	uint8_t stateIndex;

	//Signals:

	//virtual void inputPuck(PulseSignals::PULSE signal);
	//virtual void puckAtHeightSensor(PulseSignals::PULSE signal);
	//virtual void puckAtGate(PulseSignals::PULSE signal);
	virtual void puckAtJunk(PulseSignals::PULSE signal);
	//virtual void outputPuck(PulseSignals::PULSE signal);
	virtual void refresh(PulseSignals::PULSE signal);
	//virtual void request();
	//virtual void closeGate();
	virtual void checkJunk();
	//virtual void conveyorTwoAnswers();
	//virtual void receivedPuckInformation();
	virtual void startBtn();
	//virtual void stopBtn();
	virtual void resetBtn();
	virtual void softFailure();
	virtual void hardFailure();
};

#endif /* STOP2_H_ */
