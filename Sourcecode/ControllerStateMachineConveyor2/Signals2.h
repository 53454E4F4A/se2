/**
 * Signals2.h
 *
 *  Created on: 07.05.2014
 *  @author: Ulrich ter Horst
 *  @file: Signals.h
 */

#ifndef SIGNALS2_H_
#define SIGNALS2_H_
#include "iostream.h"
#include "../PulseSignals.h"
#include "Puck.h"

using namespace std;

class Signals2
{
public:
	Signals2(){cout << "Signals constructor called" << endl;};
	virtual ~Signals2(){cout << "Signals destructor called" << endl;};
	virtual void inputPuck(PulseSignals::PULSE signal)			= 0;
	virtual void puckAtHeightSensor(PulseSignals::PULSE signal)	= 0;
	virtual void puckAtGate(PulseSignals::PULSE signal)			= 0;
	virtual void puckAtJunk(PulseSignals::PULSE signal)			= 0;
	virtual void outputPuck(PulseSignals::PULSE signal)			= 0;
	virtual void refresh(PulseSignals::PULSE signal)			= 0;
	virtual void request()		 								= 0;
	virtual void closeGate()         							= 0;
	virtual void checkJunk()         							= 0;
	virtual void conveyorTwoAnswers()							= 0;
	virtual void receivedPuckInformation(Puck *puck)			= 0;
	virtual void startBtn()          							= 0;
	virtual void stopBtn()           							= 0;
	virtual void resetBtn()          							= 0;
	virtual void softFailure()		 							= 0;
	virtual void hardFailure()		 							= 0;
};

#endif /* SIGNALS2_H_ */
