/**
 * ConveyorBackward.cpp
 *
 *  Created on: 30.04.2014
 *  @author: Ulrich ter Horst
 */


#include "../Puck.h"
#include "ControllerStateMachineConveyor2/Controller2.h"
#include "../defines.h"

#include "ControllerStateMachineConveyor2/ConveyorBackward.h"
#include "ControllerStateMachineConveyor2/TurnPuck2.h"
#include "ControllerStateMachineConveyor2/Stop2.h"
#include "ControllerStateMachineConveyor2/SoftFailureUnreceipted2.h"
#include "ControllerStateMachineConveyor2/HardFailureUnreceipted2.h"

//#include "../HardwareControl/Conveyor.h"

using namespace std;

ConveyorBackward::ConveyorBackward(Context2* context) : ConveyorState2(context)
{
	entryFunction();
	stateIndex = IDX_CONVEYORBACKWARD;
}

ConveyorBackward::~ConveyorBackward()
{

}

void ConveyorBackward::inputPuck(PulseSignals::PULSE signal)
{
	convContext->controller->puck->receiveSignal(signal);
	new (this) TurnPuck2(convContext);
}

void ConveyorBackward::refresh(PulseSignals::PULSE signal)
{

	if (convContext->controller->puck != NULL)
	{
		PulseSignals::PULSE puckAnswer;

		puckAnswer = convContext->controller->puck->receiveSignal(signal);
		if (puckAnswer == PulseSignals::NOTOK)
		{
			hardFailure();
		}
	}
	convContext->controller->serial.ping();
}

void ConveyorBackward::stopBtn()
{
	if(convContext->controller->serial.sendPulse(PulseSignals::STOP_BTN))
	{
		convContext->addStateOnStack(stateIndex);
		new (this) Stop2(convContext);
	}
	else
	{
		hardFailure();
	}
}

void ConveyorBackward::resetBtn()
{
	if(convContext->controller->serial.sendPulse(PulseSignals::RESET_BTN))
	{
		new (this) Initializing2(convContext);
	}
	else
	{
		hardFailure();
	}
}

void ConveyorBackward::softFailure()
{
	if(convContext->controller->serial.sendPulse(PulseSignals::SOFTFAILURE))
	{
			convContext->addStateOnStack(stateIndex);
			new (this) SoftFailureUnreceipted2(convContext);
	}
	else
	{
		hardFailure();
	}
}

void ConveyorBackward::hardFailure(){
	convContext->controller->serial.sendPulse(PulseSignals::HARDFAILURE);
		//Change state to hardFailure unreceipted
	new (this) HardFailureUnreceipted2(convContext);
}

void ConveyorBackward::entryFunction()
{
	convContext->controller->conveyor.backward_fast();
	convContext->controller->statusLight.setColor(Statuslight::GREEN);
	convContext->controller->statusLight.setFrequency(Statuslight::FLASH_PERMANENT);

	convContext->controller->panel.revokeLedQ2();

	Logger::log("ConveyorBackward","entry");
}

