/*
 * Context.cpp
 *
 *  Created on: 07.05.2014
 *  @author: Ulrich ter Horst
 */

#include "./Context2.h"
#include "./Operating.h"


#include "ControllerStateMachineConveyor2/Stop2.h"
#include "ControllerStateMachineConveyor2/TurnPuck2.h"

#include "ControllerStateMachineConveyor2/ConveyorBackward.h"
#include "ControllerStateMachineConveyor2/Ready.h"
#include "ControllerStateMachineConveyor2/PuckOnConveyor.h"

using namespace std;

Context2::Context2(Controller2 *contr):
	controller(contr)
{
	cout << "Context - Constructor called" << endl;
	puckPassedInput = false;

	actualState = NULL;
}

Context2::~Context2()
{

}

void Context2::setState(Signals2* newState)
{
	actualState = newState;
}

bool Context2::goToPrevState(){
	if(stateStack.empty()){
		return false;
	}
	if(actualState != NULL)
	{
		delete actualState;
	}

	switch(stateStack.top()){
	case 5:
		actualState = new Ready(this);
		break;
	case 6:
		actualState = new PuckOnConveyor(this);
		break;
	case 7:
		actualState = new Stop2(this);
		break;
	case 8:
		actualState = new TurnPuck2(this);
		break;
	case 9:
		actualState = new ConveyorBackward(this);
		break;
	default:
		break;
	}

	stateStack.pop();
	return true;
}

void Context2::addStateOnStack(uint8_t state){
	cout << "Context2: State: " << (int)state << "wurde auf dem Stack gespeichert" << endl;

	stateStack.push(state);

	cout << "stateStack size: " << stateStack.size() << endl;
}

void Context2::puckAtHeightSensor(PulseSignals::PULSE signal)
{
	actualState->puckAtHeightSensor(signal);
}

void Context2::puckAtGate(PulseSignals::PULSE signal)
{
	actualState->puckAtGate(signal);
}

void Context2::outputPuck(PulseSignals::PULSE signal)
{
	actualState->outputPuck(signal);
}

void Context2::stopBtn()
{
	actualState->stopBtn();
}

void Context2::puckAtJunk(PulseSignals::PULSE signal)
{
	actualState->puckAtJunk(signal);
}

void Context2::resetBtn()
{
	actualState->resetBtn();
}

void Context2::conveyorTwoAnswers()
{
	actualState->conveyorTwoAnswers();
}

void Context2::receivedPuckInformation(Puck *puck)
{
	actualState->receivedPuckInformation(puck);
}
void Context2::inputPuck(PulseSignals::PULSE signal)
{
	actualState->inputPuck(signal);
}

void Context2::closeGate()
{
	actualState->closeGate();
}

void Context2::refresh(PulseSignals::PULSE signal)
{
	actualState->refresh(signal);
}

void Context2::request()
{
	actualState->request();
}

void Context2::startBtn()
{
	actualState->startBtn();
}

void Context2::checkJunk()
{
	actualState->checkJunk();
}

void Context2::softFailure()
{
	actualState->softFailure();
}

void Context2::hardFailure()
{
	actualState->hardFailure();
}
