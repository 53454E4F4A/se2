/*
 * SoftFailureReceipted2.h
 *
 *  Created on: 12.05.2014
 *  @author: Ulrich ter Horst
 */

#ifndef SOFTFAILURERECEIPTED2_H_
#define SOFTFAILURERECEIPTED2_H_

#include "ControllerStateMachineConveyor2/ConveyorState2.h"
#include "ControllerStateMachineConveyor2/Context2.h"

using namespace std;

class SoftFailureReceipted2: public ConveyorState2
{
public:
	SoftFailureReceipted2();
	SoftFailureReceipted2(Context2 *context);
	virtual ~SoftFailureReceipted2();

	//virtual void inputPuck(PulseSignals::PULSE signal);
	//virtual void puckAtHeightSensor(PulseSignals::PULSE signal);
	//virtual void puckAtGate(PulseSignals::PULSE signal);
	virtual void puckAtJunk(PulseSignals::PULSE signal);
	//virtual void outputPuck(PulseSignals::PULSE signal);
	virtual void refresh(PulseSignals::PULSE signal);
	//virtual void request();
	//virtual void closeGate();
	//virtual void checkJunk();
	//virtual void conveyorTwoAnswers();
	//virtual void receivedPuckInformation();
	virtual void startBtn();
	//virtual void stopBtn();
	virtual void resetBtn();
	//virtual void softFailure();
	virtual void hardFailure();

	/**
		 * @return void
		 * This function is called by constructor.
		 * it sets:
		 * -gate: deny
		 * -statuslight: all off
		 * -conveyor: stop
		 */
	virtual void entryFunction();

private:
	bool isInInputPuck;
};



#endif /* SOFTFAILURERECEIPTED2_H_ */
