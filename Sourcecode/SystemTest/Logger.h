
#ifndef LOGGER_H_
#define LOGGER_H_



#include <list>
#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream
#include <string.h>
#include <pthread.h>
using namespace std;
class Logger {

   public:

     static void log(string tag, string message);


  private:
     static const bool DEBUGGING = true;
     Logger();
     virtual ~Logger();
     static pthread_mutex_t mtx_;

  protected:
      void execute(void* arg);
      void shutdown(void);
};

#endif /* LOGGER_H_ */
