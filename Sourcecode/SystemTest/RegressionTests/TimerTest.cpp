/*
 * TimerTest.cpp
 *
 *  Created on: 21.05.2014
 *      Author: Janek Tichy
 *
 *  This is the regression test for the class ConveyorTime.cpp / .h
 */

#include "TimerTest.h"
#include "ConveyorTime.h"
#include "PulseSignals.h"
#include "Logger.h"
#include <unistd.h>

TimerTest::TimerTest() {

}

TimerTest::~TimerTest() {

}

void TimerTest::RegressionTest() {
	getChid();
	initTimer();
	Logger::log("TimerTest", "init done");

	ConveyorTime::getInstance().startCounter(); // Conveyer time is increasing

	ConveyorTime::getInstance().addNewEvent(PulseSignals::TURNPUCK, 500, true); //cyclic pulse
	Logger::log("TimerTest", "insert event 500 true");
	ConveyorTime::getInstance().addNewEvent(PulseSignals::TURNPUCK, 500, false); //unique pulse
	Logger::log("TimerTest", "insert event 500 false");
	ConveyorTime::getInstance().addNewEvent(PulseSignals::TURNPUCK, 500, true); //cyclic same pulse at same time
	Logger::log("TimerTest", "insert event 500 true");
	ConveyorTime::getInstance().addNewEvent(PulseSignals::TURNPUCK, 500, true, true); //cyclic pulse with system time
	Logger::log("TimerTest", "insert event 500 true,true");
	usleep(1500); //to see the system pulses
	ConveyorTime::getInstance().stopCounter();
	Logger::log("TimerTest", "counter is stopped for 1500 ms");
	usleep(1500); //to see the system pulses
	ConveyorTime::getInstance().addNewEvent(PulseSignals::TURNPUCK, 500, false, true); //unique pulse with system time
	Logger::log("TimerTest", "insert event 500 false,true");
	usleep(1500); //to see all pulses
	ConveyorTime::getInstance().startCounter();
	Logger::log("TimerTest", "Counter is started");

	//delete events, and testing skipping problem
	ConveyorTime::getInstance().deleteEvent(PulseSignals::TURNPUCK, true);
	Logger::log("TimerTest", "delete event true");
	ConveyorTime::getInstance().deleteEvent(PulseSignals::TURNPUCK, false); //delete a non existing event
	Logger::log("TimerTest", "try to delete a non existing event");
	ConveyorTime::getInstance().deleteEvent(PulseSignals::TURNPUCK, true);
	Logger::log("TimerTest", "delete event true");
	ConveyorTime::getInstance().deleteEvent(PulseSignals::TURNPUCK, true);
	Logger::log("TimerTest", "delete event true");
	ConveyorTime::getInstance().deleteEvent(PulseSignals::TURNPUCK, false);
	Logger::log("TimerTest", "delete event false");

	//waiting for existing events
	usleep(5000);
	//regression test is done

}

void TimerTest::getChid() {
	chid = ChannelCreate(0);
	if (chid == -1) {
		perror("Controller: ChannelCreate failed");
		exit(EXIT_FAILURE);
	}
}

void TimerTest::initTimer() {
	ConveyorTime::getInstance().setChid(chid);
	ConveyorTime::getInstance().start(NULL);
}

void TimerTest::MsgReseiveFromTimer() {
	if (MsgReceivePulse(chid, &pulse, sizeof(pulse), NULL) == -1) {
		perror("Controller: MsgReceivePulse failed");
		exit(EXIT_FAILURE);
	}

	switch (pulse.code) {
	case PulseSignals::CHECKJUNK:
		break;
	case PulseSignals::TURNPUCK:
		break;
	}

}

