/*
 * PuckTest.cpp
 *
 *  Created on: May 16, 2014
 *      Author: Lars Schwensen
 */

#include <RegressionTests/PuckTest.h>

PuckTest::PuckTest()
{
	puck = new Puck();

	puck->receiveSignal(PulseSignals::INPUTPUCK);

}

PuckTest::~PuckTest()
{

}
