

#include <iostream>
#include <stdint.h>
#include "HWaccess.h"
#include "SerialTest.h"
#include "Puck.h"


using namespace std;

SerialTest::SerialTest()
{
        //
}

SerialTest::~SerialTest()
{
        //
}

void SerialTest::runTest()
{

  Puck *p = new Puck(1, 2, Puck::JUNK, 3, 4, 5, Puck::METAL, Puck::NOMETAL);
  Logger::log("SerialTest", "Sending Signals");
  for(int i = PulseSignals::INPUTPUCK; i <= PulseSignals::NOTFREE; i++) {

    this->serial->sendPulse((PulseSignals::PULSE)i);
  }
  Logger::log("SerialTest", "Sending Puck");
  this->serial->transferPuck(p);
  Logger::log("SerialTest", "Sending Ping");
  this->serial->ping();




}



void SerialTest::initTest()
{
  this->ctrl = new TestController();
  this->serial = new SerialThread();
  this->serial->setChannel(this->ctrl->getChid());
  this->ctrl->start(NULL);
  this->serial->start(NULL);
}


