

#ifndef SerialTest_H_
#define SerialTest_H_
#include "TestController.h"
#include "SerialThread.h"
#include "Puck.h"
#include "PulseSignals.h"
class SerialTest {
public:
        SerialTest();
        ~SerialTest();

        void initTest();
        void runTest();
        TestController *ctrl;
        SerialThread *serial;
private:

        //
};

#endif /* SerialTest_H_ */
