

#ifndef SensorTest_H_
#define SensorTest_H_
#include "TestController.h"
#include "SensorThread.h"
#include "Puck.h"
#include "PulseSignals.h"
class SensorTest {
public:
        SensorTest();
        ~SensorTest();

        void initTest();
        void runTest();
        TestController *ctrl;
        SensorThread *sensor;
private:

        //
};

#endif /* SensorTest_H_ */
