/*
 * PuckTest.h
 *
 *  Created on: May 16, 2014
 *      Author: Lars Schwensen
 */

#ifndef PUCKTEST_H_
#define PUCKTEST_H_

#include "Puck.h"
#include "PulseSignals.h"

class PuckTest
{
public:
	PuckTest();
	virtual ~PuckTest();

	Puck* puck;
};

#endif /* PUCKTEST_H_ */
