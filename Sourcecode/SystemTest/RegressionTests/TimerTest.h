/*
 * TimerTest.h
 *
 *  Created on: 21.05.2014
 *      Author: Janek Tichy
 */

#ifndef TIMERTEST_H_
#define TIMERTEST_H_

#include "ConveyorTime.h"

class TimerTest {
public://Variables

private://Variables
	int chid;
	struct _pulse pulse;

public://Methods
	TimerTest();
	~TimerTest();

	void RegressionTest();

private://Methods
	void getChid();
	void initTimer();
	void MsgReseiveFromTimer();
};

#endif /* TIMERTEST_H_ */
