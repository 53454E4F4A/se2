/*
 * Controller.cpp
 */

#include "TestController.h"

TestController::TestController()
{
	init();
 
}

TestController::~TestController() {

}

int TestController::getChid() {
  return this->signalChid;
}

void TestController::init() {

    signalChid = ChannelCreate(0);
    if (signalChid == -1) {
        perror("TestController: ChannelCreate failed");
        exit(EXIT_FAILURE);
    }	

}


void TestController::execute(void *arg)
{

     stringstream ss;
	uint16_t height = 0;
    struct _pulse pulse;
    Puck *recievedPuck;
    Logger::log("TestController", "starting");

    while(!isStopped()) {
        if (-1 == MsgReceivePulse(signalChid, &pulse, sizeof(pulse), NULL)) {
            if (isStopped()) break; 
            perror("Controller: MsgReceivePulse failed");
        } else {
		
		switch(pulse.code) {
			case 	PulseSignals::INPUTPUCK:
			  Logger::log("TestController","Getting Signal INPUTPUCK");
					
					break;
			case 	PulseSignals::PUCKATHEIGHT:
			  Logger::log("TestController","Getting Signal PUCKATHEIGHT");
					height = HeightSensor::getInstance().getHeightValue();
					 ss << "Measured height: " << height;
					Logger::log("TestController", ss.str());
					break;
			case 	PulseSignals::PUCKATGATE:
			  Logger::log("TestController","Getting Signal PUCKATGATE");
					break;
			case 	PulseSignals::PUCKATJUNK:
			  Logger::log("TestController","Getting Signal PUCKATJUNK");
					break;
			case 	PulseSignals::OUTPUTPUCK:
			  Logger::log("TestController","Getting Signal OUTPUTPUCK");
					break;
			case 	PulseSignals::REFRESH:
			  Logger::log("TestController","Getting Signal REFRESH");
					break;
			case 	PulseSignals::CLOSEGATE:
			  Logger::log("TestController","Getting Signal CLOSEGATE");
					break;
			case 	PulseSignals::CHECKJUNK:
			  Logger::log("TestController","Getting Signal CHECKJUNK");
					break;
			case 	PulseSignals::JUNKISFREE:
			  Logger::log("TestController","Getting Signal JUNKISFREE");
					break;
			case 	PulseSignals::REQUEST:
			  Logger::log("TestController","Getting Signal REQUEST");
					break;
			case 	PulseSignals::OTHERDEVICEANSWERS:
			  Logger::log("TestController", "Getting Signal OTHERDEVICEANSWERS");
					break;
			case 	PulseSignals::RECEIVEDPUCKINFORMATION:
			  Logger::log("TestController", "Getting Signal RECEIVEDPUCKINFORMATION");
					recievedPuck = (Puck*)pulse.value.sival_ptr;
					if(recievedPuck) Logger::log("TestController", recievedPuck->toString());
					break;
			case 	PulseSignals::START_BTN:
			  Logger::log("TestController","Getting Signal START_BTN");
					break;
			case 	PulseSignals::STOP_BTN:
			  Logger::log("TestController","Getting Signal STOP_BTN");
					break;
			case	PulseSignals::RESET_BTN:
			  Logger::log("TestController","Getting Signal RESET_BTN");
					break;
			case	PulseSignals::SOFTFAILURE:
			  Logger::log("TestController","Getting Signal SOFTFAILURE");
					break;
			case	PulseSignals::HARDFAILURE:
			  Logger::log("TestController","Getting Signal HARDFAILURE");
					break;
			case	PulseSignals::VOIDSIGNAL:
			  Logger::log("TestController","Getting Signal VOIDSIGNAL");
					break;
			case	PulseSignals::OK:
			  Logger::log("TestController","Getting Signal OK");
					break;
			case	PulseSignals::NOTOK:
			  Logger::log("TestController","Getting Signal NOTOK");
					break;
			case	PulseSignals::LEAVECRITICAL:
			  Logger::log("TestController","Getting Signal LEAVECRITICAL");
					break;
			case	PulseSignals::GATEPASS:
			  Logger::log("TestController","Getting Signal GATEPASS");
					break;
			case	PulseSignals::GATEDENY:
			  Logger::log("TestController","Getting Signal GATEDENY");
					break;
			case	PulseSignals::TURNPUCK:
			                Logger::log("TestController","Getting Signal TURNPUCK");
					break;
			case	PulseSignals::FREE:
			                Logger::log("TestController","Getting Signal FREE");
					break;
			case	PulseSignals::NOTFREE:
			                Logger::log("TestController","Getting Signal NOTFREE");
					break;
			
		}
        
        }
   
    }
}


void TestController::shutdown(void)
{
    Logger::log("TestController","shutting down");
}
