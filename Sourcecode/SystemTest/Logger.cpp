/*
 * Controller.cpp
 */
#include <time.h>
#include <string.h>
#include <string>
#include <iostream>
#include "Logger.h"
#include "ScopePattern.h"

Logger::Logger()
{
}

Logger::~Logger() {

}
pthread_mutex_t Logger::mtx_ = PTHREAD_MUTEX_INITIALIZER;
void Logger::log(string tag, string message) {
    ScopePattern lock(&mtx_);
   static bool init = false;
    static struct timespec start, stop;

    stringstream ss;
      double millis;

      if(DEBUGGING) {
      if(!init) {
		if( clock_gettime( CLOCK_REALTIME, &start) == -1 ) {
		  perror( "clock gettime" );
		}
		init = true;
	}    

    if( clock_gettime( CLOCK_REALTIME, &stop) == -1 ) {
      perror( "clock gettime" );
     }

   millis = ( stop.tv_sec - start.tv_sec ) + (double)( stop.tv_nsec - start.tv_nsec ) / (double)1000000000;


   cout.precision(10);
   ss << "[" << fixed << millis << "]" << tag << ": " <<  message;
   cout << ss.str() << endl;
     }
}
