/*
 * ActorTest.cpp
 *
 *  Created on: 06.04.2014
 *      Author: Yannic
 */

#include <iostream>
#include <stdint.h>
#include "HWaccess.h"
#include "ActorTest.h"
#include "../HardwareControl/Controlpanel.h"
#include "../HardwareControl/Conveyor.h"
#include "../HardwareControl/Gate.h"
#include "../HardwareControl/Statuslight.h"


using namespace std;

ActorTest::ActorTest()
{
	//
}

ActorTest::~ActorTest()
{
	//
}

void ActorTest::runTest()
{
	Controlpanel controlpanel;
	Gate gate;
	Conveyor conveyor;

	cout << "##### Start actor test #####" << endl;

	// Test of statuslight
	cout << "Test statuslight red" << endl;
	Statuslight statuslight;
	statuslight.setColor(Statuslight::RED);
	usleep(2000000);
	statuslight.setColor(Statuslight::RED);
	cout << "Test statuslight yellow" << endl;
	statuslight.setColor(Statuslight::YELLOW);
	usleep(2000000);
	statuslight.setColor(Statuslight::YELLOW);
	cout << "Test statuslight green" << endl;
	statuslight.setColor(Statuslight::GREEN);
	usleep(2000000);
	statuslight.setColor(Statuslight::GREEN);

        cout << "Test statuslight all" << endl;

        //statuslight.setFrequenzy(Statuslight::FLASH_1HZ);
        //statuslight.toggleColor( Statuslight::RED | Statuslight::GREEN | Statuslight::YELLOW);
        usleep(5000000);
        statuslight.setColor(Statuslight::RED);
        usleep(5000000);
        statuslight.setColor(Statuslight::YELLOW);
        usleep(5000000);
        statuslight.setColor(Statuslight::GREEN);

	statuslight.stop();

	// Test of controlpanel
	cout << "Test controlpanel LED Q1" << endl;
	controlpanel.setLedQ1();
	usleep(1000000);
	controlpanel.revokeLedQ1();
	cout << "Test controlpanel LED Q2" << endl;
	controlpanel.setLedQ2();
	usleep(1000000);
	controlpanel.revokeLedQ2();
	cout << "Test controlpanel LED Reset" << endl;
	controlpanel.setLedReset();
	usleep(1000000);
	controlpanel.revokeLedReset();
	cout << "Test controlpanel LED Start" << endl;
	controlpanel.setLedStart();
	usleep(1000000);
	controlpanel.revokeLedStart();

	// Test of gate
	cout << "Test gate open" << endl;
	gate.Pass();
	usleep(1000000);
	gate.Deny();

	// Test of conveyor
	cout << "Test conveyor forward fast" << endl;
	conveyor.forward_fast();
	usleep(2000000);
	cout << "Test conveyor forward slow" << endl;
	conveyor.forward_normal();
	usleep(2000000);
	cout << "Test conveyor backward fast" << endl;
	conveyor.backward_fast();
	usleep(2000000);
	cout << "Test conveyor backward slow" << endl;
	conveyor.backward_normal();
	usleep(2000000);
	conveyor.stop();

	cout << "##### Finished actor test #####" << endl;
}





