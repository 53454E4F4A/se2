
#ifndef TestControllerH
#define TestControllerH


#include "HAWThread.h"

#include "Puck.h"
#include "Logger.h"
#include "PulseSignals.h"

using namespace thread;

#include <list>

class TestController : public HAWThread {
private:
    
public:
  TestController();
    virtual ~TestController();
    int getChid();
   

private:
	int signalChid;
	void init();


protected:
    void execute(void* arg);
    void shutdown(void);
};

#endif /* Controller_H_ */
