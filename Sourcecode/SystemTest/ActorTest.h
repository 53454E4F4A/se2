/*
 * ActorTest.h
 *
 *  Created on: 06.04.2014
 *      Author: Yannic
 */

#ifndef ACTORTEST_H_
#define ACTORTEST_H_

class ActorTest {
public:
	ActorTest();
	~ActorTest();

	void runTest();

private:
	//
};

#endif /* ACTORTEST_H_ */
