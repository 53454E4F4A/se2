/*
 * HWLock.h
 *
 *  Created on: 29.03.2014
 *      Author: tichy
 *
 *  This class will get you the code to implement the scope pattern
 */


#ifndef SCOPEPATTERN_H_
#define SCOPEPATTERN_H_

#include <pthread.h> // evt. bei manchen fehler, weil die Lib nicht installiert ist

class ScopePattern {
private:
	pthread_mutex_t* pMtx_;

public:
	ScopePattern(pthread_mutex_t* pMtx);
	~ScopePattern();

private:
	ScopePattern(const ScopePattern& l);
	ScopePattern& operator=(ScopePattern& l);

};

#endif /* HWLOCK_H_ */
