/*
 * HWLock.cpp
 *
 *  Created on: 29.03.2014
 *      Author: tichy
 */

#include <pthread.h> // look into .h
#include "ScopePattern.h"

/**
 * Standard constructor, lock the mutex
 * @param pMtx is a pointer on a pthread mutex
 */
ScopePattern::ScopePattern(pthread_mutex_t* pMtx):pMtx_(pMtx){
	pthread_mutex_lock(pMtx_);
}

/**
 * Standard destructor, unlock the mutex
 */
ScopePattern::~ScopePattern(){
	pthread_mutex_unlock(pMtx_);
}
